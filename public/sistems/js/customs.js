    function toastNotif(status, pesan, judul, urlPindahHalaman) {
        
        if (status !== '' && pesan !== '' && judul !== '') {
                toastr.options = {
                    "closeButton"      : true,
                    "debug"            : false,
                    "newestOnTop"      : true,
                    "progressBar"      : true,
                    "positionClass"    : "toast-top-right",
                    "preventDuplicates": false,
                    "showDuration"     : "800",
                    "hideDuration"     : "1600",
                    "timeOut"          : "2000",
                    "extendedTimeOut"  : "500",
                    "showEasing"       : "swing",
                    "hideEasing"       : "linear",
                    "showMethod"       : "fadeIn",
                    "hideMethod"       : "fadeOut",
                    "onHidden"         : function() {
                        if (urlPindahHalaman && urlPindahHalaman.trim() !== '') {
                        // Pindah halaman setelah pesan toastr ditutup
                            window.location.href = urlPindahHalaman;
                        }
                    }
                };    
                toastr[status](pesan, judul);
        }
    }

    function editDataPaket(paketid) 
    {
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        
        $.ajax({
            url: baseUrl + '/sistem/data-paket',
            type: 'GET',
            dataType:"json",
            data: { paket: paketid },
            success: function (data) 
            {
                document.getElementById('pketid').value         = data.pketid;
                document.getElementById('uasal_peserta').value  = data.asal_peserta;
                document.getElementById('ujenis_peserta').value = data.jenis_peserta;
                document.getElementById('unama_paket').value    = data.nama_paket;
                document.getElementById('uharga_paket').value   = data.harga_paket;
                document.getElementById('ustatus').value        = data.status;
                $('#modalEditPaket').modal('show');
            },
            error: function (xhr) {
                if (xhr.status === 404) {
                    var responseData = JSON.parse(xhr.responseText);
                    var message = responseData.message;
                    toastNotif('error', message, 'GAGAL', );
                }
            }
        });
    }

    function filePembayaran(pendaftaranId) 
    {
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

        $.ajax({
            url: baseUrl + '/sistem/bukti-pembayaran',
            type: 'GET',
            dataType:"json",
            data: { pendaftaran: pendaftaranId },
            success: function (data) 
            {                
                var gambar = document.getElementById('file-buktipembayaran');
                gambar.src = data.bukti_pembayaran;
                document.getElementById('tf-invoice').innerText   = data.kode_invoice;
                document.getElementById('tf-total').innerText     = data.total_biaya;
                document.getElementById('tf-unik').innerText      = data.kode_unik;
                document.getElementById('tf-buttonver').innerHTML = data.btn_verify;

                $('#modalBuktiPembayaran').modal('show');

            },
            error: function (xhr) {
                if (xhr.status === 404) {
                    var responseData = JSON.parse(xhr.responseText);
                    var message = responseData.message;
                    toastNotif('error', message, 'GAGAL', );
                }
            }
        });
    }

    function detailPendaftaran(idPendaftaran) 
    {
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

        
        
        $.ajax({
            url: baseUrl + '/sistem/data-peserta-pendaftaran',
            type: 'GET',
            dataType:"json",
            data: { pendaftaran: idPendaftaran },
            success: function (data) 
            {                
                document.getElementById('md-invoice').innerText = data.kode_invoice;
                document.getElementById('md-total').innerText   = data.total_biaya;
                document.getElementById('md-unik').innerText    = data.kode_unik;
                document.getElementById('md-waktu').innerText   = data.waktu_pendaftaran;
                document.getElementById('md-status').innerText  = data.status_pembayaran;
                document.getElementById('md-admin').innerText   = data.admin_verifikasi || '-';
                document.getElementById('md-tablepeserta').innerHTML  = data.data_pendaftaran;
                $('#modalDetailPeserta').modal('show');
            },
            error: function (xhr) {
                if (xhr.status === 404) {
                    var responseData = JSON.parse(xhr.responseText);
                    var message = responseData.message;
                    toastNotif('error', message, 'GAGAL', );
                }
            }
        });
    }

    function konfirmasiPendaftaran(idPendaftaran) 
    {
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

        Swal.fire({
                title: 'Konfirmasi Pendaftaran',
                text: "Apakah anda ingin mengkonfirmasi pendaftaran peserta ini ?",
                type: 'question',
                showCancelButton: true,
                cancelButtonColor: '#FF3D60',
                confirmButtonText: 'Ya, konfirmasi'
        }).then(function (t){
            if (t.value) {
                $.ajax({
                    url: baseUrl + '/sistem/verifikasi-pendaftaran',
                    type: 'GET',
                    dataType:"json",
                    data: { pendaftaran: idPendaftaran, status: 'terverifikasi'},
                    success: function (data) 
                    {
                        $('#table-pendaftaran').DataTable().ajax.reload();
                        toastNotif(data.statuslog, data.message, data.title);
                    },
                    error: function (xhr) {
                        if (xhr.status === 404) {
                            var responseData = JSON.parse(xhr.responseText);
                            var message = responseData.message;
                            toastNotif('error', message, 'GAGAL', );
                        }
                    }
                });
            }
        })        
    }

    function tolakPendaftaran(idPendaftaran) 
    {
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

        Swal.fire({
            title: 'Tolak Pendaftaran',
            text: "Apakah anda ingin menolak pendaftaran peserta ini ?",
            type: 'question',
            showCancelButton: true,
            cancelButtonColor: '#FF3D60',
            confirmButtonText: 'Ya, tolak'
        }).then(function (t){
            if (t.value) {
                $.ajax({
                    url: baseUrl + '/sistem/verifikasi-pendaftaran',
                    type: 'GET',
                    dataType:"json",
                    data: { pendaftaran: idPendaftaran, status: 'ditolak'},
                    success: function (data) 
                    {
                        $('#table-pendaftaran').DataTable().ajax.reload();
                        toastNotif(data.statuslog, data.message, data.title);
                    },
                    error: function (xhr) {
                        if (xhr.status === 404) {
                            var responseData = JSON.parse(xhr.responseText);
                            var message = responseData.message;
                            toastNotif('error', message, 'GAGAL', );
                        }
                    }
                });
            }
        })        
    }

    function editDataUser(userid) 
    {
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        
        $.ajax({
            url: baseUrl + '/sistem/detail-user',
            type: 'GET',
            dataType:"json",
            data: { user: userid },
            success: function (data) 
            {
                document.getElementById('pmkid').value         = data.pmkid;
                document.getElementById('unama_lengkap').value = data.nama_lengkap;
                document.getElementById('uemail').value        = data.email;                
                document.getElementById('uno_hp').value        = data.no_hp;
                document.getElementById('ulevel_user').value   = data.level_akun;
                document.getElementById('ustatus').value       = data.status_akun;
                $('#modalEditUser').modal('show');
            },
            error: function (xhr) {
                if (xhr.status === 404) {
                    var responseData = JSON.parse(xhr.responseText);
                    var message = responseData.message;
                    toastNotif('error', message, 'GAGAL', );
                }
            }
        });
    }

    function editDataPaketHotel(paketid) 
    {
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        
        $.ajax({
            url: baseUrl + '/sistem/detail-paket-hotel',
            type: 'GET',
            dataType:"json",
            data: { paket: paketid },
            success: function (data) 
            {
                document.getElementById('paketid').value     = data.pakthtlid;
                document.getElementById('unama_hotel').value = data.hotel_id;
                document.getElementById('unama_paket').value = data.nama_paket;
                document.getElementById('uharga').value      = data.harga_paket;
                document.getElementById('ustatus').value     = data.status_paket;
                $('#modalUpdatePaketHotel').modal('show');
            },
            error: function (xhr) {
                if (xhr.status === 404) {
                    var responseData = JSON.parse(xhr.responseText);
                    var message = responseData.message;
                    toastNotif('error', message, 'GAGAL', );
                }
            }
        });
    }

    function strukReservasi(bukti_pembayaran) 
    {
        var gambar = document.getElementById('file-buktipembayaran');
        gambar.src = bukti_pembayaran;
        $('#modalBuktiPembayaran').modal('show');

    }

    function verifikasiReservasi(idReservasi, status) 
    {
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        var textTitle        = '';
        var textSubTitle     = '';
        var konfirmButton    = '';

        if (status == 'selesai') {
            textTitle        = 'Konfirmasi Reservasi';
            textSubTitle     = 'Apakah anda ingin mengkonfirmasi Reservasi peserta ini ?';
            konfirmButton    = 'Ya, konfirmasi';
        } 
        else if (status == 'batal'){
            textTitle        = 'Batalkan Reservasi';
            textSubTitle     = 'Apakah anda ingin membatalkan Reservasi peserta ini ?';
            konfirmButton    = 'Ya, batalkan';
        }

        Swal.fire({
                title: textTitle,
                text: textSubTitle,
                type: 'question',
                showCancelButton: true,
                cancelButtonColor: '#FF3D60',
                confirmButtonText: konfirmButton,
        }).then(function (t){
            if (t.value) {
                $.ajax({
                    url: baseUrl + '/sistem/verifikasi-reservasi',
                    type: 'GET',
                    dataType:"json",
                    data: { reservasi: idReservasi, statusres: status},
                    success: function (data) 
                    {
                        $('#table-reservasi-hotel').DataTable().ajax.reload();
                        toastNotif(data.statuslog, data.message, data.title);
                    },
                    error: function (xhr) {
                        if (xhr.status === 404) {
                            var responseData = JSON.parse(xhr.responseText);
                            var message = responseData.message;
                            toastNotif('error', message, 'GAGAL', );
                        }
                    }
                });
            }
        })        
    }

    function detailReservasi(reservasi) 
    {
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

        
        
        $.ajax({
            url: baseUrl + '/sistem/data-reservasi-detail',
            type: 'GET',
            dataType:"json",
            data: { reservasi: reservasi },
            success: function (data) 
            {                
                document.getElementById('namalengkap').innerText = data.namalengkap;
                document.getElementById('nohp').innerText        = data.nohp;
                document.getElementById('namahotel').innerText   = data.namahotel;
                document.getElementById('pakethotel').innerText  = data.pakethotel;
                document.getElementById('cico').innerText        = data.cico;
                document.getElementById('totalbiaya').innerHTML  = data.totalbiaya;
                document.getElementById('waktu').innerText       = data.waktu;
                $('#modalDetailReservasiHotel').modal('show');
            },
            error: function (xhr) {
                if (xhr.status === 404) {
                    var responseData = JSON.parse(xhr.responseText);
                    var message = responseData.message;
                    toastNotif('error', message, 'GAGAL', );
                }
            }
        });
    }

    function previewAbstract(uRLAbstract) {
        
        var iframe = document.getElementById('iframeAbstract');
        iframe.src = uRLAbstract;
        $('#modalPreview').modal('show');

    }

    function hapusPendaftaran(idPendaftaran) 
    {
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

        Swal.fire({
            title: 'Hapus Pendaftaran',
            text: "Apakah anda ingin menghapus pendaftaran peserta ini ?",
            type: 'question',
            showCancelButton: true,
            cancelButtonColor: '#FF3D60',
            confirmButtonText: 'Ya, hapus'
        }).then(function (t){
            if (t.value) {
                $.ajax({
                    url: baseUrl + '/sistem/hapus-pendaftaran',
                    type: 'GET',
                    dataType:"json",
                    data: { pendaftaran: idPendaftaran, status: 'hapus'},
                    success: function (data) 
                    {
                        $('#table-pendaftaran').DataTable().ajax.reload();
                        toastNotif(data.statuslog, data.message, data.title);
                    },
                    error: function (xhr) {
                        if (xhr.status === 404) {
                            var responseData = JSON.parse(xhr.responseText);
                            var message = responseData.message;
                            toastNotif('error', message, 'GAGAL', );
                        }
                    }
                });
            }
        })        
    }

    function konfirmasiKehadiran(idPeserta)
    {
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

        Swal.fire({
            title: 'Presensi Kehadiran',
            text: "Konfirmasi kehadiran peserta ini ?",
            type: 'question',
            showCancelButton: true,
            cancelButtonColor: '#FF3D60',
            confirmButtonText: 'Ya, hadir'
        }).then(function (t){
            if (t.value) {
                $.ajax({
                    url: baseUrl + '/sistem/presensi-kehadiran',
                    type: 'GET',
                    dataType:"json",
                    data: { peserta: idPeserta},
                    success: function (data) 
                    {
                        toastNotif(data.statuslog, data.message, data.title);
                        document.getElementById('list-data-peserta').innerHTML = '';
                        document.getElementById('detail-peserta').innerHTML    = '';
                        document.getElementById('identitaspeserta').value      = '';
                        document.getElementById('identitaspeserta').focus();

                    },
                    error: function (xhr) {
                        if (xhr.status === 404) {
                            var responseData = JSON.parse(xhr.responseText);
                            var message = responseData.message;
                            toastNotif('error', message, 'GAGAL', );
                        }
                    }
                });
            }
        }) 
    }

    function resetDayPresensi(day)
    {
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

        Swal.fire({
            title: 'Reset Presensi',
            text: "Reset data presensi ?",
            type: 'question',
            showCancelButton: true,
            cancelButtonColor: '#FF3D60',
            confirmButtonText: 'Ya, reset'
        }).then(function (t){
            if (t.value) {
                showLoading();
                $.ajax({
                    url: baseUrl + '/sistem/reset-presensi',
                    type: 'GET',
                    data: { hari: day},
                    dataType:"json",
                    success: function (data) 
                    {
                        toastNotif(data.statuslog, data.message, data.title);
                    },
                    error: function (xhr) {
                        if (xhr.status === 404) {
                            var responseData = JSON.parse(xhr.responseText);
                            var message = responseData.message;
                            toastNotif('error', message, 'GAGAL', );
                        }
                    },
                    complete: function() {
                        hideLoading();
                    }
                });
            }
        }) 
    }

    function hideLoading(){
        $("#loading-moment").fadeOut();
    }

    // Fungsi tutup loading
    function showLoading(){
        $("#loading-moment").fadeIn();
    }

    function getIdPeserta(idPeserta)
    {
        var valuepeserta   = idPeserta;
        document.getElementById("identitaspeserta").value     = idPeserta;
        document.getElementById('list-data-peserta').innerHTML = '';
        document.getElementById('detail-peserta').innerHTML    = '';
        clearTimeout(typingTimer);

        // Cek panjang inputan
        if (valuepeserta.length >= 2) {

            typingTimer = setTimeout(function() {
                const baseUrlElement = document.getElementById('base-url');
                const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

                $.ajax({
                    url: baseUrl + '/sistem/search-peserta',
                    type: 'GET',
                    dataType: "json",
                    data: { peserta: valuepeserta},
                    success: function(data) {
                        if (data.statuslog == 'success') {
                            document.getElementById('list-data-peserta').innerHTML = data.results;
                        }else if (data.statuslog == 'single-success') {
                            document.getElementById('list-data-peserta').innerHTML = data.results;
                            document.getElementById('detail-peserta').innerHTML = data.detaildata;
                        } else {                            
                            document.getElementById('list-data-peserta').innerHTML = data.results;
                        }
                    },
                    error: function(xhr) {
                        if (xhr.status === 404) {
                            var responseData = JSON.parse(xhr.responseText);
                        }
                    }
                });
            }, doneTypingInterval);
        }
    }

    hideLoading();

        
    






   





    $('#logout-btn').click(function(){
        const link = $('#logout-btn').data('link');
            Swal.fire({
                  title: 'Logout',
                  text: "Apakah anda ingin logout dari aplikasi?",
                  type: 'question',
                  showCancelButton: true,
                  cancelButtonColor: '#FF3D60',
                  confirmButtonText: 'Ya, logout sistem'
            }).then(function (t){
                if (t.value) {
                    window.location = link;
                }
            })
    });

    $('#btn-resetpencarian').click(function(){
        document.getElementById('list-data-peserta').innerHTML = '';
        document.getElementById('detail-peserta').innerHTML    = '';
        document.getElementById('identitaspeserta').value = '';
        document.getElementById('identitaspeserta').focus();
    });

    $('#btn-generateqrcode').click(function(){
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

        Swal.fire({
            title: 'Generate QR Code',
            text: "Generate QR Code peserta terbaru ?",
            type: 'question',
            showCancelButton: true,
            cancelButtonColor: '#FF3D60',
            confirmButtonText: 'Ya, generate'
        }).then(function (t){
            if (t.value) {
                showLoading();
                $.ajax({
                    url: baseUrl + '/sistem/generate-qrcode',
                    type: 'GET',
                    dataType:"json",
                    success: function (data) 
                    {
                        toastNotif(data.statuslog, data.message, data.title);
                    },
                    error: function (xhr) {
                        if (xhr.status === 404) {
                            var responseData = JSON.parse(xhr.responseText);
                            var message = responseData.message;
                            toastNotif('error', message, 'GAGAL', );
                        }
                    },
                    complete: function() {
                        hideLoading();
                    }
                });
            }
        }) 
    });

    $('#btn-sendnotifqrcode').click(function(){
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

        Swal.fire({
            title: 'Send QR Code',
            text: "Send QR Code peserta ?",
            type: 'question',
            showCancelButton: true,
            cancelButtonColor: '#FF3D60',
            confirmButtonText: 'Ya, send'
        }).then(function (t){
            if (t.value) {
                showLoading();
                $.ajax({
                    url: baseUrl + '/sistem/send-notif-qrcode',
                    type: 'GET',
                    dataType:"json",
                    success: function (data) 
                    {
                        toastNotif(data.statuslog, data.message, data.title);
                    },
                    error: function (xhr) {
                        if (xhr.status === 404) {
                            var responseData = JSON.parse(xhr.responseText);
                            var message = responseData.message;
                            toastNotif('error', message, 'GAGAL', );
                        }
                    },
                    complete: function() {
                        hideLoading();
                    }
                });
            }
        }) 
    });

    $("#form-login").submit(function (event) {
        event.preventDefault();
      
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

        var formData = new FormData(this);
          $.ajax({
              url: baseUrl + '/sistem/login',
              type: "post",
              data: formData,
              dataType: 'json',
              contentType: false,
              cache: false,
              processData: false,
              success: function (response) {
                toastNotif(response.statuslog, response.message, response.title, response.links);
              },
              error: function (xhr) {
                  var responseData = JSON.parse(xhr.responseText);
                  var message      = responseData.message;
                  toastNotif('error', message, 'GAGAL');
              }
          });        
       
    });

    $("#form-tambah-paket").submit(function (event) {
        event.preventDefault();
      
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        var formData         = new FormData(this);

        Swal.fire({
            title: 'Tambah Paket',
            text: "Apakah anda ingin menambahkan paket baru ?",
            type: 'question',
            showCancelButton: true,
            cancelButtonColor: '#FF3D60',
            confirmButtonText: 'Ya, tambah'
        }).then(function (t){
            if (t.value) 
            {
                $.ajax({
                    url: baseUrl + '/sistem/tambah-paket',
                    type: "post",
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        toastNotif(response.statuslog, response.message, response.title);
                        $('#table-paket').DataTable().ajax.reload();
                        $('#modalTambahPaket').modal('hide');

                    },
                    error: function (xhr) {
                        var responseData = JSON.parse(xhr.responseText);
                        var message      = responseData.message;
                        toastNotif('error', message, 'GAGAL');
                    }
                });
            }
        })
                
       
    });

    $("#form-edit-paket").submit(function (event) {
        event.preventDefault();
      
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        var formData         = new FormData(this);

        Swal.fire({
            title: 'Update Paket',
            text: "Apakah anda ingin mengupdate data paket ?",
            type: 'question',
            showCancelButton: true,
            cancelButtonColor: '#FF3D60',
            confirmButtonText: 'Ya, Update'
        }).then(function (t){
            if (t.value) 
            {
                $.ajax({
                    url: baseUrl + '/sistem/update-paket',
                    type: "post",
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        toastNotif(response.statuslog, response.message, response.title);
                        $('#table-paket').DataTable().ajax.reload();
                        $('#modalEditPaket').modal('hide');

                    },
                    error: function (xhr) {
                        var responseData = JSON.parse(xhr.responseText);
                        var message      = responseData.message;
                        toastNotif('error', message, 'GAGAL');
                    }
                });
            }
        })
                
       
    });

    $("#form-tambah-user").submit(function (event) {
        event.preventDefault();
      
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        var formData         = new FormData(this);

        Swal.fire({
            title: 'Tambah User',
            text: "Apakah anda ingin menambahkan user baru ?",
            type: 'question',
            showCancelButton: true,
            cancelButtonColor: '#FF3D60',
            confirmButtonText: 'Ya, tambah'
        }).then(function (t){
            if (t.value) 
            {
                $.ajax({
                    url: baseUrl + '/sistem/tambah-user',
                    type: "post",
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        toastNotif(response.statuslog, response.message, response.title);
                        $('#table-datauser').DataTable().ajax.reload();
                        $('#modalTambahUser').modal('hide');
                    },
                    error: function (xhr) {
                        var responseData = JSON.parse(xhr.responseText);
                        var message      = responseData.message;
                        toastNotif('error', message, 'GAGAL');
                    }
                });
            }
        })
                
       
    });

    $("#form-edit-user").submit(function (event) {
        event.preventDefault();
      
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        var formData         = new FormData(this);

        Swal.fire({
            title: 'Update User',
            text: "Apakah anda ingin mengedit data user ?",
            type: 'question',
            showCancelButton: true,
            cancelButtonColor: '#FF3D60',
            confirmButtonText: 'Ya, edit'
        }).then(function (t){
            if (t.value) 
            {
                $.ajax({
                    url: baseUrl + '/sistem/update-user',
                    type: "post",
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        toastNotif(response.statuslog, response.message, response.title);
                        $('#table-datauser').DataTable().ajax.reload();
                        $('#modalEditUser').modal('hide');
                    },
                    error: function (xhr) {
                        var responseData = JSON.parse(xhr.responseText);
                        var message      = responseData.message;
                        toastNotif('error', message, 'GAGAL');
                    }
                });
            }
        })
                
       
    });

    $("#form-tambah-hotel").submit(function (event) {
        event.preventDefault();
      
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        var formData         = new FormData(this);

        Swal.fire({
            title: 'Tambah Hotel',
            text: "Apakah anda ingin menambahkan Hotel baru ?",
            type: 'question',
            showCancelButton: true,
            cancelButtonColor: '#FF3D60',
            confirmButtonText: 'Ya, tambah'
        }).then(function (t){
            if (t.value) 
            {
                $.ajax({
                    url: baseUrl + '/sistem/tambah-hotel',
                    type: "post",
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        toastNotif(response.statuslog, response.message, response.title);
                        document.getElementById("form-tambah-hotel").reset();
						document.getElementById("gambar_hotel").value = '';
                        $('#table-hotel').DataTable().ajax.reload();
                        $('#modalTambahHotel').modal('hide');
                    },
                    error: function (xhr) {
                        var responseData = JSON.parse(xhr.responseText);
                        var message      = responseData.message;
                        toastNotif('error', message, 'GAGAL');
                    }
                });
            }
        })
                
       
    });

    $("#form-tambahpaket-hotel").submit(function (event) {
        event.preventDefault();
      
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        var formData         = new FormData(this);

        Swal.fire({
            title: 'Tambah Paket Hotel',
            text: "Apakah anda ingin menambahkan Paket baru ?",
            type: 'question',
            showCancelButton: true,
            cancelButtonColor: '#FF3D60',
            confirmButtonText: 'Ya, tambah'
        }).then(function (t){
            if (t.value) 
            {
                $.ajax({
                    url: baseUrl + '/sistem/tambah-paket-hotel',
                    type: "post",
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        toastNotif(response.statuslog, response.message, response.title);
                        document.getElementById("form-tambahpaket-hotel").reset();
                        $('#table-paket-hotel').DataTable().ajax.reload();
                        $('#modalTambahPaketHotel').modal('hide');
                    },
                    error: function (xhr) {
                        var responseData = JSON.parse(xhr.responseText);
                        var message      = responseData.message;
                        toastNotif('error', message, 'GAGAL');
                    }
                });
            }
        })
                
       
    });

    $("#form-update-paket-hotel").submit(function (event) {
        event.preventDefault();
      
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        var formData         = new FormData(this);

        Swal.fire({
            title: 'Update Paket Hotel',
            text: "Apakah anda ingin mengupdate Paket Hotel ?",
            type: 'question',
            showCancelButton: true,
            cancelButtonColor: '#FF3D60',
            confirmButtonText: 'Ya, update'
        }).then(function (t){
            if (t.value) 
            {
                $.ajax({
                    url: baseUrl + '/sistem/update-paket-hotel',
                    type: "post",
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        toastNotif(response.statuslog, response.message, response.title);
                        document.getElementById("form-update-paket-hotel").reset();
                        $('#table-paket-hotel').DataTable().ajax.reload();
                        $('#modalUpdatePaketHotel').modal('hide');
                    },
                    error: function (xhr) {
                        var responseData = JSON.parse(xhr.responseText);
                        var message      = responseData.message;
                        toastNotif('error', message, 'GAGAL');
                    }
                });
            }
        })
                
       
    });

    $('#btn-cetaklaporan').click(function(){
        var selectedValue = document.getElementById('jenis-laporan').value;
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        
        // Buat URL tujuan berdasarkan nilai yang dipilih
        var url = baseUrl ;
        switch (selectedValue) {
            case 'Semua':
                url = '/sistem/cetak-laporan/semua';
                break;
            case 'Symposium':
                url = '/sistem/cetak-laporan/symposium';
                break;
            case 'Workshop 1':
                url = '/sistem/cetak-laporan/workshop1';
                break;
            case 'Workshop 2':
                url = '/sistem/cetak-laporan/workshop2';
                break;
            default:
                url = '/sistem/cetak-laporan/semua';
                break;
        }
        // Buka halaman baru dengan URL yang sesuai
        window.open(url, '_blank');
    });

    $('#btn-cetaklaporanpresensi').click(function(){
        var selectedValue    = document.getElementById('jenis-laporanpresensi').value;
        const baseUrlElement = document.getElementById('base-url');
        const baseUrl        = baseUrlElement.getAttribute('data-baseurl');
        
        // Buat URL tujuan berdasarkan nilai yang dipilih
        var url = baseUrl ;
        switch (selectedValue) {
            case 'workshop1':
                url = '/sistem/cetak-laporan-presensi/workshop1';
                break;
            case 'workshop2':
                url = '/sistem/cetak-laporan-presensi/workshop2';
                break;
            case 'symposiumd1':
                url = '/sistem/cetak-laporan-presensi/symposiumd1';
                break;
            case 'symposiumd2':
                url = '/sistem/cetak-laporan-presensi/symposiumd2';
                break;
        }
        // Buka halaman baru dengan URL yang sesuai
        window.open(url, '_blank');
    });
    
    var typingTimer;  // Timer identifier
    var doneTypingInterval = 500;

    $("#identitaspeserta").on('input', function() {
        var valuepeserta   = $(this).val();
        document.getElementById('list-data-peserta').innerHTML = '';
        document.getElementById('detail-peserta').innerHTML    = '';
        clearTimeout(typingTimer);
        // Cek panjang inputan
        if (valuepeserta.length >= 2) {

            typingTimer = setTimeout(function() {
                const baseUrlElement = document.getElementById('base-url');
                const baseUrl        = baseUrlElement.getAttribute('data-baseurl');

                $.ajax({
                    url: baseUrl + '/sistem/search-peserta',
                    type: 'GET',
                    dataType: "json",
                    data: { peserta: valuepeserta},
                    success: function(data) {
                        if (data.statuslog == 'success') {
                            document.getElementById('list-data-peserta').innerHTML = data.results;
                        }else if (data.statuslog == 'single-success') {
                            document.getElementById('list-data-peserta').innerHTML = data.results;
                            document.getElementById('detail-peserta').innerHTML = data.detaildata;
                        } else {                            
                            document.getElementById('list-data-peserta').innerHTML = data.results;
                        }
                    },
                    error: function(xhr) {
                        if (xhr.status === 404) {
                            var responseData = JSON.parse(xhr.responseText);
                        }
                    }
                });
            }, doneTypingInterval);
        }
    });