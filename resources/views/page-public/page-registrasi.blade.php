@extends('layout.mainbody')

@section('container')

    <div class="section over-hide padding-top-120 padding-top-mob-nav section-background-24">	
        <div class="section-1400 pt-xl-4">
            <div class="container-fluid padding-top-bottom-80">
                <div class="row">
                    <div class="col-lg">
                        <h2 class="display-8 mb-3">
                            PIT PAMKI 2024
                        </h2>
                    </div>
                    <div class="col-lg-auto align-self-center mt-4 mt-lg-0">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb no-border">
                                <li class="breadcrumb-item"><a href="#" class="link link-dark-primary size-14" data-hover="Home">Home</a></li>
                                <li class="breadcrumb-item active font-weight-500" aria-current="page"><span class="size-14">Materi Acara</span></li>
                            </ol>
                        </nav>				
                    </div>
                </div>
            </div>
        </div>

        <div class="section over-hide padding-top-bottom-120 bg-white section-background-20 background-img-top" id="pit-pamki">	
			<div class="section-1400">
				<div class="container-fluid">
					<div class="row sticky-wrapper" id="accordionExample">
						<div class="col-lg-4">
							<div class="section border-4 bg-white landing-shadow-4 p-3 p-xl-4 z-bigger" id="sticker-careers">
								<div class="row">
									<div class="col-12">
                                        
                                        <a href="#pit-pamki" data-bagian="tata-cara" data-gal='m_PageScroll2id' class="btn btn-user-profile btn-fluid justify-content-start px-3" data-toggle="collapse" data-target="#tata-cara" aria-expanded="true" aria-controls="tata-cara">
											Tata Cara Pendaftaran
										</a>
										<a href="#pit-pamki" data-bagian="registrasi" data-gal='m_PageScroll2id' class="btn btn-user-profile btn-fluid justify-content-start px-3" data-toggle="collapse" data-target="#registrasi" aria-expanded="false" aria-controls="registrasi">
											Pendaftaran / Registrasi
										</a>
										<a href="#pit-pamki" data-bagian="bukti-pembayaran" data-gal='m_PageScroll2id' class="btn btn-user-profile btn-fluid justify-content-start mt-1 px-3" data-toggle="collapse" data-target="#bukti-pembayaran" aria-expanded="false" aria-controls="bukti-pembayaran">
											Konfirmasi Pembayaran
										</a>
                                        <a href="#pit-pamki" data-bagian="notifikasi-email" data-gal='m_PageScroll2id' class="btn btn-user-profile btn-fluid justify-content-start mt-1 px-3" data-toggle="collapse" data-target="#notifikasi-email" aria-expanded="false" aria-controls="notifikasi-email">
											Notifikasi Ulang Email
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-8 mt-4 mt-lg-0">
							<div class="section border-4 bg-white landing-shadow-4 p-3 p-xl-4">   
							
                                {{-- Tata Cara --}}
								<div id="tata-cara" class="collapse p-0 show" data-parent="#accordionExample">
									<div class="card-body p-0">
										<div class="row">
											<div class="col-12">
												<h5 class="mb-3">
													Tata Cara Pendaftaran
												</h5>
											</div>
											<div class="col-12 pb-3">
												<div class="section divider divider-gray"></div>
											</div>
											<div class="col-12 text-dark">
												
                                                <p>
                                                    <ol>
                                                        <li><h6>Lengkapi Formulir Pendaftaran</h6>
                                                            <ul>
                                                                <li>Isilah formulir pendaftaran dengan data peserta yang valid.</li>
                                                                <li>Harap menggunakan email aktif untuk menerima notifikasi pembayaran dan informasi terkait acara.</li>
                                                                <li>Apabila tidak menemukan email pendaftaran pada kotak masuk, <b>silahkan check pada bagian SPAM</b>.</li>
                                                            </ul>
                                                        </li>
                                                        <li><h6>Tata Cara Pembayaran</h6>
                                                            <ul>
                                                                <li>Setelah mengisi formulir dan meng-submit pendaftaran, Anda akan menerima notifikasi melalui email yang berisi nomor Invoice dan jumlah pembayaran yang harus dibayarkan sesuai dengan jenis kegiatan dan jenis peserta yang anda daftarkan.</li>
                                                                <li>Saat proses pembayaran pastikan nominal harus sesuai dengan jumlah yang harus dibayarkan dan kode unik yang tertera melalui email.</li>
                                                                <li>Pada bagian berita acara, pastikan Anda mengisi berita acara dengan <b>NOMOR INVOICE</b> yang diterima melalui email.</li>
                                                                <li>Pembayaran yang telah dilakukan <b class="text-danger">TIDAK DAPAT DIBATALKAN</b> atau <b class="text-danger">REFUND</b>.</li>
                                                            </ul>
                                                        </li>
                                                        <li><h6>Konfirmasi Pembayaran</h6>
                                                            <ul>
                                                                <li>Pembayaran dilakukan melalui : 
                                                                    <table width="100%" class="ml-3">
                                                                        <tr>
                                                                            <th width="20%" >Nama BANK</th>
                                                                            <th width="5%">:</th>
                                                                            <td>MANDIRI</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Nama Rekening</th>
                                                                            <th >:</th>
                                                                            <td>PAMKI CABANG RIAU</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Nomor Rekening</th>
                                                                            <th >:</th>
                                                                            <td>1220093714817</td>
                                                                        </tr>
                                                                    </table>
                                                                    
                                                                </li>
                                                                <li>Setelah melakukan pembayaran, Anda dapat mengunggah bukti transfer melalui formulir konfirmasi pembayaran.</li>
                                                                <li>Setelah konfirmasi pembayaran diterima, Anda akan menerima notifikasi pembayaran Anda.</li>
                                                                <li>Proses konfirmasi pembayaran akan memakan waktu hingga 2x24 jam.</li>
                                                            </ul>
                                                        </li>
                                                        <li><h6>Kontak Admin</h6>
                                                            <ul>
                                                                <table width="100%" class="ml-3">
                                                                    <tr>
                                                                        <th width="20%" >Mislindawati</th>
                                                                        <th width="5%">:</th>
                                                                        <td>+62 811 753 684</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Agnes Rica</th>
                                                                        <th >:</th>
                                                                        <td>+62 812 6808 1956</td>
                                                                    </tr>
                                                                    </tr>
                                                                </table>
                                                                
                                                            </ul>
                                                        </li>
                                                    </ol>         
                                                </p>

											</div>
										</div>
									</div>
								</div>

                                {{-- Form Registrasi --}}
                                <div id="registrasi" class="collapse p-0 " data-parent="#accordionExample">
									<div class="card-body p-0">
										<div class="row">
											<div class="col-12">
												<h5 class="mb-3">
													Pendaftaran PIT PAMKI 2024
												</h5>
											</div>
											<div class="col-12 pb-3">
												<div class="section divider divider-gray"></div>
											</div>
											<div class="col-12 text-dark ">
                                                                                                
                                                <h5 class="mb-2 text-center">Form Registrasi PIT PAMKI 2024</h5>

                                                <div class="col-12 text-left">
                                                </div>
                                                
                                                
                                                <form id="form-registrasi" action="#">
                                                    <div class="row ">
                                                        <p class=" text-left text-danger">*Silahkan lengkapi data diri anda. <br>*Pastikan alamat email yang anda isi valid untuk menerima notifikasi pembayaran dan informasi lainnya</p>
                                                        <div class="col-lg-12 bg-primarys">
                                                            @csrf
                                                            <div class="form-group">
                                                                <label for="" class="text-dark font-weight-bold">Jenis Partisipasi</label>
                                                                <div class="section">
                                                                    <select class="wide select-style text-dark" id="jenis-partisipasi" name="jenis-partisipasi" required>
                                                                        <option value="" hidden selected disabled>- Jenis Partisipasi -</option>
                                                                        @foreach ($paket as $item)
                                                                            <option value="{{ $item }}">{{ $item }}</option>                                                                        
                                                                        @endforeach                                                                    
                                                                    </select>
                                                                </div> 	
                                                            </div>                                                              
                                                        </div>
                                                                
                                                        <div class="col-lg-12 mt-3 bg-dangers">
                                                            <div class="form-group">
                                                                <label for="" class="text-dark font-weight-bold">Jenis Kegiatan</label>
                                                                <div class="section">
                                                                    <select class="wide select-style text-dark" id="jenis-kegiatan" name="jenis-kegiatan" required>
                                                                        <option value="" hidden selected disabled>-- Jenis Kegiatan --</option>
                                                                    </select>
                                                                </div> 	
                                                            </div>                                                              
                                                        </div>

                                                        <div class="col-lg-12 mt-3 "> 
                                                            <div class="form-group">
                                                                <label for="" class="text-dark font-weight-bold">Total Biaya </label>
                                                                <input type="text" class="form-style bg-gray" id="total-biaya" name="total-biaya" readonly>
                                                            </div>   				
                                                        </div>
                        
                                                        <div class="col-md-6 mt-3"> 
                                                            <div class="form-group">
                                                                <label for="gelar-depan" class="text-dark font-weight-bold">Gelar Depan </label>
                                                                <input type="text" class="form-style" name="gelar-depan" id="gelar-depan" placeholder="Dr, Prof, Ir ..." autocomplete="off" >
                                                            </div>             
                                                        </div>

                                                        <div class="col-md-6 mt-3 "> 
                                                            <div class="form-group">
                                                                <label for="" class="text-dark font-weight-bold">Gelar Belakang </label>
                                                                <input type="text" class="form-style" name="gelar-belakang" id="gelar-belakang" placeholder="Sp.MK(K), SpOG, ..." autocomplete="off" >
                                                            </div>             
                                                        </div>
                                                    
                                                        <div class="col-lg-12 mt-3 "> 
                                                            <div class="form-group">
                                                                <label for="" class="text-dark font-weight-bold">Nama Lengkap </label>
                                                                <input type="text" class="form-style " name="nama-lengkap-peserta" id="nama-lengkap-peserta" placeholder="John Doe ..." autocomplete="off" required>
                                                            </div>   				
                                                        </div>	
                        
                                                        <div class="col-md-6 mt-3"> 
                                                            <div class="form-group">
                                                                <label for="" class="text-dark font-weight-bold">Email </label>
                                                                <input type="email" class="form-style" name="email" id="email" required autocomplete="off">
                                                            </div>             
                                                        </div>

                                                        <div class="col-md-6 mt-3 "> 
                                                            <div class="form-group">
                                                                <label for="" class="text-dark font-weight-bold">No. HP WA  </label>
                                                                <input type="number" class="form-style" name="nohp" id="nohp" required autocomplete="off" placeholder="0812345xxxx">
                                                            </div>             
                                                        </div>

                                                        <div class="col-md-12 mt-3 "> 
                                                            <div class="form-group">
                                                                <label for="" class="text-dark font-weight-bold">Instansi Asal</label>
                                                                <input type="text" class="form-style" name="instansi-asal" id="instansi-asal" autocomplete="off" required>
                                                            </div>             
                                                        </div>

                                                        <div class="col-md-12 mt-3 "> 
                                                            <button type="submit" class="btn text-white p-3 " style="background-color:#891d13;">
                                                                <i class="uil uil-user-plus size-20 mr-2"></i>REGISTRASI
                                                            </button>
                                                            <button type="reset" class="btn text-white p-3 btn-warning text-dark" >
                                                                <i class="uil uil-user-plus size-20 mr-2"></i>RESET
                                                            </button>
                                                            {{-- <button type="button" class="btn btn-primary btn-xs" id="sa-title">Click me</button> --}}
                                                        </div>
                                                    </div>
                                                </form>
                                                

											</div>
										</div>
									</div>
								</div>
								
                                {{-- Form Upload Bukti Pembayran --}}
								<div id="bukti-pembayaran" class="collapse p-0" data-parent="#accordionExample">
									<div class="card-body p-0">
										<div class="row">
											<div class="col-12">
												<h5 class="mb-3">
													Konfirmasi Pembayaran
												</h5>
											</div>
											<div class="col-12 pb-3">
												<div class="section divider divider-gray"></div>
											</div>
											<div class="col-12">
                                                <form action="#" id="form-uploadpembayaran">
                                                    <div class="row justify-content-center">
                                                        @csrf

                                                        <div class="col-lg-12 mt-3 "> 
                                                            <div class="form-group">
                                                                <label for="" class="text-dark font-weight-bold">Nomor Invoice</label>
                                                                <input type="text" class="form-style " autocomplete="off" name="kode_invoice" id="kode_invoice" placeholder="PMKI-0507-0999..." required>
                                                                <input type="hidden" name="id_pendaftaran" id="id_pendaftaran">
                                                            </div>   				
                                                        </div>                                                                                                            
                                                        
                                                    
                                                        <div class="col-lg-12 mt-3 "> 
                                                            <div class="form-group">
                                                                <label for="" class="text-dark font-weight-bold">Nama Lengkap </label>
                                                                <input type="text" readonly class="bg-gray form-style " id="dtl-namalengkap">
                                                            </div>   				
                                                        </div>

                                                        <div class="col-lg-12 mt-3 "> 
                                                            <div class="form-group">
                                                                <label for="" class="text-dark font-weight-bold">Jenis Kegiatan </label>
                                                                <input type="text" readonly class="bg-gray form-style " id="dtl-jeniskegiatan">
                                                            </div>   				
                                                        </div>

                                                        <div class="col-lg-12 mt-3 "> 
                                                            <div class="form-group">
                                                                <label for="" class="text-dark font-weight-bold">Total Biaya & Kode Unik</label>
                                                                <input type="text" readonly class="bg-gray form-style " id="dtl-totalbiaya">
                                                            </div>   				
                                                        </div>
                                                    
                                                        <div class="col-lg-12 mt-3 "> 
                                                            <div class="form-group choose-file colored-bg">
                                                                <input type="file" name="bukti-pembayaran" class="form-file" accept="image/*" required>
                                                                <label>Pilih File Bukti Transfer atau Tarik Kesini</label>
                                                                <small class="text-danger">*Format file .JPG, .JPEG, .PNG</small> <br>
                                                                <small class="text-danger" style="margin-top: -10px;">*Maksimal besar file 1Mb</small> 
                                                            </div>             
                                                        </div>  

                                                        <div class="col-md-12 mt-3 "> 
                                                            <button type="submit" class="btn text-white p-3 btn-fluid" style="background-color:#891d13;">
                                                                <i class="uil uil-user-plus size-20 mr-2"></i>UPLOAD BUKTI PEMBAYARAN
                                                            </button>
                                                        </div>
                        
                                                    </div>
                                                </form>
										</div>
									</div>
								    </div>
                                </div>

                                {{-- Notifikasi Ulang --}}
                                <div id="notifikasi-email" class="collapse p-0" data-parent="#accordionExample">
									<div class="card-body p-0">
										<div class="row">
											<div class="col-12">
												<h5 class="mb-3">
													Notifikasi Ulang Email
												</h5>
											</div>
											<div class="col-12 pb-3">
												<div class="section divider divider-gray"></div>
											</div>
											<div class="col-12">
                                                <form action="#" id="form-notifikasi-ulang">
                                                    @csrf
                                                    <div class="row justify-content-center">
                                                        
                                                        <div class="col-lg-12 mt-3 "> 
                                                            <div class="form-group">
                                                                <label for="" class="text-dark font-weight-bold">Email</label>
                                                                <input type="email" name="email" id="notif_email"  class="form-style " autocomplete="off" required>
                                                            </div>   				
                                                        </div>
                                                    
                                                        <div class="col-md-12 mt-3 "> 
                                                            <button type="submit" class="btn text-white p-3 btn-fluid" style="background-color:#891d13;">
                                                                <i class="uil uil-user-plus size-20 mr-2"></i>NOTIFIKASI ULANG
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>    
										    </div>
									    </div>
								    </div>
                                </div>
																
								<div id="collapseFive" class="collapse p-0" data-parent="#accordionExample">
									<div class="card-body p-0">
										<div class="row">
											<div class="col-12">
												<h5 class="mb-3">
													Troubleshoot account issues
												</h5>
											</div>
											<div class="col-12 pb-3">
												<div class="section divider divider-gray"></div>
											</div>
											<div class="col-12">
												<h6 class="mb-3">
													At vero eos et accusamus
												</h6>
												<p class="mb-1">
													Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.
												</p>
												<p class="mb-4">
													Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. 
												</p>
												<h6 class="mb-3">
													Similique sunt in culpa
												</h6>
												<p class="mb-4">
													Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. 
												</p>
												<h6 class="mb-3">
													Nam libero tempore
												</h6>
												<p class="mb-1">
													Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. 
												</p>
												<p class="mb-4">
													Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.
												</p>
												<h6 class="mb-3">
													Similique sunt in culpa
												</h6>
												<p class="mb-0">
													Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. 
												</p>
											</div>
										</div>
									</div>
								</div>
								
								<div id="collapseSix" class="collapse p-0" data-parent="#accordionExample">
									<div class="card-body p-0">
										<div class="row">
											<div class="col-12">
												<h5 class="mb-3">
													Issues using tools
												</h5>
											</div>
											<div class="col-12 pb-3">
												<div class="section divider divider-gray"></div>
											</div>
											<div class="col-12">
												<h6 class="mb-3">
													At vero eos et accusamus
												</h6>
												<p class="mb-1">
													Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.
												</p>
												<p class="mb-4">
													Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. 
												</p>
												<h6 class="mb-3">
													Similique sunt in culpa
												</h6>
												<p class="mb-4">
													Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. 
												</p>
												<h6 class="mb-3">
													Nam libero tempore
												</h6>
												<p class="mb-1">
													Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. 
												</p>
												<p class="mb-0">
													Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.
												</p>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
        
    </div>

@endsection        