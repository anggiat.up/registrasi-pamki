@extends('layout.mainbody')

@section('container')

		<!-- Hero Slider -->
		<div class="section pb-4 padding-top-120 padding-top-mob-nav mt-xl-3 px-lg-2">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<div class="section  border-4 bg-white section-shadow-blue">
							<div class="section swiper-hero-corporate-v2">
								<div class="swiper-wrapper">
									<div class="swiper-slide">
										<div class="section  padding-top-bottom-120 px-3 text-center corporate-hero-back-img-5">
											<div class="section ">
												<h2 class="display-1 color-light-2 mb-3 text-center" style="color: #891d13">
													PIT PAMKI 2024
												</h2>
												<h4 class="mb-3" style="color: #000000">
													"Enchancing Integrated Infectious Disease Management through the <br>Emphasis on Accurate Laboratory Diagnosis and Sustained Surveillance" 
												</h4>
                                                <h5 style="font-size: 30px;font-weight:800;color:#7b2c25;" class=" mb-4">
                                                    {{-- <b class="bg-primary"> --}}
                                                        6 - 8 September 2024 <br>
                                                        Hotel Pangeran Pekanbaru - Riau
                                                    {{-- </b> --}}
                                                </h5>

                                                <div class="countdown">
                                                    <b class="">
                                                    
                                                    </b>
                                                </div>
                                                <div class="section padding-bottom-80"></div>
												<div class=" col-4 offset-4 row justify-content-center"> 
                                                    <a href="{{ url('sertifikat') }}" class="btn text-white p-3 btn-fluisd mt-1 mr-lg-1" style="background-color:#891d13;font-size:16px">
                                                        <i class="uil uil-user-plus size-20 mr-2"></i>GENERATE CERTIFICATE
                                                    </a>              
                                                    {{-- <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-pengumuman"  class="btn text-white p-3 btn-fluisd mt-1 ml-lg-1" style="background-color:#a66507;font-size:16px">
                                                        <i class="uil uil-megaphone size-20 mr-2"></i>PENGUMUMAN
                                                    </a>               --}}
                                                </div>
                                                
											</div>
										</div>
									</div>
                                    <div class="swiper-slide">
										<div class="section over-hide padding-top-bottom-120 px-3 text-center corporate-hero-back-img-5">                                            
											<div class="section parallax-fade-hero-short">
												<h2 class="display-1 color-light-2 mb-3 text-center text-primary">
													PIT PAMKI 2024 {{-- <span class="color-primary">.</span> --}}
												</h2>
												<h4 class="mb-3">
													"Enchancing Integrated Infectious Disease Management through the <br>Emphasis on Accurate Laboratory Diagnosis and Sustained Surveillance"                                                                                                        
												</h4>
                                                
                                                <a href="#page-section" class="btn btn-dark" data-gal='m_PageScroll2id'>Registrasi<i class="uil uil-arrow-down size-22 ml-2"></i></a>
												<div class="section padding-bottom-80"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<!-- Logo section Untuk Sponsor -->
		<div class="section over-hide   pb-4 px-lg-2" id="page-section">
			<div class="container-fluid">
				{{-- <div class="row">
					<div class="col-12"> --}}
						{{-- <div class="section over-hide border-4 section-shadow-blue  " style="background-color: #ffffff"> --}}
							<div class="row justify-content-center bg-white">
								<div class="col-12 over-hide  pt-5">
									<div class="section swiper-logos">
										<div class="swiper-wrapper ">
											<div class="swiper-slide">
												<div class="section logos-wrap-1 text-center margin-auto">
													<img src="img/sponsor/sponsor1.png" alt="">
												</div>
											</div>
											<div class="swiper-slide">
												<div class="section logos-wrap-1 text-center margin-auto">
													<img src="img/sponsor/sponsor2.png" alt="" >
												</div>
											</div>
											<div class="swiper-slide">
												<div class="section logos-wrap-1 text-center margin-auto">
													<img src="img/sponsor/sponsor3.png" alt="">
												</div>
											</div>
											<div class="swiper-slide">
												<div class="section logos-wrap-1 text-center margin-auto">
													<img src="img/sponsor/sponsor4.png" alt="">
												</div>
											</div>
											<div class="swiper-slide">
												<div class="section logos-wrap-1 text-center margin-auto">
													<img src="img/sponsor/sponsor5.png" alt="">
												</div>
											</div>
											<div class="swiper-slide">
												<div class="section logos-wrap-1 text-center margin-auto">
													<img src="img/sponsor/sponsor6.png" alt="">
												</div>
											</div>
											<div class="swiper-slide">
												<div class="section logos-wrap-1 text-center margin-auto">
													<img src="img/sponsor/sponsor7.png" alt="">
												</div>
											</div>
											<div class="swiper-slide">
												<div class="section logos-wrap-1 text-center margin-auto">
													<img src="img/sponsor/sponsor8.png" alt="">
												</div>
											</div>
											<div class="swiper-slide">
												<div class="section logos-wrap-1 text-center margin-auto">
													<img src="img/sponsor/sponsor9.png" alt="">
												</div>
											</div>
											<div class="swiper-slide">
												<div class="section logos-wrap-1 text-center margin-auto">
													<img src="img/sponsor/sponsor10.jpg" alt="">
												</div>
											</div>
											<div class="swiper-slide">
												<div class="section logos-wrap-1 text-center margin-auto">
													<img src="img/sponsor/sponsor11.png" alt="">
												</div>
											</div>
											<div class="swiper-slide">
												<div class="section logos-wrap-1 text-center margin-auto">
													<img src="img/sponsor/sponsor12.png" alt="">
												</div>
											</div>
											<div class="swiper-slide">
												<div class="section logos-wrap-1 text-center margin-auto">
													<img src="img/sponsor/sponsor13.png" alt="">
												</div>
											</div>
											<div class="swiper-slide">
												<div class="section logos-wrap-1 text-center margin-auto">
													<img src="img/sponsor/sponsor14.png" alt="">
												</div>
											</div>
											<div class="swiper-slide">
												<div class="section logos-wrap-1 text-center margin-auto">
													<img src="img/sponsor/sponsor15.jpg" alt="">
												</div>
											</div>
											
										</div>
									</div>
								</div>
							</div>
						{{-- </div> --}}
					{{-- </div>
				</div> --}}
			</div>
		</div>

        
        {{-- Kata Pengantar --}}
        <div class="section pb-4 px-lg-2" id="kata-sambutan">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<div class="section over-hide border-4 bg-white section-shadow-blue padding-top-bottom-120 px-3 px-xl-5">
							<div class="row justify-content-center">
                                <div class="col-xl-12 text-center">
                                    <h2 class="mb-4 display-9">Kata Pengantar</h2>
                                    <h5 class="mb-0 font-weight-400 font-italic">
                                        Assalamualaikum wr. wb., <br>
                                        Salam sejawat, <br><br>
                                        Puji dan syukur kami panjatkan kehadirat Tuhan Yang Maha Kuasa atas berkah dan rahmatnya yang selalu melingkupi kita semua. Kami mengundang saudara untuk mengikuti Pertemuan Ilmiah Tahunan (PIT) PAMKI 2024 yang akan diselenggarakan di Pekanbaru, Provinsi Riau pada tanggal 6-8 September 2024. <br><br> PIT PAMKI kali ini mengangkat tema "Enhancing Integrated Infectious Disease Management through the Emphasis on Accurate Laboratory Diagnosis and Sustained Surveillance". Melalui kegiatan ini kami ingin menfokuskan pada pentingnya aspek diagnosis pada tata laksana penyakit infeksi secara keseluruhan. Para expert akan menyampaikan bagaimana memilih diagnosis penyakit infeksi yang tepat, akurat, terkini dan efisien yang akhirnya mendukung luaran pasien yang lebih baik. <br><br> Pada pertemuan ini juga akan didiskusikan bagaimana mengelola laboratorium yang bermutu dalam pelayanan pasien maupun dalam surveilans penyakit infeksi untuk mendukung penanggulangan penyakit infeksi di masyarakat. Serangkaian kegiatan ilmiah seperti simposium dan workshop, free paper, serta kegiatan yang mendukung kebersamaan akan diselenggarakan. Dokter spesialis mikrobiologi klinik, dokter spesialis lain, dokter umum, tenaga kesehatan lain termasuk peserta didik dapat berpartisipasi pada kegiatan ini. Kami mengharapkan sejawat sekalian dapat berpartisipasi pada keseluruhan acara PIT PAMKI 2024. <br><br>

                                        Sampai berjumpa di Pekanbaru
                                    </h5>
                                </div>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>


        {{-- Lokasi Acara --}}
        <div class="section over-hide mb-4">
			<div class="section px-2">
				<div class="container-fluid">
					<div class="row justify-content-center">
						<div class="col-12">
							<div class="section border-4 over-hide padding-top-bottom-120 section-background-5">
								<div class="background-dark-blue-over"></div>
								<div class="section z-bigger px-3">
									<div class="row justify-content-center">
										<div class="col-lg-5 offset-1 text-left">
											<h4 class="color-white">TEMPAT PENYELENGGARAAN</h4>
											<h5 class="mb-4 color-white">Hotel Pangeran Pekanbaru - Riau</h5>
                                            <img src="{{ asset('img/logo-pangeran.png') }}" alt="">
										</div>
										<div class="col-lg-5 text-left offset-1">
                                            <h4 class="text-white text-left"><b>ALAMAT</b></h4> 
                                            <a href="https://maps.app.goo.gl/f6DKhFeBT5DjS8678" target="_blank">
                                                <h5 class="text-white text-left"><b><i class="fa-solid fa-map-location-dot fa-lg "></i> Jl. Jend. Sudirman No.371-373, Cinta Raja, Kec. Sail, Kota Pekanbaru, Riau 28126</b></h5> 
                                            </a>
                                            <iframe class="text-left" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.6635727380744!2d101.45034407496472!3d0.5046315994904305!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31d5aebce81ef37f%3A0x9b837fcba8f106e9!2sHotel%20Pangeran%20Pekanbaru!5e0!3m2!1sid!2sid!4v1710651813916!5m2!1sid!2sid" width="100%" height="150" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

        {{-- Flier Workshop --}}
        <div class="section  padding-top-120  ">
			<div class="section-1400 pt-xl-4 ">
				<div class="container-fluid padding-bottom-80">
					<div class="row">
						<div class="col-12 over-hide">
							<div class="section  swiper-project-page-v6 padding-bottom-80">
								<div class="swiper-wrapper">
									<div class="swiper-slide">
										<a href="{{ asset('img/pamki-ws1.jpg') }}" data-fancybox="gallery">
											<div class="gallery-wrap over-hide border-4 img-wrap">
												<img src="{{ asset('img/pamki-ws1.jpg') }}" alt="">
												<div class="gallery-mask">
													<div class="gallery-icon">
														<i class="uil uil-plus size-22 color-white"></i>
													</div>
												</div>
											</div>
										</a>
									</div>
									<div class="swiper-slide">
										<a href="{{ asset('img/pamki-ws2.jpg') }}" data-fancybox="gallery">
											<div class="gallery-wrap over-hide border-4 img-wrap">
												<img src="{{ asset('img/pamki-ws2.jpg') }}" alt="">
												<div class="gallery-mask">
													<div class="gallery-icon">
														<i class="uil uil-plus size-22 color-white"></i>
													</div>
												</div>
											</div>
										</a>
									</div>
									
								</div>
								<div class="swiper-pagination"></div>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>

        {{-- Detail Acara --}}
		<div class="section pb-4 px-lg-2">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<div class="section over-hide border-4 bg-white section-shadow-blue px-3 px-xl-5" style="padding-top: 50px;padding-bottom:100px;">
							<div class="row">
								<div class="col-12 align-self-center mb-2 pb-4">
									<div class="row">
										<div class="col-auto align-self-center">
											<h6 class="color-gray-dark text-vertical text-uppercase mx-0 px-0">
												detail
											</h6>
										</div>
										<div class="col">
                                            <h2 class="mb-2 display-9">Materi & Acara</h2>
										</div>
									</div>
								</div>
								<div class="col-md-12 pr-xl-5" data-scroll-reveal="enter bottom move 40px over 0.5s after 0.3s">
                                    <p class="text-danger font-weight-bold">* Klik tanggal untuk melihat detail materi & acara</p>
                                    <div class="accordion" id="accordionExample">
                                        <div class="card">
                                            <div class="card-header " id="headingOne">
                                                <div class="btn-accordion collapsed show" role="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    <h6 class="mt-2">Jumat, 6 September 2024 </h6>
                                                </div>
                                            </div>
                                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                <div class="card-body">
                                                    <h6 class="mt-4">Agenda PIT PAMKI 2024 : Hari ke - 1</h6>                
                                                    <h6 class="mb-4">Detail Kegiatan Jumat, 6 September 2024 </h6>
                                                    <table class="table table-bordered" width="100%" >
                                                        <thead class="thead-dark text-white text-center">
                                                            <tr>
                                                                <th scope="col" width="30%">Waktu</th>
                                                                <th scope="col">Kegiatan</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="text-dark">
                                                            <tr >
                                                                <th class="text-center align-middle">08:00 - 16:00 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark"><b>Workshop 1 :</b> Quality Assurance in Bacterial Culture and Antimicrobial Susceptibility Test <br>
                                                                        <b>Workshop 2 :</b> Expertise Microbiology : Bridging from Bench to Word</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">19:00 - 22:00 WIB</th>
                                                                <td class="align-middle p-3">
                                                                    <p class="text-dark">Sidang Organisasi</p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headingTwo">
                                                <div class="btn-accordion collapsed" role="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                    <h6 class="mt-2">Sabtu, 7 September 2024 </h6>
                                                </div>
                                            </div>
                                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                                <div class="card-body">
                                                    <h6 class="mt-4">Agenda PIT PAMKI 2024 : Hari ke - 2</h6>                
                                                    <h6 class="mb-4">Detail Kegiatan Sabtu, 7 September 2024 </h6>
                                                    <table  class="table table-bordered" width="100%" >
                                                        <thead class="thead-dark text-white text-center">
                                                          <tr>
                                                            <th scope="col" width="30%">Waktu</th>
                                                            <th scope="col">Kegiatan</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody >
                                                            <tr>
                                                                <th class="text-center align-middle">07:00 - 08:00 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark">Registrasi</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">08:00 - 08:20 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark">Opening Ceremony</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">08:20 - 08:35 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark"> Sambutan Ketua Panitia <br>
                                                                      <b>Dr. dr. Dewi Anggraini Sp.MK(K)</b></p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">08:35 - 08:50 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark">Sambutan Ketua PP PAMKI <br>
                                                                      <b>Dr. dr. Anis Karuniawati, Ph.D, Sp.MK(K)</b></p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">08:50 - 09:00 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark">Sambutan ----</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Keynote Speech and Plenary Lecture</i></th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">09:00 - 09:20 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark"><b>Keynote Speech : </b> <br> 
                                                                        Strengthening Indonesia's Health Laboratory Network for Enhanced Surveillance and Diagnosis of Infectious Diseases, Promoting Early Detection and Comprehensive Disease Management <br>
                                                                      <b>Speaker :</b> dr. Maria Endang Sumiwi, MPH</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">09:20 - 09:40 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark"><b>Plenary Lecture : </b> <br> 
                                                                        Ethical and Legal Challenges in Laboratory Diagnostics <br>
                                                                      <b>Speaker :</b> Prof. Dr. dr. Dedi Afandi, SP. FM(K)</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">09:40 - 10:00 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark"><b>Plenary Lecture : </b> <br> 
                                                                        SINAR : PAMKI's Antimicrobial Resistance Surveilance System and Its Impact on Atimicrobial Resistance Control in Indonesia <br>
                                                                      <b>Speaker :</b> dr. Anis Karuniawati, Ph.D, Sp. MK(K)</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">10:00 - 10:15 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark">Kunjungan ke <i>exhibition</i> dan <i>coffe break</i></p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium I</i><br>
                                                                Theme : Management for MDR infection</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">10:15 - 10:35 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark"><b>Topic : </b>  
                                                                        Current Treatment Strategis Agains Multidrug-Resistant Gram-Negative Bacterial Infections in the Hospital Setting<br>
                                                                      <b>Speaker :</b> Prof. Dr. dr. Erni Juwita Nelwan, Ph.D, Sp. PD, K-PTI</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">10:35 - 10:55 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark"><b>Topic : </b>  
                                                                        Implementing Syndromic Testing for the Rapid Molecular Diagnosis of Multidrug-Resistant Organisms to Optimize the Rational Use of Antibiotics <br>
                                                                      <b>Speaker :</b> dr. I Wayan Agus Gede Manik Saputra, M.Med.Klin, Sp.M</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">10:55 - 11:15 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark">Diskusi dan Tanya Jawab</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">11:15 - 11:30 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark">Industrial Break (Sponsorship)</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">11:30 - 13:00 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark">Lunch Simposium I</p>
                                                                    <p class="text-dark"><b>Topic : </b> 
                                                                        Assessing the Effectiveness of Screening for Methicillin-Resistant Staphylococcus aureus (MRSA) in Infection Control Antimicrobial Stewardship<br>
                                                                      <b>Speaker :</b> Dr. dr. Dewi Anggraini Sp.MK(K)</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium II</i><br>
                                                                Theme : Advance infectious disease diagnostic assay</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">13:00 - 13:20 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark"><b>Topic : </b>  
                                                                        Indonesia's Strategy in Building Resilience and Self-Reliance in Healthcare Equipment<br>
                                                                      <b>Speaker :</b> Roy Himawan, S. Farm, Apt, MKM</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">13:20 - 13:40 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark"><b>Topic : </b>  
                                                                        A New Paradigm for Implementing Molecular Diagnostics in Combating Infectious Disease <br>
                                                                      <b>Speaker :</b> Dr. dr. Andani Eka Putra, Msc</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">13:40 - 14:00 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark">Diskusi dan Tanya Jawab</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">14:00 - 14:15 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark">Industrial Break (Sponsorship)</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium III</i><br>
                                                                Theme : Prevention and Control of infection</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">14:15 - 14:35 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark"><b>Topic : </b>  
                                                                        Reuse of Single Use Medical Devices : Clinical and Economic Outcomes, Legal and Ethical Issues, and Current Hospital Practice <br>
                                                                      <b>Speaker :</b> Juliette Severin, MD, Ph.D</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">14:35 - 14:55 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark"><b>Topic : </b>  
                                                                        Singe-dose Prophylactic Antibiotic Use : Why is it Still Difficult to Implement in Indonesia ? <br>
                                                                      <b>Speaker :</b> Dr. dr. Harry Prathon, Sp.OG(K)</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">14:55 - 15:15 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark">Diskusi dan Tanya Jawab</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">15:15 - 15:30 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark">Industrial Break (Sponsorship)</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle" rowspan="2">15:30 - 15:45 WIB</th>
                                                                <td class="p-3">
                                                                    <p class="text-dark"><b>Penutupan Simposium Hari I</b></p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="p-3">
                                                                    <p class="text-dark"><b>Closing Remarks Day 1</b></p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="headingThree">
                                                <div class="btn-accordion collapsed" role="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                    <h6 class="mt-2">Minggu, 8 September 2024 </h6>
                                                </div>
                                            </div>
                                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                                <div class="card-body">
                                                    <h6 class="mt-4">Agenda PIT PAMKI 2024 : Hari ke - 3</h6>                
                                                    <h6 class="mb-4">Detail Kegiatan Minggu, 8 September 2024 </h6>

                                                    <table  class="table table-sm" width="100%" border="1">
                                                        <thead class="thead-dark text-white text-center">
                                                          <tr>
                                                            <th scope="col" width="30%">Waktu</th>
                                                            <th scope="col">Kegiatan</th>
                                                          </tr>
                                                        </thead>
                                                        <tbody class="text-dark">
                                                            <tr>
                                                                <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium IV</i><br>
                                                                Theme : Update in Tuberculosis Management</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">07:00 - 08:00 WIB</th>
                                                                <td>
                                                                    <p class="text-dark">Meet The Expert</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">08:00 - 08:20 WIB</th>
                                                                <td>
                                                                    <p class="text-dark"><b>Topic : </b>  
                                                                        Update Diagnosiss TB : New Diagnostic Tools<br>
                                                                      <b>Speaker :</b> Roni Chandra, S.Si, M.Biomed</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">08:20 - 08:40 WIB</th>
                                                                <td>
                                                                    <p class="text-dark"><b>Topic : </b>  
                                                                        Update Management of MDR TB in Indonesia<br>
                                                                      <b>Speaker :</b> dr. Indra Yovi, Sp.P(K)</p>
                                                                </td>
                                                            </tr>                        
                                                            <tr>
                                                                <th class="text-center align-middle">08:40 - 09:00 WIB</th>
                                                                <td>
                                                                    <p class="text-dark">Diskusi dan Tanya Jawab</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">09:00 - 09:15 WIB</th>
                                                                <td>
                                                                    <p class="text-dark">Industrial Break (Sponsorship)</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">09:15 - 09:30 WIB</th>
                                                                <td>
                                                                    <p class="text-dark">Coffe Break </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium V</i><br>
                                                                Theme : Emerging and Neglected Infetious Diseases : Insights, Advances, and Challenges</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">09:30 - 09:50 WIB</th>
                                                                <td>
                                                                    <p class="text-dark"><b>Topic : </b>  
                                                                        Diagnosis and Treatment Strategies for Fastidious Pathogens in Pediatric Cases <br>
                                                                      <b>Speaker :</b> dr. Helmia Farida, M.Ked, Ph.D, Sp.A (K)</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">09:50 - 10:10 WIB</th>
                                                                <td>
                                                                    <p class="text-dark"><b>Topic : </b>  
                                                                        New Insights into Diagnosing Fastidious Bacteria <br>
                                                                      <b>Speaker :</b> Prof. Dr. Zakuan Zainy Bin Deris</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">10:10 - 10:30 WIB</th>
                                                                <td>
                                                                    <p class="text-dark">Diskusi dan Tanya Jawab</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">10:30 - 10:45 WIB</th>
                                                                <td>
                                                                    <p class="text-dark">Industrial Break (Sponsorship)</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium VI</i><br>
                                                                Theme : Viral Disease Management</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">10:45 - 11:05 WIB</th>
                                                                <td>
                                                                    <p class="text-dark"><b>Topic : </b>  
                                                                        Best Practices in Antifungal Management and WHO fungal Priority Pathogens List <br>
                                                                      <b>Speaker :</b> Dr. Jasmine Chung Shimin</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">11:05 - 11:25 WIB</th>
                                                                <td>
                                                                    <p class="text-dark"><b>Topic : </b>  
                                                                        Key Initial Steps When Processing and Direct Examintaion of Clinical Specimens for Fungal Examination <br>
                                                                      <b>Speaker :</b> dr. Dian Dwi Wahyuni, Sp.MK (K)</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">11:25 - 11:45 WIB</th>
                                                                <td>
                                                                    <p class="text-dark">Diskusi dan Tanya Jawab</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">11:45 - 12:00 WIB</th>
                                                                <td>
                                                                    <p class="text-dark">Industrial Break (Sponsorship)</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">12:00 - 13:00 WIB</th>
                                                                <td>
                                                                    <p class="text-dark">Lunch Simposium II</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium VII</i><br>
                                                                Theme : Fungal Infectious disease management</th>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">13:00 - 13:20 WIB</th>
                                                                <td>
                                                                    <p class="text-dark"><b>Topic : </b>  
                                                                        Effect of Antimicrobial Stewardship with Rapid MALDI-TOF Identification on Hospitalization Outcome <br>
                                                                      <b>Speaker :</b> Juliette Severin, MD, Ph.D</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">13:20 - 13:40 WIB</th>
                                                                <td>
                                                                    <p class="text-dark"><b>Topic : </b>  
                                                                        Antimicrobial Stewardship in Immunocumpromised Patients <br>
                                                                      <b>Speaker :</b> Prof. Jan Nouwen, MD, Ph.D</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">13:40 - 14:00 WIB</th>
                                                                <td>
                                                                    <p class="text-dark">Diskusi dan Tanya Jawab</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle">14:00 - 14:15 WIB</th>
                                                                <td>
                                                                    <p class="text-dark">Industrial Break (Sponsorship)</p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <th class="text-center align-middle" rowspan="2">14:15 - 14:30 WIB</th>                            
                                                                <td>
                                                                    <p class="text-dark"><b>Closing Ceremony</b></p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

        {{-- Detail Harga --}}
        <div class="section pb-4 px-lg-2">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<div class="section over-hide border-4 bg-white section-shadow-blue px-3 px-xl-5" style="padding-top: 50px;padding-bottom:100px;">
							<div class="row">
								<div class="col-12 align-self-center pb-4">
									<div class="row">
										<div class="col-auto align-self-center">
											<h6 class="color-gray-dark text-vertical text-uppercase mx-0 px-0">
												detail
											</h6>
										</div>
										<div class="col">
											<h3 class="display-9 mb-0">
												Biaya Registrasi
											</h3>
										</div>
									</div>
								</div>
								<div class="col-md-12 pr-xl-5 table-responsive" data-scroll-reveal="enter bottom move 40px over 0.5s after 0.3s">
                                    <p class="text-danger">* Scroll kekanan untuk melihat semua harga</p>
									<table class="table " width="100%" style="">
                                        <thead class="text-white" style="background-color:#891d12">
                                            <tr>
                                              <td class="text-center align-middle font-weight-bold" rowspan="2" width="20%">Time</td>
                                              <td class="text-center font-weight-bold" colspan="4" style="padding: 20px">National Participant</td>
                                              <td class="text-center font-weight-bold" colspan="2" style="padding: 20px">International Participant</td>
                                            </tr>
                                            <tr>
                                              <td class="text-center align-middle font-weight-bold" width="12%" >Specialist PAMKI Member</td>
                                              <td class="text-center align-middle font-weight-bold">Specialist Non-PAMKI Member</td>
                                              <td class="text-center align-middle font-weight-bold" width="12%">Other</td>
                                              <td class="text-center align-middle font-weight-bold">Clinical Microbiology Resident</td>
                                              <td class="text-center align-middle font-weight-bold">Consultant</td>
                                              <td class="text-center align-middle font-weight-bold" width="12%">Trainee</td>
                                            </tr>
                                        </thead>
                                        <tbody class="text-dark font-weight-bold">
                                            <tr>
                                                <td class="text-center p-1 text-white" style="background-color: #a66507;" colspan="7"><i>Workshop</i></td>
                                            </tr>
                                            <tr>
                                                <td class="align-middle p-3" ></td>
                                                <td class="align-middle text-center p-3">IDR 1,000K</td>
                                                <td class="align-middle text-center">IDR 1,000K</td>
                                                <td class="align-middle text-center">IDR 1,000K</td>
                                                <td class="align-middle text-center">IDR 1,000K</td>
                                                <td class="align-middle text-center">N/A</td>
                                                <td class="align-middle text-center">N/A</td>
                                                </tr>
                                            <tr>
                                                <td class="text-center p-1 text-white" style="background-color: #a66507;" colspan="7"><i>Simposium</i></td>
                                            </tr>
                                            <tr>
                                            <td class="align-middle p-3" >Early Registration <br> (Until 30 April 2024)</td>
                                            <td class="align-middle text-center">IDR 1,000K</td>
                                            <td class="align-middle text-center">IDR 1,500K</td>
                                            <td class="align-middle text-center">IDR 800K</td>
                                            <td class="align-middle text-center">IDR 750K</td>
                                            <td class="align-middle text-center">US $150K</td>
                                            <td class="align-middle text-center">US $75K</td>
                                            </tr>
                                            <tr>
                                            <td class="align-middle p-3">Mid Registration </td>
                                            <td class="align-middle text-center">IDR 1,200K</td>
                                            <td class="align-middle text-center">IDR 1,800K</td>
                                            <td class="align-middle text-center">IDR 1,000K</td>
                                            <td class="align-middle text-center">IDR 850K</td>
                                            <td class="align-middle text-center">US $200</td>
                                            <td class="align-middle text-center">US $85</td>
                                            </tr>
                                            <tr>
                                            <td class="align-middle p-3">On-Site</td>
                                            <td class="align-middle text-center">IDR 1,500K</td>
                                            <td class="align-middle text-center">IDR 2,000K</td>
                                            <td class="align-middle text-center">IDR 1,200K</td>
                                            <td class="align-middle text-center">IDR 1,000K</td>
                                            <td class="align-middle text-center">US $250</td>
                                            <td class="align-middle text-center">US $100</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <small class="text-danger mb-4 font-weight-bold">*Paket Workshop + Symposium Get Disc 10%</small>

								</div>
                                <div class="col-md-12 text-center">  
                                    <a href="{{ url('pendaftaran') }}"  type="button" class="btn  btn-fluid p-4 text-white animsition-link" style="background-color:#891d12">
                                        <i class="uil uil-navigator size-20 mr-2"></i>REGISTRASI PIT PAMKI 2024
                                    </a>             
                                </div>
                                
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
        
        <style>
            .equal-height-col {
                display: flex;
                flex-direction: column;
            }

            .equal-height-col .room-wrap {
                flex: 1;
            }
        </style>

        {{-- Akomodasi --}}
        <div class="section pb-4 px-lg-2 d-none" id="akomodasi-hotel">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<div class="section over-hide border-4 bg-white section-shadow-blue  px-3 px-xl-5" style="padding-top: 50px;padding-bottom:80px;">
							<div class="row">
								<div class="col-12 align-self-center  pb-4">
									<div class="row">
										<div class="col-auto align-self-center">
											<h6 class="color-gray-dark text-vertical text-uppercase mx-0 px-0">
												services
											</h6>
										</div>
										<div class="col">
											<h3 class="display-9 mb-0">
												Akomodasi
											</h3>
										</div>
										
									</div>
								</div>
								<div class="col-12">
									<div class="row">
										<div class="col-xl-12">
											<div class="row">
                                                
                                                @foreach ($hotel as $item)
                                                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 mb-4 equal-height-col" data-scroll-reveal="enter bottom move 40px over 0.5s after 0.3s">
                                                    <div class="section over-hide border-4 room-wrap bg-white img-wrap">
                                                        <img src="{{ asset('gambar-hotel/' . $item['gambar_hotel']) }}" alt="">                                                        
                                                        <div class="room-wrap-stars px-2 border-4">
                                                            @for($i = 1; $i <= $item['rate_bintang']; $i++)
                                                                @if($i == $item['rate_bintang'])
                                                                    <i class="uil uil-star size-12"></i>
                                                                @else
                                                                    <i class="uil uil-star size-12 mr-1"></i>
                                                                @endif
                                                            @endfor
                                                        </div>
                                                        <div class="section p-3 p-xl-4 section-background-21">
                                                            <div class="row align-items-center">
                                                                <div class="col align-self-center text-dark">
                                                                    <h5 class="mb-0 text-center">
                                                                        {{ $item['nama_hotel'] }}
                                                                    </h5>
                                                                    {{-- <p>Jl. Jend Sudirman No. 389, Pekanbaru, Provinsi Riau</p> --}}
                                                                </div>
                                                            </div>                                                                
                                                            <div class="section divider mt-3 mt-xl-4"></div>
                                                            <div class="row pt-3 pt-xl-4 text-dark">                                                                    
                                                                <a href="javascript:void(0);" data-idhotel="{{ $item['hotelid'] }}"  class="btn btn-dark-primary btn-fluid reservasi-modal">Reservasi</a>                                                                     
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                @endforeach
                                {{--                                                 
                                                <div class="col-md-3 mb-4" data-scroll-reveal="enter bottom move 40px over 0.5s after 0.3s">
                                                    <div class="section over-hide border-4 room-wrap bg-white img-wrap">
                                                        <img src="img/akomodasi-2.jpg" alt="">                                                        
                                                        <div class="room-wrap-stars px-2 border-4">
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12"></i>
                                                        </div>
                                                        <div class="section p-3 p-xl-4 section-background-21">
                                                            <div class="row">
                                                                <div class="col align-self-center text-dark">
                                                                    <h5 class="mb-0">
                                                                        Hotel Grand Central
                                                                    </h5>
                                                                    Jl. Jend Sudirman No 1 – Pku
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="section divider mt-3 mt-xl-4"></div>
                                                            <div class="row pt-3 pt-xl-4">
                                                                <a href="#" class="btn btn-dark-primary btn-fluid">Reservasi</a> 
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-4" data-scroll-reveal="enter bottom move 40px over 0.5s after 0.3s">
                                                    <div class="section over-hide border-4 room-wrap bg-white img-wrap">
                                                        <img src="img/akomodasi-3.jpg" alt="">                                                        
                                                        <div class="room-wrap-stars px-2 border-4">
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12"></i>
                                                        </div>
                                                        <div class="section p-3 p-xl-4 section-background-21">
                                                            <div class="row">
                                                                <div class="col align-self-center text-dark">
                                                                    <h5 class="mb-0">
                                                                        Hotel The Premiere
                                                                    </h5>
                                                                    Jl. Jend Sudirman No 1 – Pku
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="section divider mt-3 mt-xl-4"></div>
                                                            <div class="row pt-3 pt-xl-4">
                                                                <a href="#" class="btn btn-dark-primary btn-fluid">Reservasi</a> 
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-4" data-scroll-reveal="enter bottom move 40px over 0.5s after 0.3s">
                                                    <div class="section over-hide border-4 room-wrap bg-white img-wrap">
                                                        <img src="img/akomodasi-4.jpg" alt="">                                                        
                                                        <div class="room-wrap-stars px-2 border-4">
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12"></i>
                                                        </div>
                                                        <div class="section p-3 p-xl-4 section-background-21">
                                                            <div class="row">
                                                                <div class="col align-self-center text-dark">
                                                                    <h5 class="mb-0">
                                                                        Hotel Grand Jatra
                                                                    </h5>
                                                                    Jl. Jend Sudirman No 1 – Pku
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="section divider mt-3 mt-xl-4"></div>
                                                            <div class="row pt-3 pt-xl-4">
                                                                <a href="#" class="btn btn-dark-primary btn-fluid">Reservasi</a> 
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-4" data-scroll-reveal="enter bottom move 40px over 0.5s after 0.3s">
                                                    <div class="section over-hide border-4 room-wrap bg-white img-wrap">
                                                        <img src="img/akomodasi-5.jpg" alt="">                                                        
                                                        <div class="room-wrap-stars px-2 border-4">
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12"></i>
                                                        </div>
                                                        <div class="section p-3 p-xl-4 section-background-21">
                                                            <div class="row">
                                                                <div class="col align-self-center text-dark">
                                                                    <h5 class="mb-0">
                                                                        Hotel Aryaduta
                                                                    </h5>
                                                                    Jl. Jend Sudirman No 1 – Pku
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="section divider mt-3 mt-xl-4"></div>
                                                            <div class="row pt-3 pt-xl-4">
                                                                <a href="#" class="btn btn-dark-primary btn-fluid">Reservasi</a> 
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-4" data-scroll-reveal="enter bottom move 40px over 0.5s after 0.3s">
                                                    <div class="section over-hide border-4 room-wrap bg-white img-wrap">
                                                        <img src="img/akomodasi-6.jpg" alt="">                                                        
                                                        <div class="room-wrap-stars px-2 border-4">
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12"></i>
                                                        </div>
                                                        <div class="section p-3 p-xl-4 section-background-21">
                                                            <div class="row">
                                                                <div class="col align-self-center text-dark">
                                                                    <h5 class="mb-0">
                                                                        Hotel Novotel
                                                                    </h5>
                                                                    Jl. Jend Sudirman No 1 – Pku
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="section divider mt-3 mt-xl-4"></div>
                                                            <div class="row pt-3 pt-xl-4">
                                                                <a href="#" class="btn btn-dark-primary btn-fluid">Reservasi</a> 
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-4" data-scroll-reveal="enter bottom move 40px over 0.5s after 0.3s">
                                                    <div class="section over-hide border-4 room-wrap bg-white img-wrap">
                                                        <img src="img/akomodasi-7.jpg" alt="">                                                        
                                                        <div class="room-wrap-stars px-2 border-4">
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12"></i>
                                                        </div>
                                                        <div class="section p-3 p-xl-4 section-background-21">
                                                            <div class="row">
                                                                <div class="col align-self-center text-dark">
                                                                    <h5 class="mb-0">
                                                                        Hotel The Zuri
                                                                    </h5>
                                                                    Jl. Jend Sudirman No 1 – Pku
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="section divider mt-3 mt-xl-4"></div>
                                                            <div class="row pt-3 pt-xl-4">
                                                                <a href="#" class="btn btn-dark-primary btn-fluid">Reservasi</a> 
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 mb-4" data-scroll-reveal="enter bottom move 40px over 0.5s after 0.3s">
                                                    <div class="section over-hide border-4 room-wrap bg-white img-wrap">
                                                        <img src="img/akomodasi-2.jpg" alt="">                                                        
                                                        <div class="room-wrap-stars px-2 border-4">
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12 mr-1"></i>
                                                            <i class="uil uil-star size-12"></i>
                                                        </div>
                                                        <div class="section p-3 p-xl-4 section-background-21">
                                                            <div class="row">
                                                                <div class="col align-self-center text-dark">
                                                                    <h5 class="mb-0">
                                                                        Hotel Cempaka Syariah
                                                                    </h5>
                                                                    Jl. Jend Sudirman No 1 – Pku
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="section divider mt-3 mt-xl-4"></div>
                                                            <div class="row pt-3 pt-xl-4">
                                                                <a href="#" class="btn btn-dark-primary btn-fluid">Reservasi</a> 
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> --}}
											                                                
											</div>								
										</div>
									</div>							
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

        {{-- Lomba Abstract --}}
        <div class="section pb-4 px-lg-2 d-none" id="call-for-abstract">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<div class="section over-hide border-4 bg-white section-shadow-blue  px-3 px-xl-5" style="padding-top: 50px;padding-bottom:100px;">
							<div class="row">
								<div class="col-12 align-self-center pb-4">
									<div class="row">
										<div class="col-auto align-self-center">
											<h6 class="color-gray-dark text-vertical text-uppercase mx-0 px-0">
												event
											</h6>
										</div>
										<div class="col">
											<h3 class="display-9 mb-0">
												Call for Abstract
											</h3>
										</div>
										
									</div>
								</div>
								<div class="col-12">
									<div class="row px-0 justify-content-center">
                                        <div class="col-lg-8 col-xl-5 px-0 img-wrap mb-5 mb-xl-0">
                                            <img src="img/lomba-poster.png" width="150px" alt="">
                                        </div>
                                        <div class="col-sm-11 col-md-10 col-lg-8 col-xl-6 px-0 align-self-center mb-5 mb-xl-0 pb-4 pb-xl-0 order-xl-first" data-scroll-reveal="enter left move 40px over 0.5s after 0.3s">
                                            <div class="row justify-content-center">
                                                <div class="col-11 col-lg-11 col-xl-12">
                                                    <h4 class="mb-3">
                                                        ABSTRACT "Annual Scientific Meeting of PAMKI 2024"
                                                    </h4>
                                                    <p class=" text-dark">
                                                        <b>Abstract Submission Deadline Extended : <u>30 June 2024</u></b>  <br>
                                                        Submission Requirements: 
                                                        <ol class="text-dark">
                                                            <li>You must complete your registration before submitting an abstract. If you have not yet registered, please visit the Online Registration page .</li>
                                                            <li>The work displayed is original work and free of plagiarism.</li>
                                                            <li>Format of scientific work that can be included : 
                                                                <ul>
                                                                    <li>Case presentation/case report</li>
                                                                    <li>Evidence-based Case Report (EBCR)</li>
                                                                    <li>Original article</li>
                                                                    <li>Systematic review</li>
                                                                    <li>Meta-analysis</li>
                                                                </ul>
                                                            </li>                                                            
                                                            <li>All abstracts must be submitted in English</li>                                                                
                                                        </ol>

                                                        <b class="text-dark">Read full terms and conditions...</b>

                                                    </p>
                                                    <button type="button" data-toggle="modal" data-target="#modal-callabstract" class="btn btn-dark mt-3">
                                                        Persyaratan<i class="uil uil-file-alt size-22 "></i>
                                                    </button>
                                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-3"  class="btn btn-dark mt-3">Daftar Lomba<i class="uil uil-focus-add size-22 "></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>							
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    

        {{-- Kuliner Pekanbaru --}}
        <div class="section pb-4 px-lg-2" id="page-top1">
			<div class="section  padding-top-bottom-120 bg-white ">
				<div class="section-1400">
					<div class="container-fluid ">
						
						
							<div class="col-12 ">
								<div class="section swiper-project-page-v5 padding-bottom-80 ">
                                    <h2 class="display-9 mb-0 pb-4 text-center">
                                        Kuliner di Pekanbaru
                                    </h2>

									<div class="swiper-wrapper">
										
                                        @for ($i = 1; $i <=8; $i++)
                                        <div class="swiper-slide">
											<a href="{{ asset('img/kuliner' . $i . '.jpg') }}" data-fancybox="gallery">
												<div class="gallery-wrap over-hide border-4 img-wrap">
													<img src="{{ asset('img/kuliner' . $i . '.jpg') }}" alt="">
													<div class="gallery-mask">
														<div class="gallery-icon">
															<i class="uil uil-plus size-22 color-white"></i>
														</div>
													</div>
												</div>
											</a>
										</div>
                                        @endfor
                                        
										
									</div>
									<div class="swiper-pagination"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> 

			<!-- Testimonials section
			================================================== -->

			<!-- section
			================================================== -->

		</div>

        {{-- Kuliner Pekanbaru --}}
        <div class="section pb-4 px-lg-2" id="page-top2">
			<div class="section  padding-top-bottom-120 bg-white ">
				<div class="section-1400">
					<div class="container-fluid ">
						
						
							<div class="col-12 ">
								<div class="section swiper-project-page-v5 padding-bottom-80 ">
                                    <h2 class="display-9 mb-0 pb-4 text-center">
                                        Destinasi Wisata
                                    </h2>

									<div class="swiper-wrapper">
										
                                        @for ($i = 1; $i <=10; $i++)
                                        <div class="swiper-slide">
											<a href="{{ asset('img/Destinasi wisata_page-' . $i . '.jpg') }}" data-fancybox="gallery">
												<div class="gallery-wrap over-hide border-4 img-wrap">
													<img src="{{ asset('img/Destinasi wisata_page-' . $i . '.jpg') }}" alt="">
													<div class="gallery-mask">
														<div class="gallery-icon">
															<i class="uil uil-plus size-22 color-white"></i>
														</div>
													</div>
												</div>
											</a>
										</div>
                                        @endfor
                                        
										
									</div>
									<div class="swiper-pagination"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> 

			<!-- Testimonials section
			================================================== -->

			<!-- section
			================================================== -->

		</div>

        

	

		<!-- Logo Pamki
		================================================== -->

		{{-- <div class="section pb-4 px-lg-2">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<div class="section over-hide border-4 bg-white section-shadow-blue padding-top-bottom-80 px-3 px-xl-5">
							<div class="row justify-content-center">
								<div class="col-md-6 col-lg-3 text-center border-counters-right">
									<div class="counter-big-num mb-0 font-weight-300"><span class="counter">783</span>+</div>
									<p class="lead mb-0 py-4 font-weight-600 position-relative z-bigger color-dark-blue">
										Completed Projects
									</p>
								</div>
								<div class="col-md-6 col-lg-3 mt-5 mt-md-0 text-center border-counters-right">
									<div class="counter-big-num mb-0 font-weight-300"><span class="counter">37</span></div>
									<p class="lead mb-0 py-4 font-weight-600 position-relative z-bigger color-dark-blue">
										Availble Country
									</p>
								</div>
								<div class="col-md-6 col-lg-3 mt-5 mt-lg-0 text-center border-counters-right">
									<div class="counter-big-num mb-0 font-weight-300"><span class="counter">42</span>M</div>
									<p class="lead mb-0 py-4 font-weight-600 position-relative z-bigger color-dark-blue">
										User Worldwide
									</p>
								</div>
								<div class="col-md-6 col-lg-3 mt-5 mt-lg-0 text-center">
									<div class="counter-big-num mb-0 font-weight-300"><span class="counter">35</span></div>
									<p class="lead mb-0 py-4 font-weight-600 position-relative z-bigger color-dark-blue">
										Awards Winner
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> --}}

@endsection        