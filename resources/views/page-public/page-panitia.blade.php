@extends('layout.mainbody')

@section('container')

    <div class="section over-hide padding-top-120 padding-top-mob-nav section-background-24">	
        <div class="section-1400 pt-xl-4">
            <div class="container-fluid padding-top-bottom-80">
                <div class="row">
                    <div class="col-lg">
                        <h2 class="display-8 mb-3">
                            PIT PAMKI 2024
                        </h2>
                    </div>
                    <div class="col-lg-auto align-self-center mt-4 mt-lg-0">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb no-border">
                                <li class="breadcrumb-item"><a href="#" class="link link-dark-primary size-14" data-hover="Home">Home</a></li>
                                <li class="breadcrumb-item active font-weight-500" aria-current="page"><span class="size-14">Panitia</span></li>
                            </ol>
                        </nav>				
                    </div>
                </div>
            </div>
        </div>

        <div class="section pb-4 px-lg-2">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<div class="section over-hide border-4 bg-white section-shadow-blue padding-top-bottom-120 px-3 px-xl-5">
							<div class="row">
								<div class="col-12 align-self-center mb-5 pb-4">
									<div class="row">
										<div class="col-auto align-self-center">
											<h6 class="color-gray-dark text-vertical text-uppercase mx-0 px-0">
												detail
											</h6>
										</div>
										<div class="col">
											<h3 class="Susunan Panitia mb-0">
												Susunan Panitia
											</h3>
										</div>
									</div>
								</div>
								<div class="col-md-8 offset-md-2 pr-xl-5" data-scroll-reveal="enter bottom move 40px over 0.5s after 0.3s">
                                    
									<table class="table text-dark ">
                                        <tr>
                                            <td colspan="3"><b>PEMBINA</b></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>dr. Anis Karuniawati, Ph.D, Sp.MK(K)</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Dr. dr. Wani Devita Gunardi, Sp.MK(K)</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Prof. dr. Amin Soebandrio,Ph.D, Sp.MK(K)</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Prof. Dr. dr. Kuntaman, MS, Sp.MK(K)</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><b>KETUA</b></td>
                                            <td>:</td>
                                            <td> Dr. dr. Dewi Anggraini, Sp.MK(K)</td>
                                        </tr>
                                        <tr>
                                            <td><b>WAKIL KETUA</b></td>
                                            <td>:</td>
                                            <td> Dr. dr. Maya Savira, M.Kes</td>
                                        </tr>
                                        <tr>
                                            <td><b>SEKRETARIS</b> </td>
                                            <td>:</td>
                                            <td>dr. Dewi Yana, Sp.MK</td>
                                        </tr>
                                        <tr>
                                            <td><b>BENDAHARA</b> </td>
                                            <td>:</td>
                                            <td>dr. Fauzia Andrini, M.Kes</td>
                                        </tr>
                                        <tr>
                                            <td><b>SIE ACARA</b> </td>
                                            <td>:</td>
                                            <td>dr. Eckert Simata Uli Hutapea, Sp.MK</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>drh. Ghiandra NS Saukhan, M.Si</td>
                                        </tr>
                                        <tr>
                                            <td><b>SIE ILMIAH</b> </td>
                                            <td>:</td>
                                            <td> dr. Dian Oktavianti Putri, Sp.MK</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>drh. Rifia Tiara Fani, M.Sc</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>drh. Mulya Fitranda. AR,M.Sc</td>
                                        </tr>
                                        <tr>
                                            <td><b>SIE PERLENGKAPAN / SIE TRANSPORTASI</b> </td>
                                            <td>:</td>
                                            <td> dr. Surya Darma, Sp.MK</td>
                                        </tr>
                                        <tr>
                                            <td><b>SIE DOKUMENTASI</b> </td>
                                            <td>:</td>
                                            <td>dr. Irham Taslim, Sp.MK</td>
                                        </tr>
                                        <tr>
                                            <td><b>SIE AKOMODASI </b></td>
                                            <td>:</td>
                                            <td>dr. Evelyne Nida Tiotantra, Sp.MK</td>
                                        </tr>
                                        <tr>
                                            <td><b>SIE. HUMAS DAN PUBLIKASI</b> </td>
                                            <td>:</td>
                                            <td> drg. Rita Endriani, M.Kes</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Khikam</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Anggiat</td>
                                        </tr>
                                        <tr>
                                            <td><b>SEKRETARIAT ACARA</b></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><b>Narahubung</b> </td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="align-middle"><b>SPONSORSHIP</b> </td>
                                            <td class="align-middle">:</td>
                                            <td>&quot;Dr. dr. Dewi Anggraini, Sp.MK(K) <br> (Hp: 081280104689) <br> email: pamkicabangriau@gmail.com&quot;</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td><b>SEKRETARIAT</b></td>
                                            <td>:</td>
                                            <td>Sundari</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Febi</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Agnes Rica</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Mislinda</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Zizi</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Erla</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Ramadina</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td>Putri</td>
                                        </tr>
                                    </table>

								</div>
                               
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
        
    </div>

@endsection        