@extends('layout.mainbody')

@section('container')

    <div class="section over-hide padding-top-120 padding-top-mob-nav section-background-24">	
        <div class="section-1400 pt-xl-4">
            <div class="container-fluid padding-top-bottom-80">
                <div class="row">
                    <div class="col-lg">
                        <h2 class="display-8 mb-3">
                            PIT PAMKI 2024
                        </h2>
                    </div>
                    <div class="col-lg-auto align-self-center mt-4 mt-lg-0">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb no-border">
                                <li class="breadcrumb-item"><a href="#" class="link link-dark-primary size-14" data-hover="Home">Home</a></li>
                                <li class="breadcrumb-item active font-weight-500" aria-current="page"><span class="size-14">Kontak</span></li>
                            </ol>
                        </nav>				
                    </div>
                </div>
            </div>
        </div>

        <div class="section pb-4 px-lg-2">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<div class="section over-hide border-4 bg-white section-shadow-blue padding-top-bottom-120 px-3 px-xl-5">
							<div class="row">
								<div class="col-12 align-self-center mb-5 pb-4">
									<div class="row">
										<div class="col-auto align-self-center">
											<h6 class="color-gray-dark text-vertical text-uppercase mx-0 px-0">
												detail
											</h6>
										</div>
										<div class="col">
											<h3 class="Susunan Panitia mb-0">
												Kontak
											</h3>
										</div>
									</div>
								</div>
								<div class="col-md-12  pr-xl-5" data-scroll-reveal="enter bottom move 40px over 0.5s after 0.3s">
                                    
									<div class="row justify-content-center">
                                        <div class="col-xl-8"> 
                                            <div class="section over-hide border-4 text-center bg-white px-3 px-lg-4 py-5 landing-shadow-4 "> 
                                                <h4 class="mb-4">Kontak Kami</h4>
                                                <a href="https://wa.me/6281371718191" class="link size-18 mb-1 text-dark" data-hover="dr. Dewi Yana, Sp.MK : +62 813 7171 8191">dr. Dewi Yana, Sp.MK : +62 813 7171 8191</a> 
                                                <div class="clearfix w-100"></div>
                                                <a href="https://wa.me/62811753684" class="link size-18 mb-1 text-dark" data-hover="Mislindawati : +62 811 753 684">Mislindawati : +62 811 753 684</a> 
                                                <div class="clearfix w-100"></div>
                                                <a href="https://wa.me/6281268081956" class="link size-18 mb-1 text-dark" data-hover="Agnes Rica : +62 812 6808 1956">Agnes Rica : +62 812 6808 1956</a> 
                                                <div class="clearfix w-100"></div>
                                                <a href="" class="link size-18 mb-4" style="color:#891d13" data-hover="pamkicabangriau@gmail.com">pamkicabangriau@gmail.com</a>
                                                <div class="clearfix w-100"></div>
                                            </div>	
                                        </div>	
                                    </div>	

								</div>
                               
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
        
    </div>

@endsection        