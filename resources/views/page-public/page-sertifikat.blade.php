@extends('layout.mainbody')

@section('container')

    <div class="section over-hide padding-top-120 padding-top-mob-nav section-background-24">	
        <div class="section-1400 pt-xl-4">
            <div class="container-fluid padding-top-bottom-80">
                <div class="row">
                    <div class="col-lg">
                        <h2 class="display-8 mb-3">
                            PIT PAMKI 2024
                        </h2>
                    </div>
                    <div class="col-lg-auto align-self-center mt-4 mt-lg-0">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb no-border">
                                <li class="breadcrumb-item"><a href="#" class="link link-dark-primary size-14" data-hover="Home">Home</a></li>
                                <li class="breadcrumb-item active font-weight-500" aria-current="page"><span class="size-14">Materi Acara</span></li>
                            </ol>
                        </nav>				
                    </div>
                </div>
            </div>
        </div>

        <div class="section over-hide padding-top-bottom-120 bg-white section-background-20 background-img-top" id="pit-pamki">	
			<div class="section-1400">
				<div class="container-fluid">
						
						<div class="col-lg-12 mt-4 mt-lg-0">
							<div class="section border-4 bg-white landing-shadow-4 p-3 p-xl-4">   
							
                                
                                {{-- Form Upload Bukti Pembayran --}}
								<div id="bukti-pembayaran" >
									<div class="card-body p-0">
										<div class="row">
											<div class="col-12">
												<h5 class="mb-3">
													Cetak Sertifikat
												</h5>
											</div>
											<div class="col-12 pb-3">
												<div class="section divider divider-gray"></div>
											</div>
											<div class="col-12">
                                                <form action="#" id="form-cetaksertifikat">
                                                    <div class="row justify-content-center">
                                                        @csrf

                                                        <div class="col-md-12  "> 
                                                            <div class="alert alert-danger alert-dismissible fade show mt-2 d-none" id="notif-alertserti" role="alert">
                                                                <i class="uil uil-times-circle"></i> <span id="text-notif-alertserti"></span>
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                    <i class="uil uil-multiply"></i>
                                                                </button>
                                                            </div>
                                                            <div class="alert alert-danger alert-dismissible fade show mt-2 d-none" id="notif-alertsertisuccess" role="alert">
                                                                <i class="uil uil-times-circle"></i> <span id="text-notif-alertsertisuccess"></span>
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                    <i class="uil uil-multiply"></i>
                                                                </button>
                                                            </div>

                                                        </div>

                                                        <div class="col-lg-12 mt-3 "> 
                                                            <div class="form-group">
                                                                <label for="" class="text-dark font-weight-bold">Nomor Peserta</label>
                                                                <input type="text" class="form-style text-uppercase" autocomplete="off" name="nomor_peserta" id="nomor_peserta" value="pmki-" placeholder="PMKI-0XXX" autofocus required>
                                                            </div>   				
                                                            
                                                        </div>                                                                                                            
                                                        
                                                    
                                                        <div class="col-lg-12 mt-3 "> 
                                                            <div class="form-group">
                                                                <label for="" class="text-dark font-weight-bold">Nama Lengkap </label>
                                                                <input type="text"  class=" form-style " name="namalengkappeserta" id="dtl-namalengkap">
                                                            </div>   				
                                                        </div>

                                                        <div class="col-lg-12 mt-3 "> 
                                                            <div class="form-group">
                                                                <label for="" class="text-dark font-weight-bold">Jenis Kegiatan </label>
                                                                <input type="text" readonly class="bg-gray form-style " id="dtl-jeniskegiatan">
                                                            </div>   				
                                                        </div>

                                                        <div class="col-md-12 mt-3 "> 
                                                            <button type="submit" class="btn text-white p-3 btn-fluid disabled"  id="btn-sertifikat" style="background-color:#891d13;">
                                                                <i class="uil uil-user-plus size-20 mr-2"></i>CETAK SERTIFIKAT
                                                            </button>
                                                        </div>

                                                        {{-- <div class="alert alert-danger alert-icon alert-dismissible fade show mt-5 " id="notif-alertabs" role="alert">
                                                            <i class="uil uil-times-circle"></i> <span id="text-notif-alertabs"></span>
                                                        </div> --}}

                                                    </div>
                                                </form>
										</div>
									</div>
								    </div>
                                </div>

                                
								
							</div>
						</div>
				</div>
			</div>
		</div>
        
    </div>

@endsection        