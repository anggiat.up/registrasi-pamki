@extends('layout.mainbody')

@section('container')

    <div class="section over-hide padding-top-120 padding-top-mob-nav section-background-24">	
        <div class="section-1400 pt-xl-4">
            <div class="container-fluid padding-top-bottom-80">
                <div class="row">
                    <div class="col-lg">
                        <h2 class="display-8 mb-3">
                            PIT PAMKI 2024
                        </h2>
                    </div>
                    <div class="col-lg-auto align-self-center mt-4 mt-lg-0">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb no-border">
                                <li class="breadcrumb-item"><a href="#" class="link link-dark-primary size-14" data-hover="Home">Home</a></li>
                                <li class="breadcrumb-item active font-weight-500" aria-current="page"><span class="size-14">Materi Acara</span></li>
                            </ol>
                        </nav>				
                    </div>
                </div>
            </div>
        </div>

        <div class="section pb-4 px-lg-2">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<div class="section over-hide border-4 bg-white section-shadow-blue padding-top-bottom-120 px-3 px-xl-5">
							<div class="row">
								<div class="col-12 align-self-center mb-5 pb-4">
									<div class="row">
										<div class="col-auto align-self-center">
											<h6 class="color-gray-dark text-vertical text-uppercase mx-0 px-0">
												detail
											</h6>
										</div>
										<div class="col">
											<h3 class=" mb-0">
												Materi Acara
											</h3>
										</div>
									</div>
								</div>
								<div class="col-md-12 pr-xl-5" data-scroll-reveal="enter bottom move 40px over 0.5s after 0.3s">
                                    
									<div class="col-md-12 pr-xl-5" data-scroll-reveal="enter bottom move 40px over 0.5s after 0.3s">
                                        <div class="accordion" id="accordionExample">
                                            <div class="card">
                                                <div class="card-header " id="headingOne">
                                                    <div class="btn-accordion collapsed " role="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                        <h6 class="mt-2">Jumat, 6 September 2024 </h6>
                                                    </div>
                                                </div>
                                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                    <div class="card-body">
                                                        <h6 class="mt-4">Agenda PIT PAMKI 2024 : Hari ke - 1</h6>                
                                                        <h6 class="mb-4">Detail Kegiatan Jumat, 6 September 2024 </h6>
                                                        <table class="table table-bordered" width="100%" >
                                                            <thead class="thead-dark text-white text-center">
                                                                <tr>
                                                                    <th scope="col" width="30%">Waktu</th>
                                                                    <th scope="col">Kegiatan</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="text-dark">
                                                                <tr >
                                                                    <th class="text-center align-middle">08:00 - 16:00 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"><b>Workshop 1 :</b> Quality Assurance in Bacterial Culture and Antimicrobial Susceptibility Test <br>
                                                                            <b>Workshop 2 :</b> Expertise Microbiology : Bridging from Bench to Word</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">19:00 - 22:00 WIB</th>
                                                                    <td class="align-middle">
                                                                        <p class="text-dark">Sidang Organisasi</p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header" id="headingTwo">
                                                    <div class="btn-accordion collapsed" role="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                        <h6 class="mt-2">Sabtu, 7 September 2024 </h6>
                                                    </div>
                                                </div>
                                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                                    <div class="card-body">
                                                        <h6 class="mt-4">Agenda PIT PAMKI 2024 : Hari ke - 2</h6>                
                                                        <h6 class="mb-4">Detail Kegiatan Sabtu, 7 September 2024 </h6>
                                                        <table  class="table " width="100%" >
                                                            <thead class="thead-dark text-white text-center">
                                                              <tr>
                                                                <th scope="col" width="30%">Waktu</th>
                                                                <th scope="col">Kegiatan</th>
                                                              </tr>
                                                            </thead>
                                                            <tbody >
                                                                <tr>
                                                                    <th class="text-center align-middle">07:00 - 08:00 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Registrasi</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">08:00 - 08:20 WIB</th>
                                                                    <td >
                                                                        <p class="text-dark">Opening Ceremony</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">08:20 - 08:35 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"> Sambutan Ketua Panitia <br>
                                                                          <b>Dr. dr. Dewi Anggraini Sp.MK(K)</b></p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">08:35 - 08:50 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Sambutan Ketua PP PAMKI <br>
                                                                          <b>Dr. dr. Anis Karuniawati, Ph.D, Sp.MK(K)</b></p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">08:50 - 09:00 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Sambutan ----</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Keynote Speech and Plenary Lecture</i></th>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">09:00 - 09:20 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"><b>Keynote Speech : </b> <br> 
                                                                            Strengthening Indonesia's Health Laboratory Network for Enhanced Surveillance and Diagnosis of Infectious Diseases, Promoting Early Detection and Comprehensive Disease Management <br>
                                                                          <b>Speaker :</b> Representative from Ministry of Health</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">09:20 - 09:40 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"><b>Plenary Lecture : </b> <br> 
                                                                            Ethical and Legal Challenges in Laboratory Diagnostics <br>
                                                                          <b>Speaker :</b> </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">09:40 - 10:00 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"><b>Plenary Lecture : </b> <br> 
                                                                            SINAR : PAMKI's Antimicrobial Resistance Surveilance System and Its Impact on Atimicrobial Resistance Control in Indonesia <br>
                                                                          <b>Speaker :</b> </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">10:00 - 10:15 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Kunjungan ke <i>exhibition</i> dan <i>coffe break</i></p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium I</i><br>
                                                                    Theme : Management for MDR infection</th>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">10:15 - 10:35 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"><b>Topic : </b> <br> 
                                                                            Multidrug-Resistant Gram-Negative Bacterial Infections in the Hospital <br>
                                                                            Setting: Emerging Treatment Options <br>
                                                                          <b>Speaker :</b> </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">10:35 - 10:55 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"><b>Topic : </b> <br> 
                                                                            Optimizing the Role of Microbiology Laboratories in Detecting Multidrug-Resistant Bacteria <br>
                                                                          <b>Speaker :</b> </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">10:55 - 11:15 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Diskusi dan Tanya Jawab</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">11:15 - 11:30 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Industrial Break (Sponsorship)</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">11:30 - 13:00 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Lunch Simposium I</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium II</i><br>
                                                                    Theme : Advance infectious disease diagnostic assay</th>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">13:00 - 13:20 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"><b>Topic : </b> <br> 
                                                                            The Role of Metagenomics and Next-Generation Sequencing in Infectious Disease Diagnosis <br>
                                                                          <b>Speaker :</b> </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">13:20 - 13:40 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"><b>Topic : </b> <br> 
                                                                            CRISPR technology: new paradigm to target the infectious disease pathogens or Nanotechnology for diagnosis and treatment of infectious diseases <br>
                                                                          <b>Speaker :</b> </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">13:40 - 14:00 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Diskusi dan Tanya Jawab</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">14:00 - 14:15 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Industrial Break (Sponsorship)</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium III</i><br>
                                                                    Theme : Prevention and Control of infection</th>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">14:15 - 14:35 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"><b>Topic : </b> <br> 
                                                                            Hospital Environmental Cleaning to Prevent MDRO Spread in the ICU <br>
                                                                          <b>Speaker :</b> </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">14:35 - 14:55 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"><b>Topic : </b> <br> 
                                                                            Reuse of single use medical devices : clinical and economic outcomes, legal and ethical issues, and current hospital practice <br>
                                                                          <b>Speaker :</b> </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">14:55 - 15:15 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Diskusi dan Tanya Jawab</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">15:15 - 15:30 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Industrial Break (Sponsorship)</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle" rowspan="2">15:30 - 15:45 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"><b>Penutupan Simposium Hari I</b></p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <p class="text-dark"><b>Closing Remarks Day 1</b></p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header" id="headingThree">
                                                    <div class="btn-accordion collapsed" role="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                        <h6 class="mt-2">Minggu, 8 September 2024 </h6>
                                                    </div>
                                                </div>
                                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                                    <div class="card-body">
                                                        <h6 class="mt-4">Agenda PIT PAMKI 2024 : Hari ke - 3</h6>                
                                                        <h6 class="mb-4">Detail Kegiatan Minggu, 8 September 2024 </h6>
                
                                                        <table  class="table table-sm" width="100%" border="1">
                                                            <thead class="thead-dark text-white text-center">
                                                              <tr>
                                                                <th scope="col" width="30%">Waktu</th>
                                                                <th scope="col">Kegiatan</th>
                                                              </tr>
                                                            </thead>
                                                            <tbody class="text-dark">
                                                                <tr>
                                                                    <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium IV</i><br>
                                                                    Theme : Update in Tuberculosis Management</th>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">07:00 - 08:00 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Meet The Expert</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">08:00 - 08:20 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"><b>Topic : </b> <br> 
                                                                            Update Diagnosiss TB : New Diagnostic Tools<br>
                                                                          <b>Speaker :</b> </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">08:20 - 08:40 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"><b>Topic : </b> <br> 
                                                                            Update Management of MDR TB in Indonesia<br>
                                                                          <b>Speaker :</b> </p>
                                                                    </td>
                                                                </tr>                        
                                                                <tr>
                                                                    <th class="text-center align-middle">08:40 - 09:00 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Diskusi dan Tanya Jawab</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">09:00 - 09:15 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Industrial Break (Sponsorship)</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">09:15 - 09:30 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Coffe Break </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium V</i><br>
                                                                    Theme : Emerging and Neglected Infetious Diseases : Insights, Advances, and Challenges</th>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">09:30 - 09:50 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"><b>Topic : </b> <br> 
                                                                            Melioidosis diseases in Indonesia : Experience in Riau Province <br>
                                                                          <b>Speaker :</b> </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">09:50 - 10:10 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"><b>Topic : </b> <br> 
                                                                            Streptococcus suis infection in Indonesia : Experience in Bali <br>
                                                                          <b>Speaker :</b> </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">10:10 - 10:30 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Diskusi dan Tanya Jawab</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">10:30 - 10:45 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Industrial Break (Sponsorship)</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium VI</i><br>
                                                                    Theme : Viral Disease Management</th>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">10:45 - 11:05 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"><b>Topic : </b> <br> 
                                                                            HIV Resistance in Indonesia <br>
                                                                          <b>Speaker :</b> </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">11:05 - 11:25 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"><b>Topic : </b> <br> 
                                                                            Updates on Emerging and Re-emerging Viral Disease <br>
                                                                          <b>Speaker :</b> </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">11:25 - 11:45 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Diskusi dan Tanya Jawab</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">11:45 - 12:00 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Industrial Break (Sponsorship)</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">12:00 - 13:00 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Lunch Simposium II</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium VII</i><br>
                                                                    Theme : Fungal Infectious disease management</th>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">13:00 - 13:20 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"><b>Topic : </b> <br> 
                                                                            Best Practices in Antifungal Management and WHO fungal Priority Pathogens List <br>
                                                                          <b>Speaker :</b> </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">13:20 - 13:40 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark"><b>Topic : </b> <br> 
                                                                            Key Initial Steps When Processing and Direct Examintaion of Clinical Specimens for Fungal Examination <br>
                                                                          <b>Speaker :</b> </p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">13:40 - 14:00 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Diskusi dan Tanya Jawab</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle">14:00 - 14:15 WIB</th>
                                                                    <td>
                                                                        <p class="text-dark">Industrial Break (Sponsorship)</p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center align-middle" rowspan="2">14:15 - 14:30 WIB</th>                            
                                                                    <td>
                                                                        <p class="text-dark"><b>Closing Ceremony</b></p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>									
                                    </div>

								</div>
                               
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
        
    </div>

@endsection        