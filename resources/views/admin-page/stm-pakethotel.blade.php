@extends('admin-page.stm-mainbody')

@section('container')

    

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="header-title">Table Data Paket Hotel</h4>
                <p class="sub-header">
                    {{-- See how aspects of the Bootstrap grid system work across multiple devices with a handy table. --}}
                </p>

                <button type="button" class="mb-4 btn btn-sm btn-primary waves-effect waves-light "  data-toggle="modal" data-target=".modal-tambapaket-hotel">Tambah Paket Hotel</button>
                
                <div class="table-responsive">
                    
                    <table id="table-paket-hotel" class="table table-striped table-bordered mb-0" width="100%">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Hotel</th>
                            <th class="text-center">Nama Paket</th>
                            <th class="text-center">Harga</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                        </thead>                       
                    </table>


                </div>
            </div>
        </div>
    </div>

    
    <div class="modal fade modal-tambapaket-hotel" id="modalTambahPaketHotel" tabindex="-1"  role="dialog" aria-labelledby="modalTambahPaketHotelLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myExtraLargeModalLabel">Tambah Data Paket Hotel</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12">
                            <form id="form-tambahpaket-hotel" method="post" action="#">
                                @csrf
                                
                                <div class="form-group">
                                    <label for="nama_hotel">Nama Hotel</label>
                                    <select class="form-control" id="nama_hotel" name="nama_hotel" required>
                                        <option value="" selected hidden disabled>-- Pilih Hotel --</option>
                                        @foreach ($datahotel as $item)
                                        <option value="{{ $item['hotelid'] }}">{{ $item['nama_hotel'] }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="nama_paket">Nama Paket</label>
                                    <input type="text" class="form-control" id="nama_paket" name="nama_paket" required >
                                </div>

                                <div class="form-group">
                                    <label for="harga">Harga Paket</label>
                                    <input type="number" class="form-control" id="harga" name="harga" required >
                                </div>

                                <div class="form-group">
                                    <label for="status">Status Paket</label>
                                    <select class="form-control" id="status" name="status" required>
                                        <option value="" selected hidden disabled>-- Pilih Status --</option>
                                        <option value="aktif">Aktif</option>
                                        <option value="tidak-aktif">Tidak Aktif</option>
                                    </select>
                                </div>


                                <button type="submit" class="btn btn-primary">Tambah Paket Hotel</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-udpdatepaket-hotel" id="modalUpdatePaketHotel" tabindex="-1"  role="dialog" aria-labelledby="modalUpdatePaketHotelLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myExtraLargeModalLabel">Update Data Paket Hotel</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12">
                            <form id="form-update-paket-hotel" method="post" action="#">
                                @csrf
                                <input type="hidden" name="paketid" id="paketid">
                                <div class="form-group">
                                    <label for="nama_hotel">Nama Hotel</label>
                                    <select class="form-control" id="unama_hotel" name="nama_hotel" required>
                                        <option value="" selected hidden disabled>-- Pilih Hotel --</option>
                                        @foreach ($datahotel as $item)
                                        <option value="{{ $item['hotelid'] }}">{{ $item['nama_hotel'] }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="nama_paket">Nama Paket</label>
                                    <input type="text" class="form-control" id="unama_paket" name="nama_paket" required >
                                </div>

                                <div class="form-group">
                                    <label for="harga">Harga Paket</label>
                                    <input type="number" class="form-control" id="uharga" name="harga" required >
                                </div>

                                <div class="form-group">
                                    <label for="status">Status Paket</label>
                                    <select class="form-control" id="ustatus" name="status" required>
                                        <option value="" selected hidden disabled>-- Pilih Status --</option>
                                        <option value="aktif">Aktif</option>
                                        <option value="tidak-aktif">Tidak Aktif</option>
                                    </select>
                                </div>


                                <button type="submit" class="btn btn-primary">Update Paket Hotel</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection        