<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ public_path('style-laporan/css/bootstrap.min.css') }}">
    

    <title>Laporan Data Peserta</title>
    <style>
      table td, table th, table tr {
        border: 1px solid #000000 !important;
        font-size: 11px !important;
        color: #000000;
      }
    </style>
  </head>
  <body style="font-family: Arial, Helvetica, sans-serif;">
    <h1 style="font-size: 25px;font-weight: bold;" class="text-center">LAPORAN PESERTA PIT PAMKI 2024 <br> PRESENSI {{ $titlepresensi }}</h1>
    <p>
      <small><i>Dicetak Oleh : {{ auth()->user()->nama_lengkap; }}</i></small> <br>
      <small><i>Waktu : {{ date('H:i d-F-Y'); }}</i></small>
    </p>
    <div class="table table-bordered ">
      
      <table class="" width="100%" border="1" style="font-size: 5px;">
        <thead>
          <tr>
            <th scope="col" class="p-4 text-center" width="5%">#</th>
            <th scope="col" class="p-4 text-center" >NAMA LENGKAP</th>
            <th scope="col" class="p-4 text-center" width="8%">NO HP</th>
            <th scope="col" class="p-4 text-center" width="8%">NO PESERTA</th>
            <th scope="col" class="p-4 text-center" width="12%">JENIS PESERTA</th>
            <th scope="col" class="p-4 text-center" width="13%">NAMA PAKET</th>
            <th scope="col" class="p-4 text-center" width="10%">STATUS PRESENSI</th>
          </tr>
        </thead>
        <tbody>
          {{ $no = 1; }}
          @foreach ($datapeserta as $item)
            <tr>
              <td class="p-3 align-middle text-center" scope="row">{{ $no++; }}</td>
              <td class="p-3 align-middle">{{ $item['gelar_depan'] . ' ' .$item['nama_lengkap'] . ' ' . $item['gelar_belakang'] }}</td>
              <td class="p-3 align-middle text-center">{{ $item['nohp'] }}</td>
              <td class="p-3 align-middle text-center text-uppercase">{{ $item['pst_id'] }}</td>
              <td class="p-3 align-middle text-center">{{ $item['jenis_peserta'] }}</td>
              <td class="p-3 align-middle text-center" style="font-size: 10px ;">{{ $item['nama_paket'] }}</td>
              <td class="p-3 align-middle text-center text-uppercase" style="font-size: 10px ;">{{ $item[$colompresensi] }}</td>
            </tr>
            
          @endforeach
        </tbody>
      </table>
    </div>
    

    
  </body>
</html>