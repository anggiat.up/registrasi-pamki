@extends('admin-page.stm-mainbody')

@section('container')

    

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="header-title">Table Data Hotel</h4>
                <p class="sub-header">
                    {{-- See how aspects of the Bootstrap grid system work across multiple devices with a handy table. --}}
                </p>

                <button type="button" class="mb-4 btn btn-sm btn-primary waves-effect waves-light "  data-toggle="modal" data-target=".modal-tambah-hotel">Tambah Hotel</button>
                
                <div class="table-responsive">
                    
                    <table id="table-hotel" class="table table-striped table-bordered mb-0" width="100%">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Nama Hotel</th>
                            <th class="text-center">Gambar</th>
                            <th class="text-center">Bintang Rate</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Alamat</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                        </thead>                       
                    </table>


                </div>
            </div>
        </div>
    </div>

    
    <div class="modal fade modal-tambah-hotel" id="modalTambahHotel" tabindex="-1"  role="dialog" aria-labelledby="modalTambahHotelLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="myExtraLargeModalLabel">Tambah Data Hotel</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-12">
                            <form id="form-tambah-hotel" method="post" action="#">
                                @csrf
                                
                                <div class="form-group">
                                    <label for="nama_hotel">Nama Hotel</label>
                                    <input type="text" class="form-control" id="nama_hotel" name="nama_hotel" required >
                                </div>

                                <div class="form-group">
                                    <label for="gambar_hotel">Gambar Hotel</label>
                                    <input type="file" class="form-control" id="gambar_hotel" name="gambar_hotel" required accept="image/*">
                                </div>

                                <div class="form-group">
                                    <label for="alamat">Alamat</label>
                                    <input type="text" class="form-control" id="alamat" name="alamat" required >
                                </div>

                                <div class="form-group">
                                    <label for="rate_bintang">Rate Bintang</label>
                                    <select class="form-control" id="rate_bintang" name="rate_bintang" required>
                                        <option value="5">Bintang 5</option>
                                        <option value="4">Bintang 4</option>
                                        <option value="3">Bintang 3</option>
                                        <option value="2">Bintang 2</option>
                                        <option value="1">Bintang 1</option>
                                        <option value="0">Tanpan Bintang</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="status">Status Hotel</label>
                                    <select class="form-control" id="status" name="status" required>
                                        <option value="aktif">Aktif</option>
                                        <option value="tidak-aktif">Tidak Aktif</option>
                                    </select>
                                </div>


                                <button type="submit" class="btn btn-primary">Tambah Hotel</button>
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection        