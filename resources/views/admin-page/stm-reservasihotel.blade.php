@extends('admin-page.stm-mainbody')

@section('container')

    

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="header-title">Table Data Reservasi Hotel</h4>
                <p class="sub-header">
                    {{-- See how aspects of the Bootstrap grid system work across multiple devices with a handy table. --}}
                </p>

                <div class="table-responsive">
                    
                    <table id="table-reservasi-hotel" class="table table-striped table-bordered mb-0" width="100%">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Nama Lengkap</th>
                            <th class="text-center"> Nama Paket & Hotel</th>
                            <th class="text-center">Harga & Total Biaya</th>
                            <th class="text-center">CheckIn / CheckOut</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                        </thead>                       
                    </table>


                </div>
            </div>
        </div>
    </div>

    
    {{-- Modal Bukti Pembayaran --}}
    <div class="modal fade modalBuktiPembayaran" id="modalBuktiPembayaran" tabindex="-1" role="dialog" aria-labelledby="modalPembayaranLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalPembayaranLabel">File Bukti Pembayaran Peserta</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <img id="file-buktipembayaran" src="" width="100%" alt="">
                     <hr class="mt-2 mb-2">                    
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modalDetailReservasiHotel" id="modalDetailReservasiHotel" tabindex="-1" role="dialog" aria-labelledby="modalDetailReservasiHotelLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalDetailReservasiHotelLabel">Detail Data Peserta P</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-nowrap">
                                <tr>
                                    <td colspan="3" class="bg-secondary text-white text-center"> <i>Detail Reservasi Hotel</i></td>
                                </tr>
                                <tr>
                                    <th width="30%">NAMA LENGKAP</th>
                                    <td width="5%">:</td>
                                    <td id="namalengkap"></td>
                                </tr>
                                <tr>
                                    <th>NO HP</th>
                                    <td>:</td>
                                    <td id="nohp"></td>
                                </tr>
                                <tr>
                                    <th>NAMA HOTEL</th>
                                    <td>:</td>
                                    <td id="namahotel"></td>
                                </tr>
                                <tr>
                                    <th>PAKET HOTEL</th>
                                    <td>:</td>
                                    <td id="pakethotel"></td>
                                </tr>
                                <tr>
                                    <th>TANGGAL CHECKIN & CHECKOUT</th>
                                    <td>:</td>
                                    <td id="cico"></td>
                                </tr>
                                <tr>
                                    <th class="align-middle">TOTAL BIAYA</th>
                                    <td class="align-middle">:</td>
                                    <td id="totalbiaya"></td>
                                </tr>
                                <tr>
                                    <th>WAKTU PENDAFTARAN</th>
                                    <td>:</td>
                                    <td id="waktu"></td>
                                </tr>                                                                
                        </table>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
@endsection        