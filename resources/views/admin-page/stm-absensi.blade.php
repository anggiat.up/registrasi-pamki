@extends('admin-page.stm-mainbody')

@section('container')

    <div class="row" id="form-presensi">
        <div class="col-lg-12 col-xl-12">
            <div class="card-box">
                
                <div class="row mb-3">
                    {{-- <form id="presensi-galadinner" > --}}
                        <div class="col-xl-8 offset-xl-2">
                            
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <input type="text" class="form-control" id="identitaspeserta" name="identitaspeserta"  autofocus required>
                                    <div class="input-group-append">
                                        <span class="input-group-text" style="cursor: pointer;" id="btn-resetpencarian">
                                            <i class="fas fa-times" id="clear-icon"  ></i>
                                        </span>
                                    </div>
                                </div>
                                <small id="emailHelp" class="form-text text-muted">*Cari data berdasarkan Nomor-registrasi, Email, atau No. HP</small>
                            </div>
                            
                        </div>
                        <div class="col-xl-2">
                            <div class="form-group  ml-0" style="">
                                <button type="submit" class="btn btn-lg btn-primary">Cari Peserta</button>                                
                            </div>
                        </div>
                            
                    {{-- </form> --}}

                </div>

                <div class="row " >
                    <div class="col-xl-8 offset-xl-2 " id="data-peserta">
                        <ol class="list-group list-group-numbered" id="list-data-peserta">                            
                        </ol>
                    </div>
                </div>
            </div>
        

            <div class="row " >
                <div class="col-sm-12"  style="opacity: 1;">
                    <div class="card card-body" id="detail-peserta">                        
                    </div>  
                </div>  

                
            </div>
        </div>

        
    </div>

@endsection        