@extends('layout.pbc-mainbody')

@section('container')

    <!-- Breadcrumbs-->
    <section class="section novi-bg novi-bg-img breadcrumbs-custom">
        <div class="container">
            <div class="breadcrumbs-custom__inner">
            <p class="breadcrumbs-custom__title">Materi & Acara</p>
            <ul class="breadcrumbs-custom__path">
                <li><a href="{{ url('') }}">Home</a></li>
                <li class="active">Materi & Acara</li>
            </ul>
            </div>
        </div>
    </section>

    {{-- Counter Time --}}
    <section class="section novi-bg novi-bg-img bg-accent">
        <div class="container">
          <div class="row row-fix justify-content-md-center ">

            {{-- Text Counter --}}
            <div class="col-md-8 col-lg-6 section-xl">
              <h3><b>PIT PAMKI 2024</b></h3>
              <p class="">Hitungan mundur telah dimulai menuju acara PIT PAMKI 2024. Simposium dan workshop akan menjadi momen penting bersama Perhimpunan Dokter Spesialis Mikrobiologi Klinik Indonesia (PAMKI)</p>
            </div>
            <div class="col-md-8 col-lg-6 section-xl d-flex justify-content-center align-items-center">
              <div class="countdown" data-countdown="data-countdown" data-to="2024-09-06">
                <div class="countdown-block countdown-block-Days">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Days="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Days="">00</div>
                    <div class="countdown-title">Days</div>
                  </div>
                </div>
                <div class="countdown-block countdown-block-Hours">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Hours="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Hours="">00</div>
                    <div class="countdown-title">Hours</div>
                  </div>
                </div>
                <div class="countdown-block countdown-block-Minutes">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Minutes="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Minutes="">00</div>
                    <div class="countdown-title">Minutes</div>
                  </div>
                </div>
                <div class="countdown-block countdown-block-Seconds">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Seconds="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Seconds="">00</div>
                    <div class="countdown-title">Seconds</div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
    </section>

    {{-- Tabs Materi & Acara --}}
    <section class="section novi-bg novi-bg-img section-lg bg-default">
        <div class="container">
          <!-- Bootstrap tabs-->
          <div class="tabs-custom tabs-horizontal" id="tabs-1">
            <!-- Nav tabs-->
            <ul class="list-fix nav nav-custom nav-custom-tabs">
              <li class="nav-item"><a class="nav-link" href="#tabs-1-1" data-toggle="tab" style="font-size: 18px">Jumat, 6 September 2024</a></li>
              <li class="nav-item"><a class="nav-link" href="#tabs-1-2" data-toggle="tab" style="font-size: 18px">Sabtu, 7 September 2024</a></li>
              <li class="nav-item"><a class="nav-link" href="#tabs-1-3" data-toggle="tab" style="font-size: 18px">Minggu, 8 September 2024</a></li>
            </ul>
          </div>
          <div class="tab-content text-left">

            {{-- Tab 1-1 --}}
            <div class="tab-pane fade " id="tabs-1-1">
                <h6>Agenda PIT PAMKI 2024 : Hari ke - 1</h6>                
                <h6 class="mb-4">Detail Kegiatan Jumat, 6 September 2024 </h6>
               
                <table class="table" width="100%" border="1">
                    <thead class="thead-dark text-white text-center">
                      <tr>
                        <th scope="col" width="30%">Waktu</th>
                        <th scope="col">Kegiatan</th>
                      </tr>
                    </thead>
                    <tbody class="text-dark">
                        <tr>
                            <th class="text-center align-middle">08:00 - 16:00 WIB</th>
                            <td>
                                <p><b>Workshop 1 :</b> Quality Assurance in Bacterial Culture and Antimicrobial Susceptibility Test <br>
                                   <b>Workshop 2 :</b> Expertise Microbiology : Bridging from Bench to Word</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">19:00 - 22:00 WIB</th>
                            <td>
                                <p>Sidang Organisasi</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            {{-- Tab 1-2 --}}
            <div class="tab-pane fade" id="tabs-1-2">
                <h6>Agenda PIT PAMKI 2024 : Hari ke - 2</h6>
                
                <h6 class="mb-4">Detail Kegiatan Sabtu, 7 September 2024 </h6>
               
                <table  class="table" width="100%" border="1">
                    <thead class="thead-dark text-white text-center">
                      <tr>
                        <th scope="col" width="30%">Waktu</th>
                        <th scope="col">Kegiatan</th>
                      </tr>
                    </thead>
                    <tbody class="text-dark">
                        <tr>
                            <th class="text-center align-middle">07:00 - 08:00 WIB</th>
                            <td>
                                <p>Registrasi</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">08:00 - 08:20 WIB</th>
                            <td>
                                <p>Opening Ceremony</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">08:20 - 08:35 WIB</th>
                            <td>
                                <p>Sambutan Ketua Panitia <br>
                                   <b>Dr. dr. Dewi Anggraini Sp.MK(K)</b></p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">08:35 - 08:50 WIB</th>
                            <td>
                                <p>Sambutan Ketua PP PAMKI <br>
                                   <b>Dr. dr. Anis Karuniawati, Ph.D, Sp.MK(K)</b></p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">08:50 - 09:00 WIB</th>
                            <td>
                                <p>Sambutan ----</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Keynote Speech and Plenary Lecture</i></th>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">09:00 - 09:20 WIB</th>
                            <td>
                                <p><b>Keynote Speech : </b> <br> 
                                    Strengthening Indonesia's Health Laboratory Network for Enhanced Surveillance and Diagnosis of Infectious Diseases, Promoting Early Detection and Comprehensive Disease Management <br>
                                   <b>Speaker :</b> Representative from Ministry of Health</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">09:20 - 09:40 WIB</th>
                            <td>
                                <p><b>Plenary Lecture : </b> <br> 
                                    Ethical and Legal Challenges in Laboratory Diagnostics <br>
                                   <b>Speaker :</b> </p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">09:40 - 10:00 WIB</th>
                            <td>
                                <p><b>Plenary Lecture : </b> <br> 
                                    SINAR : PAMKI's Antimicrobial Resistance Surveilance System and Its Impact on Atimicrobial Resistance Control in Indonesia <br>
                                   <b>Speaker :</b> </p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">10:00 - 10:15 WIB</th>
                            <td>
                                <p>Kunjungan ke <i>exhibition</i> dan <i>coffe break</i></p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium I</i><br>
                            Theme : Management for MDR infection</th>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">10:15 - 10:35 WIB</th>
                            <td>
                                <p><b>Topic : </b> <br> 
                                    Multidrug-Resistant Gram-Negative Bacterial Infections in the Hospital <br>
                                    Setting: Emerging Treatment Options <br>
                                   <b>Speaker :</b> </p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">10:35 - 10:55 WIB</th>
                            <td>
                                <p><b>Topic : </b> <br> 
                                    Optimizing the Role of Microbiology Laboratories in Detecting Multidrug-Resistant Bacteria <br>
                                   <b>Speaker :</b> </p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">10:55 - 11:15 WIB</th>
                            <td>
                                <p>Diskusi dan Tanya Jawab</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">11:15 - 11:30 WIB</th>
                            <td>
                                <p>Industrial Break (Sponsorship)</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">11:30 - 13:00 WIB</th>
                            <td>
                                <p>Lunch Simposium I</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium II</i><br>
                            Theme : Advance infectious disease diagnostic assay</th>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">13:00 - 13:20 WIB</th>
                            <td>
                                <p><b>Topic : </b> <br> 
                                    The Role of Metagenomics and Next-Generation Sequencing in Infectious Disease Diagnosis <br>
                                   <b>Speaker :</b> </p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">13:20 - 13:40 WIB</th>
                            <td>
                                <p><b>Topic : </b> <br> 
                                    CRISPR technology: new paradigm to target the infectious disease pathogens or Nanotechnology for diagnosis and treatment of infectious diseases <br>
                                   <b>Speaker :</b> </p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">13:40 - 14:00 WIB</th>
                            <td>
                                <p>Diskusi dan Tanya Jawab</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">14:00 - 14:15 WIB</th>
                            <td>
                                <p>Industrial Break (Sponsorship)</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium III</i><br>
                            Theme : Prevention and Control of infection</th>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">14:15 - 14:35 WIB</th>
                            <td>
                                <p><b>Topic : </b> <br> 
                                    Hospital Environmental Cleaning to Prevent MDRO Spread in the ICU <br>
                                   <b>Speaker :</b> </p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">14:35 - 14:55 WIB</th>
                            <td>
                                <p><b>Topic : </b> <br> 
                                    Reuse of single use medical devices : clinical and economic outcomes, legal and ethical issues, and current hospital practice <br>
                                   <b>Speaker :</b> </p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">14:55 - 15:15 WIB</th>
                            <td>
                                <p>Diskusi dan Tanya Jawab</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">15:15 - 15:30 WIB</th>
                            <td>
                                <p>Industrial Break (Sponsorship)</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle" rowspan="2">15:30 - 15:45 WIB</th>
                            <td>
                                <p><b>Penutupan Simposium Hari I</b></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p><b>Closing Remarks Day 1</b></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            {{-- Tab 1-3 --}}
            <div class="tab-pane fade" id="tabs-1-3">
                <h6>Agenda PIT PAMKI 2024 : Hari ke - 3</h6>
                
                <h6 class="mb-4">Detail Kegiatan Minggu, 8 September 2024 </h6>
               
                <table  class="table" width="100%" border="1">
                    <thead class="thead-dark text-white text-center">
                      <tr>
                        <th scope="col" width="30%">Waktu</th>
                        <th scope="col">Kegiatan</th>
                      </tr>
                    </thead>
                    <tbody class="text-dark">
                        <tr>
                            <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium IV</i><br>
                            Theme : Update in Tuberculosis Management</th>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">07:00 - 08:00 WIB</th>
                            <td>
                                <p>Meet The Expert</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">08:00 - 08:20 WIB</th>
                            <td>
                                <p><b>Topic : </b> <br> 
                                    Update Diagnosiss TB : New Diagnostic Tools<br>
                                   <b>Speaker :</b> </p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">08:20 - 08:40 WIB</th>
                            <td>
                                <p><b>Topic : </b> <br> 
                                    Update Management of MDR TB in Indonesia<br>
                                   <b>Speaker :</b> </p>
                            </td>
                        </tr>                        
                        <tr>
                            <th class="text-center align-middle">08:40 - 09:00 WIB</th>
                            <td>
                                <p>Diskusi dan Tanya Jawab</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">09:00 - 09:15 WIB</th>
                            <td>
                                <p>Industrial Break (Sponsorship)</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">09:15 - 09:30 WIB</th>
                            <td>
                                <p>Coffe Break </p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium V</i><br>
                            Theme : Fastidious Bacterial Infection</th>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">09:30 - 09:50 WIB</th>
                            <td>
                                <p><b>Topic : </b> <br> 
                                    Update on the Diagnosis and Surveillance of MDR N.Gonorrhoeae in Indonesia  <br>
                                   <b>Speaker :</b> </p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">09:50 - 10:10 WIB</th>
                            <td>
                                <p><b>Topic : </b> <br> 
                                    Recent Treatmentsfor N. gonorrhoeae Infection Based on WHO Guidlines <br>
                                   <b>Speaker :</b> </p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">10:10 - 10:30 WIB</th>
                            <td>
                                <p>Diskusi dan Tanya Jawab</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">10:30 - 10:45 WIB</th>
                            <td>
                                <p>Industrial Break (Sponsorship)</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium VI</i><br>
                            Theme : Viral Disease Management</th>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">10:45 - 11:05 WIB</th>
                            <td>
                                <p><b>Topic : </b> <br> 
                                    HIV Resistance in Indonesia <br>
                                   <b>Speaker :</b> </p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">11:05 - 11:25 WIB</th>
                            <td>
                                <p><b>Topic : </b> <br> 
                                    Updates on Emerging and Re-emerging Viral Disease <br>
                                   <b>Speaker :</b> </p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">11:25 - 11:45 WIB</th>
                            <td>
                                <p>Diskusi dan Tanya Jawab</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">11:45 - 12:00 WIB</th>
                            <td>
                                <p>Industrial Break (Sponsorship)</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">12:00 - 13:00 WIB</th>
                            <td>
                                <p>Lunch Simposium II</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium VII</i><br>
                            Theme : Fungal Infectious disease management</th>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">13:00 - 13:20 WIB</th>
                            <td>
                                <p><b>Topic : </b> <br> 
                                    Best Practices in Antifungal Management and WHO fungal Priority Pathogens List <br>
                                   <b>Speaker :</b> </p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">13:20 - 13:40 WIB</th>
                            <td>
                                <p><b>Topic : </b> <br> 
                                    Key Initial Steps When Processing and Direct Examintaion of Clinical Specimens for Fungal Examination <br>
                                   <b>Speaker :</b> </p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">13:40 - 14:00 WIB</th>
                            <td>
                                <p>Diskusi dan Tanya Jawab</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle">14:00 - 14:15 WIB</th>
                            <td>
                                <p>Industrial Break (Sponsorship)</p>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-center align-middle" rowspan="2">14:15 - 14:30 WIB</th>                            
                            <td>
                                <p><b>Closing Ceremony</b></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
          </div>
        </div>
    </section>
@endsection