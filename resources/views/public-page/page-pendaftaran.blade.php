@extends('layout.pbc-mainbody')

@section('container')

    <!-- Breadcrumbs-->
    <section class="section novi-bg novi-bg-img breadcrumbs-custom">
        <div class="container">
            <div class="breadcrumbs-custom__inner">
            <p class="breadcrumbs-custom__title">Pendaftaran</p>
            <ul class="breadcrumbs-custom__path">
                <li><a href="{{ url('') }}">Home</a></li>
                <li class="active">Pendaftaran</li>
            </ul>
            </div>
        </div>
    </section>

    {{-- Counter Time --}}
    <section class="section novi-bg novi-bg-img bg-accent">
        <div class="container">
          <div class="row row-fix justify-content-md-center ">

            {{-- Text Counter --}}
            <div class="col-md-8 col-lg-6 section-xl">
              <h3><b>PIT PAMKI 2024</b></h3>
              <p class="">Hitungan mundur telah dimulai menuju acara PIT PAMKI 2024. Simposium dan workshop akan menjadi momen penting bersama Perhimpunan Dokter Spesialis Mikrobiologi Klinik Indonesia (PAMKI)</p>
            </div>
            <div class="col-md-8 col-lg-6 section-xl d-flex justify-content-center align-items-center">
              <div class="countdown" data-countdown="data-countdown" data-to="2024-09-06">
                <div class="countdown-block countdown-block-Days">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Days="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Days="">00</div>
                    <div class="countdown-title">Days</div>
                  </div>
                </div>
                <div class="countdown-block countdown-block-Hours">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Hours="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Hours="">00</div>
                    <div class="countdown-title">Hours</div>
                  </div>
                </div>
                <div class="countdown-block countdown-block-Minutes">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Minutes="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Minutes="">00</div>
                    <div class="countdown-title">Minutes</div>
                  </div>
                </div>
                <div class="countdown-block countdown-block-Seconds">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Seconds="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Seconds="">00</div>
                    <div class="countdown-title">Seconds</div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
    </section>

    <section class="section novi-bg bg-gray-lighter object-wrap">
      <div class="section-xxl section-xxl_bigger">
        <div class="container">
          <h3 class="text-center"><b>Registrasi PIT PAMKI 2024</b></h3>
          <div class="row row-fix">
            <div class="col-lg-12">
              <div class="card shadow border " style="border-radius: 10px">
                <div class="card-body">
                  <form class="text-dark mt-5">

                    <div class="form-group">
                      <label for="kategoriKegiatan"><b>Kategori Kegiatan</b></label>
                      <select class="form-control" id="kategoriKegiatan">
                        <option value="" selected disabled hidden>Pilih Kategori Kegiatan</option>
                        <!-- Opsi Kategori Kegiatan -->
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="gelarDepan"><b>Gelar Depan</b></label>
                      <input type="text" class="form-control" id="gelarDepan">
                    </div>
                    <div class="form-group">
                      <label for="namaLengkap"><b>Nama Lengkap</b></label>
                      <input type="text" class="form-control" id="namaLengkap">
                    </div>
                    <div class="form-group">
                      <label for="gelarBelakang"><b>Gelar Belakang</b></label>
                      <input type="text" class="form-control" id="gelarBelakang">
                    </div>
                    <div class="form-group">
                      <label for="jenisKelamin"><b>Jenis Kelamin</b></label>
                      <select class="form-control" id="jenisKelamin">
                        <option value="">Pilih Jenis Kelamin</option>
                        <!-- Opsi Jenis Kelamin -->
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="email"><b>Email</b></label>
                      <input type="email" class="form-control" id="email">
                    </div>
                    <div class="form-group">
                      <label for="noHpWa"><b>No. Hp WA</b></label>
                      <input type="text" class="form-control" id="noHpWa">
                    </div>
                    <div class="form-group">
                      <label for="provinsi"><b>Provinsi</b></label>
                      <select class="form-control" id="provinsi">
                        <option value="">Pilih Provinsi</option>
                        <!-- Opsi Provinsi -->
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="kota"><b>Kota</b></label>
                      <select class="form-control" id="kota">
                        <option value="">Pilih Kota</option>
                        <!-- Opsi Kota -->
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="instansiAsal"><b>Instansi Asal</b></label>
                      <input type="text" class="form-control" id="instansiAsal">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
                </div>
              </div>
              
            </div>
          </div>
        </div>
    </div>

@endsection