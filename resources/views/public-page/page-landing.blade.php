@extends('layout.pbc-mainbody')

@section('container')
      {{-- TITLE AND SLIDER --}}
      <section class="text-center section-xxl fullwidth-page fullwidth-page_mod-1 bg-image-6">
        <div class="container ">
          <h1 style="color:#1057CE"><b>PIT PAMKI 2024</b></h1>
          <div class="row ">
            <div class="col-lg-12">
              <h4 class="text-white font-weight-bold" style="border-radius:40px;padding: 20px; background-color: #1057CE;">"Enchancing Integrated Infectious Disease Management through the <br> Emphasis on Accurate Laboratory Diagnosis and Sustained  Surveillance" </h4>
            </div>
          </div>
          <h5 class="text-dark text-width-2 block-centered mt-4"><b>6 - 8 September 2024 </b><br><b>Hotel Pangeran, Pekanbaru-Riau</b></h5>


          <div class="countdown text-dark" data-countdown="data-countdown" data-to="2024-09-06">
            <div class="countdown-block countdown-block-Days">
              <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Days="">
                <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
              </svg>
              <div class="countdown-wrap">
                <div class="countdown-counter" data-counter-Days="">00</div>
                <div class="countdown-title"><b>Days</b></div>
              </div>
            </div>
            <div class="countdown-block countdown-block-Hours ">
              <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Hours="">
                <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
              </svg>
              <div class="countdown-wrap">
                <div class="countdown-counter" data-counter-Hours="">00</div>
                <div class="countdown-title"><b>Hours</b></div>
              </div>
            </div>
            <div class="countdown-block countdown-block-Minutes">
              <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Minutes="">
                <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
              </svg>
              <div class="countdown-wrap">
                <div class="countdown-counter" data-counter-Minutes="">00</div>
                <div class="countdown-title">Minutes</div>
              </div>
            </div>
            <div class="countdown-block countdown-block-Seconds">
              <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Seconds="">
                <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
              </svg>
              <div class="countdown-wrap">
                <div class="countdown-counter" data-counter-Seconds="">00</div>
                <div class="countdown-title">Seconds</div>
              </div>
            </div>
          </div>

          
          <div class="group-lg group-middle">
            <a class="button btn-danger" href="//www.templatemonster.com/website-templates/monstroid2.html" target="_blank">REGISTRASI</a>
          </div>
        </div>
      </section>



      <!-- Kata Pengantar-->
      <section class="section novi-bg bg-gray-lighter object-wrap">
        <div class="section-xxl section-xxl_bigger">
          <div class="container">
            <div class="row row-fix">
              <div class="col-lg-12">
                <h3 class="text-center"><b>Kata Pengantar</b></h3>
                <p class="text-center text-dark">Assalamualaikum wr. wb.,
                  Assalamualaikum Wr Wb <br>
                    Salam sejawat,<br><br>
                    Puji dan syukur kami panjatkan kehadirat Tuhan Yang Maha Kuasa atas berkah dan rahmatnya yang selalu melingkupi kita semua. Kami mengundang saudara untuk mengikuti Pertemuan Ilmiah Tahunan (PIT) PAMKI 2024 yang akan diselenggarakan di Pekanbaru, Provinsi Riau pada tanggal 6-8 September 2024. PIT PAMKI kali ini mengangkat tema "Enhancing Integrated Infectious Disease Management through the Emphasis on Accurate Laboratory Diagnosis and Sustained Surveillance". Melalui kegiatan ini kami ingin menfokuskan pada pentingnya aspek diagnosis pada tata laksana penyakit infeksi secara keseluruhan. Para expert akan menyampaikan bagaimana memilih diagnosis penyakit infeksi yang tepat, akurat, terkini dan efisien yang akhirnya mendukung luaran pasien yang lebih baik. Pada pertemuan ini juga akan didiskusikan bagaimana mengelola laboratorium yang bermutu dalam pelayanan pasien maupun dalam surveilans penyakit infeksi untuk mendukung penanggulangan penyakit infeksi di masyarakat. Serangkaian kegiatan ilmiah seperti simposium dan workshop, free paper, serta kegiatan yang mendukung kebersamaan akan diselenggarakan. Dokter spesialis mikrobiologi klinik, dokter spesialis lain, dokter umum, tenaga kesehatan lain termasuk peserta didik dapat berpartisipasi pada kegiatan ini. Kami mengharapkan sejawat sekalian dapat berpartisipasi pada keseluruhan acara PIT PAMKI 2024. <br><br> 
                    Sampai berjumpa di Pekanbaru.</p>
              </div>
            </div>
          </div>
        </div>
        
      </section>


      {{-- Tabs Materi & Acara --}}
      <section class="section novi-bg novi-bg-img section-lg bg-default">
          <div class="container">
            <h3 class="text-center"><b>Materi Acara</b></h3>
            <!-- Bootstrap tabs-->
            <div class="tabs-custom tabs-horizontal" id="tabs-1">
              <!-- Nav tabs-->
              <ul class="list-fix nav nav-custom nav-custom-tabs">
                <li class="nav-item"><a class="nav-link" href="#tabs-1-1" data-toggle="tab" style="font-size: 18px">Jumat, 6 September 2024</a></li>
                <li class="nav-item"><a class="nav-link" href="#tabs-1-2" data-toggle="tab" style="font-size: 18px">Sabtu, 7 September 2024</a></li>
                <li class="nav-item"><a class="nav-link" href="#tabs-1-3" data-toggle="tab" style="font-size: 18px">Minggu, 8 September 2024</a></li>
              </ul>
            </div>
            <div class="tab-content text-left">

              {{-- Tab 1-1 --}}
              <div class="tab-pane fade " id="tabs-1-1">
                  <h6>Agenda PIT PAMKI 2024 : Hari ke - 1</h6>                
                  <h6 class="mb-4">Detail Kegiatan Jumat, 6 September 2024 </h6>
                  <style>th, td {
                    padding: 15px;
                  }</style>
                  <table class="table table-sm" width="100%" border="1" >
                      <thead class="thead-dark text-white text-center">
                        <tr>
                          <th scope="col" width="30%">Waktu</th>
                          <th scope="col">Kegiatan</th>
                        </tr>
                      </thead>
                      <tbody class="text-dark">
                          <tr>
                              <th class="text-center align-middle">08:00 - 16:00 WIB</th>
                              <td>
                                  <p><b>Workshop 1 :</b> Quality Assurance in Bacterial Culture and Antimicrobial Susceptibility Test <br>
                                    <b>Workshop 2 :</b> Expertise Microbiology : Bridging from Bench to Word</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">19:00 - 22:00 WIB</th>
                              <td>
                                  <p>Sidang Organisasi</p>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </div>

              {{-- Tab 1-2 --}}
              <div class="tab-pane fade" id="tabs-1-2">
                  <h6>Agenda PIT PAMKI 2024 : Hari ke - 2</h6>
                  
                  <h6 class="mb-4">Detail Kegiatan Sabtu, 7 September 2024 </h6>
                
                  <table  class="table table-sm" width="100%" border="1">
                      <thead class="thead-dark text-white text-center">
                        <tr>
                          <th scope="col" width="30%">Waktu</th>
                          <th scope="col">Kegiatan</th>
                        </tr>
                      </thead>
                      <tbody class="text-dark">
                          <tr>
                              <th class="text-center align-middle">07:00 - 08:00 WIB</th>
                              <td>
                                  <p>Registrasi</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">08:00 - 08:20 WIB</th>
                              <td>
                                  <p>Opening Ceremony</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">08:20 - 08:35 WIB</th>
                              <td>
                                  <p>Sambutan Ketua Panitia <br>
                                    <b>Dr. dr. Dewi Anggraini Sp.MK(K)</b></p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">08:35 - 08:50 WIB</th>
                              <td>
                                  <p>Sambutan Ketua PP PAMKI <br>
                                    <b>Dr. dr. Anis Karuniawati, Ph.D, Sp.MK(K)</b></p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">08:50 - 09:00 WIB</th>
                              <td>
                                  <p>Sambutan ----</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Keynote Speech and Plenary Lecture</i></th>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">09:00 - 09:20 WIB</th>
                              <td>
                                  <p><b>Keynote Speech : </b> <br> 
                                      Strengthening Indonesia's Health Laboratory Network for Enhanced Surveillance and Diagnosis of Infectious Diseases, Promoting Early Detection and Comprehensive Disease Management <br>
                                    <b>Speaker :</b> Representative from Ministry of Health</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">09:20 - 09:40 WIB</th>
                              <td>
                                  <p><b>Plenary Lecture : </b> <br> 
                                      Ethical and Legal Challenges in Laboratory Diagnostics <br>
                                    <b>Speaker :</b> </p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">09:40 - 10:00 WIB</th>
                              <td>
                                  <p><b>Plenary Lecture : </b> <br> 
                                      SINAR : PAMKI's Antimicrobial Resistance Surveilance System and Its Impact on Atimicrobial Resistance Control in Indonesia <br>
                                    <b>Speaker :</b> </p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">10:00 - 10:15 WIB</th>
                              <td>
                                  <p>Kunjungan ke <i>exhibition</i> dan <i>coffe break</i></p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium I</i><br>
                              Theme : Management for MDR infection</th>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">10:15 - 10:35 WIB</th>
                              <td>
                                  <p><b>Topic : </b> <br> 
                                      Multidrug-Resistant Gram-Negative Bacterial Infections in the Hospital <br>
                                      Setting: Emerging Treatment Options <br>
                                    <b>Speaker :</b> </p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">10:35 - 10:55 WIB</th>
                              <td>
                                  <p><b>Topic : </b> <br> 
                                      Optimizing the Role of Microbiology Laboratories in Detecting Multidrug-Resistant Bacteria <br>
                                    <b>Speaker :</b> </p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">10:55 - 11:15 WIB</th>
                              <td>
                                  <p>Diskusi dan Tanya Jawab</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">11:15 - 11:30 WIB</th>
                              <td>
                                  <p>Industrial Break (Sponsorship)</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">11:30 - 13:00 WIB</th>
                              <td>
                                  <p>Lunch Simposium I</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium II</i><br>
                              Theme : Advance infectious disease diagnostic assay</th>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">13:00 - 13:20 WIB</th>
                              <td>
                                  <p><b>Topic : </b> <br> 
                                      The Role of Metagenomics and Next-Generation Sequencing in Infectious Disease Diagnosis <br>
                                    <b>Speaker :</b> </p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">13:20 - 13:40 WIB</th>
                              <td>
                                  <p><b>Topic : </b> <br> 
                                      CRISPR technology: new paradigm to target the infectious disease pathogens or Nanotechnology for diagnosis and treatment of infectious diseases <br>
                                    <b>Speaker :</b> </p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">13:40 - 14:00 WIB</th>
                              <td>
                                  <p>Diskusi dan Tanya Jawab</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">14:00 - 14:15 WIB</th>
                              <td>
                                  <p>Industrial Break (Sponsorship)</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium III</i><br>
                              Theme : Prevention and Control of infection</th>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">14:15 - 14:35 WIB</th>
                              <td>
                                  <p><b>Topic : </b> <br> 
                                      Hospital Environmental Cleaning to Prevent MDRO Spread in the ICU <br>
                                    <b>Speaker :</b> </p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">14:35 - 14:55 WIB</th>
                              <td>
                                  <p><b>Topic : </b> <br> 
                                      Reuse of single use medical devices : clinical and economic outcomes, legal and ethical issues, and current hospital practice <br>
                                    <b>Speaker :</b> </p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">14:55 - 15:15 WIB</th>
                              <td>
                                  <p>Diskusi dan Tanya Jawab</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">15:15 - 15:30 WIB</th>
                              <td>
                                  <p>Industrial Break (Sponsorship)</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle" rowspan="2">15:30 - 15:45 WIB</th>
                              <td>
                                  <p><b>Penutupan Simposium Hari I</b></p>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <p><b>Closing Remarks Day 1</b></p>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </div>

              {{-- Tab 1-3 --}}
              <div class="tab-pane fade" id="tabs-1-3">
                  <h6>Agenda PIT PAMKI 2024 : Hari ke - 3</h6>
                  
                  <h6 class="mb-4">Detail Kegiatan Minggu, 8 September 2024 </h6>
                
                  <table  class="table table-sm" width="100%" border="1">
                      <thead class="thead-dark text-white text-center">
                        <tr>
                          <th scope="col" width="30%">Waktu</th>
                          <th scope="col">Kegiatan</th>
                        </tr>
                      </thead>
                      <tbody class="text-dark">
                          <tr>
                              <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium IV</i><br>
                              Theme : Update in Tuberculosis Management</th>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">07:00 - 08:00 WIB</th>
                              <td>
                                  <p>Meet The Expert</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">08:00 - 08:20 WIB</th>
                              <td>
                                  <p><b>Topic : </b> <br> 
                                      Update Diagnosiss TB : New Diagnostic Tools<br>
                                    <b>Speaker :</b> </p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">08:20 - 08:40 WIB</th>
                              <td>
                                  <p><b>Topic : </b> <br> 
                                      Update Management of MDR TB in Indonesia<br>
                                    <b>Speaker :</b> </p>
                              </td>
                          </tr>                        
                          <tr>
                              <th class="text-center align-middle">08:40 - 09:00 WIB</th>
                              <td>
                                  <p>Diskusi dan Tanya Jawab</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">09:00 - 09:15 WIB</th>
                              <td>
                                  <p>Industrial Break (Sponsorship)</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">09:15 - 09:30 WIB</th>
                              <td>
                                  <p>Coffe Break </p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium V</i><br>
                              Theme : Emerging and Neglected Infetious Diseases : Insights, Advances, and Challenges</th>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">09:30 - 09:50 WIB</th>
                              <td>
                                  <p><b>Topic : </b> <br> 
                                      Melioidosis diseases in Indonesia : Experience in Riau Province <br>
                                    <b>Speaker :</b> </p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">09:50 - 10:10 WIB</th>
                              <td>
                                  <p><b>Topic : </b> <br> 
                                      Streptococcus suis infection in Indonesia : Experience in Bali <br>
                                    <b>Speaker :</b> </p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">10:10 - 10:30 WIB</th>
                              <td>
                                  <p>Diskusi dan Tanya Jawab</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">10:30 - 10:45 WIB</th>
                              <td>
                                  <p>Industrial Break (Sponsorship)</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium VI</i><br>
                              Theme : Viral Disease Management</th>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">10:45 - 11:05 WIB</th>
                              <td>
                                  <p><b>Topic : </b> <br> 
                                      HIV Resistance in Indonesia <br>
                                    <b>Speaker :</b> </p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">11:05 - 11:25 WIB</th>
                              <td>
                                  <p><b>Topic : </b> <br> 
                                      Updates on Emerging and Re-emerging Viral Disease <br>
                                    <b>Speaker :</b> </p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">11:25 - 11:45 WIB</th>
                              <td>
                                  <p>Diskusi dan Tanya Jawab</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">11:45 - 12:00 WIB</th>
                              <td>
                                  <p>Industrial Break (Sponsorship)</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">12:00 - 13:00 WIB</th>
                              <td>
                                  <p>Lunch Simposium II</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle bg-secondary text-white" colspan="2"><i>Simposium VII</i><br>
                              Theme : Fungal Infectious disease management</th>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">13:00 - 13:20 WIB</th>
                              <td>
                                  <p><b>Topic : </b> <br> 
                                      Best Practices in Antifungal Management and WHO fungal Priority Pathogens List <br>
                                    <b>Speaker :</b> </p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">13:20 - 13:40 WIB</th>
                              <td>
                                  <p><b>Topic : </b> <br> 
                                      Key Initial Steps When Processing and Direct Examintaion of Clinical Specimens for Fungal Examination <br>
                                    <b>Speaker :</b> </p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">13:40 - 14:00 WIB</th>
                              <td>
                                  <p>Diskusi dan Tanya Jawab</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle">14:00 - 14:15 WIB</th>
                              <td>
                                  <p>Industrial Break (Sponsorship)</p>
                              </td>
                          </tr>
                          <tr>
                              <th class="text-center align-middle" rowspan="2">14:15 - 14:30 WIB</th>                            
                              <td>
                                  <p><b>Closing Ceremony</b></p>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </div>
            </div>
          </div>
      </section>

      {{-- Daftar Harga Paket --}}
      <section class="section novi-bg bg-gray-lighter novi-bg-img section-lg bg-default">
        <div class="container "> <br>
          <h3 class="text-center mt-5"><b>Biaya Pendaftaran</b></h3>
          <div class="row row-50">

            <table class="table table-responsive" width="100%" border="1">
              <thead class="bg-secondary text-white">
                  <tr>
                    <td class="text-center align-middle font-weight-bold" rowspan="2" width="20%">Time</td>
                    <td class="text-center font-weight-bold" colspan="4">National Participant</td>
                    <td class="text-center font-weight-bold" colspan="2">International Participant</td>
                  </tr>
                  <tr>
                    <td class="text-center align-middle" width="12%">Specialist PAMKI Member</td>
                    <td class="text-center align-middle">Specialist Non-PAMKI Member</td>
                    <td class="text-center align-middle" width="12%">Other</td>
                    <td class="text-center align-middle">Clinical Microbiology Resident</td>
                    <td class="text-center align-middle">Consultant</td>
                    <td class="text-center align-middle" width="12%">Trainee</td>
                  </tr>
              </thead>
              <tbody class="text-dark font-weight-bold">
                <tr>
                  <td class="align-middle">Early Registration <br> (Until 30 April 2024)</td>
                  <td class="align-middle text-center">IDR 1,000K</td>
                  <td class="align-middle text-center">IDR 1,500K</td>
                  <td class="align-middle text-center">IDR 800K</td>
                  <td class="align-middle text-center">IDR 750K</td>
                  <td class="align-middle text-center">US $ 150K</td>
                  <td class="align-middle text-center">US $ 75K</td>
                </tr>
                <tr>
                  <td class="align-middle">Mid Registration </td>
                  <td class="align-middle text-center">IDR 1,200K</td>
                  <td class="align-middle text-center">IDR 1,800K</td>
                  <td class="align-middle text-center">IDR 1,000K</td>
                  <td class="align-middle text-center">IDR 850K</td>
                  <td class="align-middle text-center">US $ 200</td>
                  <td class="align-middle text-center">US $ 85</td>
                </tr>
                <tr>
                  <td class="align-middle">On-Site</td>
                  <td class="align-middle text-center">IDR 1,500K</td>
                  <td class="align-middle text-center">IDR 2,000K</td>
                  <td class="align-middle text-center">IDR 1,200K</td>
                  <td class="align-middle text-center">IDR 1,000K</td>
                  <td class="align-middle text-center">US $ 250</td>
                  <td class="align-middle text-center">US $ 100</td>
                </tr>
              </tbody>
            </table>

            

            

          </div>
          <p class="text-center">
            <h4><a class="button button-primary button-block" href="{{ url('pendaftaran') }}" target="_blank">REGISTRASI
            </a></h4>
          </p>

        </div>
      </section>

      

      <section class="section parallax-container text-center parallax-header context-dark" data-parallax-img="images/parallax-03.jpg">
        <div class="parallax-content">
          <div class="parallax-header__inner">
            <div class="parallax-header__content">
              <div class="container">
                <div class="row row-fix justify-content-sm-center">
                  <div class="col-md-10 col-xl-6 text-left">
                    <h4><b>TEMPAT PENYELENGGARAAN</b></h4>
                    <h4>Hotel Pangeran</h4>
                    <img src="images/pangeran-log.png" alt="">
                  </div>
                  <div class="col-md-10 col-xl-6 text-left">
                    <h4><b>ALAMAT</b></h4> 
                    <a href="https://maps.app.goo.gl/f6DKhFeBT5DjS8678" target="_blank"><p><i class="fa-solid fa-map-location-dot fa-lg mr-3"></i> Jl. Jend. Sudirman No.371-373, Cinta Raja, Kec. Sail, Kota Pekanbaru, Riau 28126</p></a>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      


      {{-- Daftar Harga Paket --}}
      <section class="section novi-bg novi-bg-img section-lg bg-default">
        <div class="container">
          <h3 class="text-center mt-5"><b>Akomodasi</b></h3>
          <div class="row row-50">

            <div class="col-md-4 col-xl-3">
              <div class="card border shadow h-100" style="border-radius: 10px">
                <div class="card-body">
                  <div class="pricing-header text-center">
                    <h5 class="mt-3"><b>Hotel Pangeran</b></h5>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                  </div>
                  <hr class="mb-3 text-dark">
                  <div class="p-4 text-dark" style="font-size: 15px">
                    Jl. Jend Sudirman No 373 – Pku <br>
                    Tlp. (0761) 853636 <br>
                    Fax (0761) 853232
                    
                  </div>
                </div>
              </div>
              
            </div>

            <div class="col-md-4 col-xl-3">
              <div class="card border shadow h-100" style="border-radius: 10px">
                <div class="card-body">
                  <div class="pricing-header text-center">
                    <h5 class="mt-3"><b>Hotel Grand Central</b></h5>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                  </div>
                  <hr class="mb-3 text-dark">
                  <div class="p-4 text-dark" style="font-size: 15px">
                    Jl. Jendral Sudirman No.1  <br>
                    Pekanbaru, Riau <br>
                    Telp. (0761)7872616 <br>
                    Fax. (0761)7873030 <br>
                  </div>
                </div>
              </div>
              
            </div>

            <div class="col-md-4 col-xl-3">
              <div class="card border shadow h-100" style="border-radius: 10px">
                <div class="card-body">
                  <div class="pricing-header text-center">
                    <h5 class="mt-3"><b>Hotel The Premiere</b></h5>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                  </div>
                  <hr class="mb-3 text-dark">
                  <div class="p-4 text-dark" style="font-size: 15px">
                    Jl. Jend Sudirman No. 389 <br>
                    Pekanbaru, Riau <br>
                    Telp. (0761)7891818 <br>
                  </div>
                </div>
              </div>
              
            </div>

            <div class="col-md-4 col-xl-3">
              <div class="card border shadow h-100" style="border-radius: 10px">
                <div class="card-body">
                  <div class="pricing-header text-center">
                    <h5 class="mt-3"><b>Hotel Grand Jatra</b></h5>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                  </div>
                  <hr class="mb-3 text-dark">
                  <div class="p-4 text-dark" style="font-size: 15px">
                    Jl. Tengku Zainal Abidin No.1 <br>
                    Pekanbaru, Riau <br>
                    Telp. (07610850888) <br>
                    fax. (0761)850999/8500355 <br>
                  </div>
                </div>
              </div>
              
            </div>

            <div class="col-md-4 col-xl-3">
              <div class="card border shadow h-100" style="border-radius: 10px">
                <div class="card-body">
                  <div class="pricing-header text-center">
                    <h5 class="mt-3"><b>Hotel Aryaduta</b></h5>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                  </div>
                  <hr class="mb-3 text-dark">
                  <div class="p-4 text-dark" style="font-size: 15px">
                    Jl. Diponegoro 34 - Pku  <br>
                    Tlp. (0761) 44200 <br>
                    Fax (0761) 44210 <br>
                  </div>
                </div>
              </div>
              
            </div>

            <div class="col-md-4 col-xl-3">
              <div class="card border shadow h-100" style="border-radius: 10px">
                <div class="card-body">
                  <div class="pricing-header text-center">
                    <h5 class="mt-3"><b>Hotel Novotel</b></h5>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                  </div>
                  <hr class="mb-3 text-dark">
                  <div class="p-4 text-dark" style="font-size: 15px">
                    Jl Riau No 59 - Kp Baru Kec Senapelan <br>
                    Pekanbaru, Riau <br>
                    Telp. (0761) 25599 <br>
                  </div>
                </div>
              </div>
              
            </div>

            <div class="col-md-4 col-xl-3">
              <div class="card border shadow h-100" style="border-radius: 10px">
                <div class="card-body">
                  <div class="pricing-header text-center">
                    <h5 class="mt-3"><b>Hotel Cempaka House Syariah</b></h5>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                    <i class="fa-solid fa-star" style="color:gold"></i>
                  </div>
                  <hr class="mb-3 text-dark">
                  <div class="p-4 text-dark" style="font-size: 15px">
                    (Boutique Hotel) <br>
                    Jl. Arifin Ahmad No. 58  <br>
                    Pekanbaru, Riau <br>
                    Telp. (0761) 7545700 <br>

                  </div>
                </div>
              </div>
              
            </div>
           


          </div>
        </div>
      </section>

      


      <!-- Gallery-->
      <section class="section novi-bg novi-bg-img section-xl bg-default" data-lightgallery="group">
        <div class="container-fluid">
          <div class="row row-10 row-horizontal-10">
            <div class="col-md-4 col-xl-3"><a class="thumb-modern" data-lightgallery="item" href="images/image-original-1-1200x905.jpg">
                <figure><img src="images/home-default-7-472x355.jpg" alt="" width="472" height="355"/>
                </figure>
                <div class="thumb-modern__overlay"></div></a></div>
            <div class="col-md-4 col-xl-3"><a class="thumb-modern" data-lightgallery="item" href="images/image-original-2-1200x905.jpg">
                <figure><img src="images/home-default-8-472x355.jpg" alt="" width="472" height="355"/>
                </figure>
                <div class="thumb-modern__overlay"></div></a></div>
            <div class="col-md-4 col-xl-3"><a class="thumb-modern" data-lightgallery="item" href="images/image-original-3-1200x906.jpg">
                <figure><img src="images/home-default-9-472x355.jpg" alt="" width="472" height="355"/>
                </figure>
                <div class="thumb-modern__overlay"></div></a></div>
            <div class="col-md-4 col-xl-3"><a class="thumb-modern" data-lightgallery="item" href="images/image-original-4-1200x905.jpg">
                <figure><img src="images/home-default-10-472x355.jpg" alt="" width="472" height="355"/>
                </figure>
                <div class="thumb-modern__overlay"></div></a></div>
            <div class="col-md-4 col-xl-3"><a class="thumb-modern" data-lightgallery="item" href="images/image-original-5-1200x905.jpg">
                <figure><img src="images/home-default-11-472x355.jpg" alt="" width="472" height="355"/>
                </figure>
                <div class="thumb-modern__overlay"></div></a></div>
            <div class="col-md-4 col-xl-3"><a class="thumb-modern" data-lightgallery="item" href="images/image-original-6-1200x905.jpg">
                <figure><img src="images/home-default-12-472x355.jpg" alt="" width="472" height="355"/>
                </figure>
                <div class="thumb-modern__overlay"></div></a></div>
            <div class="col-md-4 col-xl-3"><a class="thumb-modern" data-lightgallery="item" href="images/image-original-7-1200x906.jpg">
                <figure><img src="images/home-default-13-472x355.jpg" alt="" width="472" height="355"/>
                </figure>
                <div class="thumb-modern__overlay"></div></a></div>
            <div class="col-md-4 col-xl-3"><a class="thumb-modern" data-lightgallery="item" href="images/image-original-8-1200x906.jpg">
                <figure><img src="images/home-default-14-472x355.jpg" alt="" width="472" height="355"/>
                </figure>
                <div class="thumb-modern__overlay"></div></a></div>
          </div>
        </div>
      </section>



      <!-- Subscribe form-->
      <section class="section novi-bg novi-bg-img section-xl bg-default text-center">
        <div class="container">
          <h5>Get the latest news delivered daily!<br> We will send you breaking news right to your inbox.</h5>
          <!-- RD Mailform-->
          <form class="rd-mailform rd-mailform_style-1 rd-mailform_sizing-1 rd-mailform-inline-flex text-center" data-form-output="form-output-global" data-form-type="subscribe" method="post" action="bat/rd-mailform.php">
            <div class="form-wrap form-wrap_icon"><span class="novi-icon form-icon linear-icon-envelope"></span>
              <input class="form-input" id="contact-email" type="email" name="email" >
              <label class="form-label" for="contact-email">Enter please your e-mail</label>
            </div>
            <button class="button button-primary" type="submit">Subscribe!</button>
          </form>
        </div>
      </section>
@endsection