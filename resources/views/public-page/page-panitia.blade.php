@extends('layout.pbc-mainbody')

@section('container')

    <!-- Breadcrumbs-->
    <section class="section novi-bg novi-bg-img breadcrumbs-custom">
        <div class="container">
            <div class="breadcrumbs-custom__inner">
            <p class="breadcrumbs-custom__title">Panitia</p>
            <ul class="breadcrumbs-custom__path">
                <li><a href="{{ url('') }}">Home</a></li>
                <li class="active">Panitia</li>
            </ul>
            </div>
        </div>
    </section>

    {{-- Counter Time --}}
    <section class="section novi-bg novi-bg-img bg-accent">
        <div class="container">
          <div class="row row-fix justify-content-md-center ">

            {{-- Text Counter --}}
            <div class="col-md-8 col-lg-6 section-xl">
              <h3><b>PIT PAMKI 2024</b></h3>
              <p class="">Hitungan mundur telah dimulai menuju acara PIT PAMKI 2024. Simposium dan workshop akan menjadi momen penting bersama Perhimpunan Dokter Spesialis Mikrobiologi Klinik Indonesia (PAMKI)</p>
            </div>
            <div class="col-md-8 col-lg-6 section-xl d-flex justify-content-center align-items-center">
              <div class="countdown" data-countdown="data-countdown" data-to="2024-09-06">
                <div class="countdown-block countdown-block-Days">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Days="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Days="">00</div>
                    <div class="countdown-title">Days</div>
                  </div>
                </div>
                <div class="countdown-block countdown-block-Hours">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Hours="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Hours="">00</div>
                    <div class="countdown-title">Hours</div>
                  </div>
                </div>
                <div class="countdown-block countdown-block-Minutes">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Minutes="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Minutes="">00</div>
                    <div class="countdown-title">Minutes</div>
                  </div>
                </div>
                <div class="countdown-block countdown-block-Seconds">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Seconds="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Seconds="">00</div>
                    <div class="countdown-title">Seconds</div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
    </section>

    <section class="section novi-bg novi-bg-img bg-default section-lg">
        <div class="container">
            <h3 class="text-center mt-5"><b>Susunan Kepanitian</b></h3>
            <p class="text-center text-dark">Kegiatan ini diselenggarakan oleh PAMKI PUSAT bekerja sama dengan PAMKI Cabang Riau</p>
            <table class="table text-dark mt-5">
                <tr>
                    <td colspan="3"><b>PEMBINA</b></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>dr. Anis Karuniawati, Ph.D, Sp.MK(K)</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Dr. dr. Wani Devita Gunardi, Sp.MK(K)</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Prof. dr. Amin Soebandrio,Ph.D, Sp.MK(K)</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Prof. Dr. dr. Kuntaman, MS, Sp.MK(K)</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>KETUA</b></td>
                    <td>:</td>
                    <td> Dr. dr. Dewi Anggraini, Sp.MK(K)</td>
                </tr>
                <tr>
                    <td><b>WAKIL KETUA</b></td>
                    <td>:</td>
                    <td> Dr. dr. Maya Savira, M.Kes</td>
                </tr>
                <tr>
                    <td><b>SEKRETARIS</b> </td>
                    <td>:</td>
                    <td>dr. Dewi Yana, Sp.MK</td>
                </tr>
                <tr>
                    <td><b>BENDAHARA</b> </td>
                    <td>:</td>
                    <td>dr. Fauzia Andrini, M.Kes</td>
                </tr>
                <tr>
                    <td><b>SIE ACARA</b> </td>
                    <td>:</td>
                    <td>dr. Eckert Simata Uli Hutapea, Sp.MK</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>drh. Ghiandra NS Saukhan, M.Si</td>
                </tr>
                <tr>
                    <td><b>SIE ILMIAH</b> </td>
                    <td>:</td>
                    <td> dr. Dian Oktavianti Putri, Sp.MK</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>drh. Rifia Tiara Fani, M.Sc</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>drh. Mulya Fitranda. AR,M.Sc</td>
                </tr>
                <tr>
                    <td><b>SIE PERLENGKAPAN / SIE TRANSPORTASI</b> </td>
                    <td>:</td>
                    <td> dr. Surya Darma, Sp.MK</td>
                </tr>
                <tr>
                    <td><b>SIE DOKUMENTASI</b> </td>
                    <td>:</td>
                    <td>dr. Irham Taslim, Sp.MK</td>
                </tr>
                <tr>
                    <td><b>SIE AKOMODASI </b></td>
                    <td>:</td>
                    <td>dr. Evelyne Nida Tiotantra, Sp.MK</td>
                </tr>
                <tr>
                    <td><b>SIE. HUMAS DAN PUBLIKASI</b> </td>
                    <td>:</td>
                    <td> drg. Rita Endriani, M.Kes</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Khikam</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Anggiat</td>
                </tr>
                <tr>
                    <td><b>SEKRETARIAT ACARA</b></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>Narahubung</b> </td>
                    <td>:</td>
                    <td></td>
                </tr>
                <tr>
                    <td class="align-middle"><b>SPONSORSHIP</b> </td>
                    <td class="align-middle">:</td>
                    <td>&quot;Dr. dr. Dewi Anggraini, Sp.MK(K) <br> (Hp: 081280104689) <br> email: pamkicabangriau@gmail.com&quot;</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><b>SEKRETARIAT</b></td>
                    <td>:</td>
                    <td>Sundari</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Febi</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Agnes Rica</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Mislinda</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Zizi</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Erla</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Ramadina</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>Putri</td>
                </tr>
            </table>
        </div>
    </section>

@endsection