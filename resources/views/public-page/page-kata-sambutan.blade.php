@extends('layout.pbc-mainbody')

@section('container')

    <!-- Breadcrumbs-->
    <section class="section novi-bg novi-bg-img breadcrumbs-custom">
        <div class="container">
            <div class="breadcrumbs-custom__inner">
            <p class="breadcrumbs-custom__title">Kata Sambutan</p>
            <ul class="breadcrumbs-custom__path">
                <li><a href="{{ url('') }}">Home</a></li>
                <li class="active">Kata Sambutan</li>
            </ul>
            </div>
        </div>
    </section>

    {{-- Counter Time --}}
    <section class="section novi-bg novi-bg-img bg-accent">
        <div class="container">
          <div class="row row-fix justify-content-md-center ">

            {{-- Text Counter --}}
            <div class="col-md-8 col-lg-6 section-xl">
              <h3><b>PIT PAMKI 2024</b></h3>
              <p class="">Hitungan mundur telah dimulai menuju acara PIT PAMKI 2024. Simposium dan workshop akan menjadi momen penting bersama Perhimpunan Dokter Spesialis Mikrobiologi Klinik Indonesia (PAMKI)</p>
            </div>
            <div class="col-md-8 col-lg-6 section-xl d-flex justify-content-center align-items-center">
              <div class="countdown" data-countdown="data-countdown" data-to="2024-09-06">
                <div class="countdown-block countdown-block-Days">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Days="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Days="">00</div>
                    <div class="countdown-title">Days</div>
                  </div>
                </div>
                <div class="countdown-block countdown-block-Hours">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Hours="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Hours="">00</div>
                    <div class="countdown-title">Hours</div>
                  </div>
                </div>
                <div class="countdown-block countdown-block-Minutes">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Minutes="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Minutes="">00</div>
                    <div class="countdown-title">Minutes</div>
                  </div>
                </div>
                <div class="countdown-block countdown-block-Seconds">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Seconds="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Seconds="">00</div>
                    <div class="countdown-title">Seconds</div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
    </section>

    <section class="section novi-bg novi-bg-img bg-default section-lg">
        <div class="container">
          <div class="row row-fix row-60 justify-content-sm-center">
            <div class="col-lg-8 section-divided__main group-post">
              <div class="section-md post-single-body">
                <!-- Quote default-->
                <div class="quote-default">
                    <svg class="quote-default__mark" version="1.1" baseprofile="tiny" x="0px" y="0px" width="30.234px" height="23.484px" viewbox="0 0 30.234 23.484">
                      <g>
                        <path d="M12.129,0v1.723c-2.438,0.891-4.348,2.291-5.73,4.201c-1.383,1.911-2.074,3.897-2.074,5.959 c0,0.445,0.07,0.773,0.211,0.984c0.093,0.141,0.199,0.211,0.316,0.211c0.117,0,0.293-0.082,0.527-0.246 c0.75-0.539,1.699-0.809,2.848-0.809c1.336,0,2.519,0.545,3.551,1.635c1.031,1.09,1.547,2.385,1.547,3.885 c0,1.57-0.592,2.953-1.775,4.148c-1.184,1.195-2.619,1.793-4.307,1.793c-1.969,0-3.668-0.809-5.098-2.426 C0.715,19.441,0,17.274,0,14.555c0-3.164,0.972-6,2.918-8.508C4.863,3.539,7.933,1.524,12.129,0z M29.039,0v1.723 c-2.438,0.891-4.348,2.291-5.73,4.201c-1.383,1.911-2.074,3.897-2.074,5.959c0,0.445,0.07,0.773,0.211,0.984 c0.094,0.141,0.199,0.211,0.316,0.211s0.293-0.082,0.527-0.246c0.75-0.539,1.699-0.809,2.848-0.809c1.336,0,2.52,0.545,3.551,1.635 s1.547,2.385,1.547,3.885c0,1.57-0.592,2.953-1.775,4.148s-2.619,1.793-4.307,1.793c-1.969,0-3.668-0.809-5.098-2.426 s-2.145-3.785-2.145-6.504c0-3.164,0.973-6,2.918-8.508C21.773,3.539,24.844,1.524,29.039,0z"></path>
                      </g>
                    </svg>
                    <div class="quote-default__text">
                      <p class="q heading-4">Kata Pengantar & Sambutan PIT PAMKI 2024</p>
                    </div>
                    {{-- <p class="quote-default__cite">Adam Smith</p> --}}
                </div>                
                <img src="images/nonstandart-post-1-886x668.jpg" alt="" height="350"/>

                <p class="text-justify text-dark">
                    Assalamualaikum Wr Wb <br>
                    Salam sejawat,<br><br>
                    Puji dan syukur kami panjatkan kehadirat Tuhan Yang Maha Kuasa atas berkah dan rahmatnya yang selalu melingkupi kita semua. Kami mengundang saudara untuk mengikuti Pertemuan Ilmiah Tahunan (PIT) PAMKI 2024 yang akan diselenggarakan di Pekanbaru, Provinsi Riau pada tanggal 6-8 September 2024. PIT PAMKI kali ini mengangkat tema "Enhancing Integrated Infectious Disease Management through the Emphasis on Accurate Laboratory Diagnosis and Sustained Surveillance". Melalui kegiatan ini kami ingin menfokuskan pada pentingnya aspek diagnosis pada tata laksana penyakit infeksi secara keseluruhan. Para expert akan menyampaikan bagaimana memilih diagnosis penyakit infeksi yang tepat, akurat, terkini dan efisien yang akhirnya mendukung luaran pasien yang lebih baik. Pada pertemuan ini juga akan didiskusikan bagaimana mengelola laboratorium yang bermutu dalam pelayanan pasien maupun dalam surveilans penyakit infeksi untuk mendukung penanggulangan penyakit infeksi di masyarakat. Serangkaian kegiatan ilmiah seperti simposium dan workshop, free paper, serta kegiatan yang mendukung kebersamaan akan diselenggarakan. Dokter spesialis mikrobiologi klinik, dokter spesialis lain, dokter umum, tenaga kesehatan lain termasuk peserta didik dapat berpartisipasi pada kegiatan ini. Kami mengharapkan sejawat sekalian dapat berpartisipasi pada keseluruhan acara PIT PAMKI 2024. <br><br> 
                    Sampai berjumpa di Pekanbaru.</p>
                
              </div>
              <div class="section-md">
                <div class="row row-60 justify-content-md-between">
                  <div class="col-md-5 text-md-left"><a class="unit flex-row unit-spacing-md align-items-center" href="standard-post.html">
                      <div class="unit-left"><span class="icon novi-icon icon-md linear-icon-arrow-left"></span></div>
                      <div class="unit-body">
                        <p class="small">Susunan Panitia Acara PIT PAMKI 2024</p>
                      </div></a></div>
                  <div class="col-md-5 text-md-right"><a class="unit flex-row unit-spacing-md align-items-center" href="standard-post.html">
                      <div class="unit-body">
                        <p class="small">Detail Kegiatan dan Materi Acara PIT PAMKI 2024</p>
                      </div>
                      <div class="unit-right"><span class="icon novi-icon icon-md linear-icon-arrow-right"></span></div></a></div>
                </div>
              </div>
              
              
            </div>
          </div>
        </div>
      </section>

@endsection