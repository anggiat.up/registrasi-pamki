@extends('layout.pbc-mainbody')

@section('container')

    <!-- Breadcrumbs-->
    <section class="section novi-bg novi-bg-img breadcrumbs-custom">
        <div class="container">
            <div class="breadcrumbs-custom__inner">
            <p class="breadcrumbs-custom__title">Kontak Kami</p>
            <ul class="breadcrumbs-custom__path">
                <li><a href="{{ url('') }}">Home</a></li>
                <li class="active">Kontak Kami</li>
            </ul>
            </div>
        </div>
    </section>

    {{-- Counter Time --}}
    <section class="section novi-bg novi-bg-img bg-accent">
        <div class="container">
          <div class="row row-fix justify-content-md-center ">

            {{-- Text Counter --}}
            <div class="col-md-8 col-lg-6 section-xl">
              <h3><b>PIT PAMKI 2024</b></h3>
              <p class="">Hitungan mundur telah dimulai menuju acara PIT PAMKI 2024. Simposium dan workshop akan menjadi momen penting bersama Perhimpunan Dokter Spesialis Mikrobiologi Klinik Indonesia (PAMKI)</p>
            </div>
            <div class="col-md-8 col-lg-6 section-xl d-flex justify-content-center align-items-center">
              <div class="countdown" data-countdown="data-countdown" data-to="2024-09-06">
                <div class="countdown-block countdown-block-Days">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Days="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Days="">00</div>
                    <div class="countdown-title">Days</div>
                  </div>
                </div>
                <div class="countdown-block countdown-block-Hours">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Hours="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Hours="">00</div>
                    <div class="countdown-title">Hours</div>
                  </div>
                </div>
                <div class="countdown-block countdown-block-Minutes">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Minutes="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Minutes="">00</div>
                    <div class="countdown-title">Minutes</div>
                  </div>
                </div>
                <div class="countdown-block countdown-block-Seconds">
                  <svg class="countdown-circle" x="0" y="0" width="130" height="130" viewbox="0 0 130 130" data-progress-Seconds="">
                    <circle class="countdown-circle-bg" cx="65" cy="65" r="64"></circle>
                    <circle class="countdown-circle-fg clipped" cx="65" cy="65" r="64"></circle>
                  </svg>
                  <div class="countdown-wrap">
                    <div class="countdown-counter" data-counter-Seconds="">00</div>
                    <div class="countdown-title">Seconds</div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
    </section>

    <section class="section novi-bg novi-bg-img section-lg bg-default">
        <div class="container">
            <div class="row row-fix row-50">
                <div class="col-md-5 col-lg-4">
                <h3>Detail Kontak</h3>
                <ul class="list-xs contact-info">
                    <li>
                    Hubungi kami melalui kontak berikut
                    </li>
                    <li>
                    <dl class="list-terms-minimal">
                        <dt>Phones</dt>
                        <dd>
                        <ul class="list-semicolon">
                            <li><a href="tel:#">(800) 123-0045</a></li>
                        </ul>
                        </dd>
                    </dl>
                    </li>
                    <li>
                    <dl class="list-terms-minimal text-dark">
                        <dt>E-mail</dt>
                        <dd><a href="mailto:#">info@demolink.org</a></dd>
                    </dl>
                    </li>        
                </ul>
                </div>
                <div class="col-md-7 col-lg-8">
                <h3>Let’s Get in Touch</h3>
                <!-- RD Mailform-->
                <form class="rd-mailform rd-mailform_style-1" data-form-output="form-output-global" data-form-type="contact" method="post" action="">
                    {{-- <div class="">
                        <span class="novi-icon form-icon linear-icon-man"></span>
                        <input class="form-input" id="contact-name" type="text" name="name" data-constraints="Required">
                        {{-- <label class="form-label" for="contact-name">Your name</label> --}}
                    {{-- </div> --}}
                    
                    <button class="button button-primary" type="submit">send</button> --}}
                </form>
                </div>
            </div>
        </div>
    </section>

@endsection