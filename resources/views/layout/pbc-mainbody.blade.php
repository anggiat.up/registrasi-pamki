<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
  <head>
    <!-- Site Title-->
    <title>{{ $nama_halaman }}</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Libre+Franklin:200,300,500,600,300italic">
    <link rel="stylesheet" href="{{ asset('old/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('old/css/style.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>.ie-panel{display: none;background: #212121;padding: 10px 0;box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3);clear: both;text-align:center;position: relative;z-index: 1;} html.ie-10 .ie-panel, html.lt-ie-10 .ie-panel {display: block;}</style>
  </head>
  <body>
    <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <div id="page-loader">
      <div class="cssload-container">
        <div class="cssload-speeding-wheel"></div>
      </div>
    </div>
    <!-- Page-->
    <div class="page">
      <!-- Page Header-->
      <header class="page-header">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar novi-bg novi-bg-img" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-sm-device-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fixed" data-xl-device-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-stick-up-clone="false" data-sm-stick-up="true" data-md-stick-up="true" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true" data-lg-stick-up-offset="69px" data-xl-stick-up-offset="1px" data-xxl-stick-up-offset="1px">
            <div class="rd-navbar-inner">
              <!-- RD Navbar Panel-->
              <div class="rd-navbar-panel">
                <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                <!-- RD Navbar Brand-->
                <div class="rd-navbar-brand">
                  <!-- Brand--><a class="brand" href="{{ url('') }}"><img class="brand-logo-dark" src="images/logo-default-347x65.png" alt="" width="173" height="32"/><img class="brand-logo-light" src="images/logo-inverse-347x65.png" alt="" width="173" height="32"/></a>
                </div>
              </div>
              <!-- RD Navbar Nav-->
              <div class="rd-navbar-nav-wrap">
                
                <ul class="rd-navbar-nav">
                  <li class="rd-nav-item">
                    <a class="" href="{{ url('') }}">Home</a>
                  </li>
                  <li class="rd-nav-item">
                    <a class="" href="{{ url('kata-sambutan') }}">Kata Sambutan</a>
                  </li>
                  <li class="rd-nav-item">
                    <a class="" href="{{ url('panitia') }}">Panitia</a>
                  </li>
                  <li class="rd-nav-item">
                    <a class="" href="{{ url('materi-acara') }}">Materi Acara</a>
                  </li>
                  <li class="rd-nav-item">
                    <a class="" href="{{ url('pendaftaran') }}">Pendaftaran</a>
                  </li>
                  <li class="rd-nav-item">
                    <a class="" href="">Makalah & Poster</a>
                  </li>
                  <li class="rd-nav-item">
                    <a class="font-weigh-bold" href="{{ url('kontak-kami') }}">Kontak Kami</a>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        </div>
      </header>

      @yield('container')

      
      <!-- Page Footer-->
      <footer>
        <div class="pre-footer-corporate novi-bg novi-bg-img">
          <div class="container">
            <div class="row justify-content-sm-center justify-content-lg-start row-30 row-md-60">
              <div class="col-sm-10 col-md-6 col-lg-10 col-xl-3">
                <h6>Tentang</h6>
                <p>PIT PAMKI 2024</p>
              </div>
              <div class="col-sm-10 col-md-6 col-lg-3 col-xl-3">
                <h6>Navigation</h6>
                <ul class="list-xxs">
                  <li><a href="{{ url('') }}">Kata Sambutan</a></li>
                  <li><a href="{{ url('') }}">Panitia</a></li>
                  <li><a href="{{ url('') }}">Materi Acara</a></li>
                  <li><a href="{{ url('') }}">Pendaftaran</a></li>
                  <li><a href="{{ url('') }}">Makalah & Poster</a></li>
                </ul>
              </div>
              <div class="col-sm-10 col-md-6 col-lg-4 col-xl-3">
                <h6>Kontak</h6>
                <ul class="list-xs">
                  <li>
                    <dl class="list-terms-minimal">
                      <dt>Phones</dt>
                      <dd>
                        <ul class="list-semicolon">
                          <li><a href="tel:#">(800) 123-0045</a></li>
                          <li><a href="tel:#">(800) 123-0045</a></li>
                        </ul>
                      </dd>
                    </dl>
                  </li>
                  <li>
                    <dl class="list-terms-minimal">
                      <dt>E-mail</dt>
                      <dd><a href="mailto:#">info@demolink.org</a></dd>
                    </dl>
                  </li>
                  <li>
                    <dl class="list-terms-minimal">
                      <dt>We are open</dt>
                      <dd>Mn-Fr: 10 am-8 pm</dd>
                    </dl>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>        
      </footer>
    </div>
    <!-- Global Mailform Output-->
    <div class="snackbars" id="form-output-global"></div>
    <!-- Javascript-->
    <script src="{{ asset('old/js/core.min.js') }}"></script>
    <script src="{{ asset('old/js/script.js') }}"></script>
  </body>
</html>