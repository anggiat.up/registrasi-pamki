<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]>
<!--><html class="no-js" lang="en"><!--<![endif]-->
<head>

	<!-- Basic Page Needs
	================================================== -->
	<meta charset="utf-8">
	<title>{{ $nama_halaman }}</title>
	<meta name="description"  content="" />
	<meta name="author" content="">
	<meta name="keywords"  content="" />
	<meta property="og:title" content="" />
	<meta property="og:type" content="" />
	<meta property="og:url" content="" />
	<meta property="og:image" content="" />
	<meta property="og:image:width" content="470" />
	<meta property="og:image:height" content="246" />
	<meta property="og:site_name" content="" />
	<meta property="og:description" content="" />
	<meta name="twitter:card" content="" />
	<meta name="twitter:site" content="" />
	<meta name="twitter:domain" content="" />
	<meta name="twitter:title" content="" />
	<meta name="twitter:description" content="" />
	<meta name="twitter:image" content="" />

	<!-- Mobile Specific Metas
	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="theme-color" content="#212121"/>
    <meta name="msapplication-navbutton-color" content="#212121"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="#212121"/>
	
	<!-- CSS
	================================================== -->
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/animsition.min.css" />
	<link rel="stylesheet" href="css/unicons.css"/>
	<link rel="stylesheet" href="css/lighbox.min.css"/>
	<link rel="stylesheet" href="css/tooltip.min.css"/>
	<link rel="stylesheet" href="css/swiper.min.css"/>
	<link rel="stylesheet" href="css/style.css"/>



	<!-- Sweet Alert-->
	<link href="{{ asset('sistems/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />

	{{-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" /> --}}
			
	<!-- Favicons
	================================================== -->
	<link rel="icon" type="image/png" href="favicon.png">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png">
	
	
</head>
<body>	
	<div id="loading-ajax">Loading...</div>      
	<div id="base-url" data-baseurl="{{ url('') }}" ></div>
	<style>
		#loading-ajax{
			width: 100%;
			height: 100%;
			position: fixed;
			text-indent: 100%;
			margin-top: -20px;
			background: #e0e0e0 url("{{ asset('img/loading.gif') }}") no-repeat center;
			z-index: 1321050 ;
			opacity: 0.6;
			background-size: 8%;
		}
		.daterangepicker {
			z-index: 1221050 !important;
		}
	</style>
	<div class="animsition">
	
        <!-- Navigation
		================================================== -->
			
        <div class="navigation-wrap cbp-af-header header-dark">
			<div class="section d-none d-xl-block nav-border-bottom-light">
				<div class="section px-lg-2">
					<div class="container-fluid">
						<div class="row">
							<span></span>
						</div>
					</div>
				</div>
			</div>
            <div class="padding-on-scroll">
				<div class="section full-width-mega-menu px-lg-2">
					<div class="container-fluid">
						<div class="row">
							<div class="col-12">
								<nav class="navbar navbar-expand-xl navbar-light">

									<a class="navbar-brand animsition-link" href="{{ url('') }}"><img src="img/Logo pamki web.png"
											alt="logo"></a>

									<button class="navbar-toggler" type="button" data-toggle="collapse"
										data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
										aria-expanded="false" aria-label="Toggle navigation">
										<span class="navbar-toggler-icon"></span>
									</button>

									<div class="collapse navbar-collapse" id="navbarSupportedContent">
										<ul class="navbar-nav ml-auto pt-4 pt-xl-0 mr-xl-4">
											<li class="nav-item dropdown">
												<a class="nav-link active"  href="{{ url('') }}">Home</a>												
											</li>
                                            <li class="nav-item dropdown">
												<a class="nav-link "  href="{{ url('home#kata-sambutan') }}" data-gal='m_PageScroll2id' data-ps2id-offset="70">Kata Sambutan</a>												
											</li>
                                            <li class="nav-item dropdown">
												<a class="nav-link "  href="{{ url('panitia') }}">Panitia</a>												
											</li>
                                            <li class="nav-item dropdown">
												<a class="nav-link "  href="{{ url('materi-acara') }}">Materi Acara</a>												
											</li>
                                            <li class="nav-item dropdown">
												<a class="nav-link "  href="{{ url('sertifikat') }}">Sertifikat</a>												
												{{-- <a class="nav-link " data-toggle="modal" data-target="#modal-3"  href="javascript:void(0);">Pendaftaran</a>												 --}}
											</li>
                                            {{-- <li class="nav-item dropdown">
												<a class="nav-link " href="{{ url('home#call-for-abstract') }}" data-gal='m_PageScroll2id' data-ps2id-offset="70">Call for Abstract</a>												
											</li> --}}
                                            {{-- <li class="nav-item dropdown">
												<a class="nav-link "  href="{{ url('home#akomodasi-hotel') }}" data-gal='m_PageScroll2id' data-ps2id-offset="70" >Reservasi Hotel</a>
											</li> --}}
											<li class="nav-item dropdown">
												<a class="nav-link "  href="{{ asset('documents/ABSTRACTS BOOK OF PAMKI 2024.pdf') }}" target="_blank" download data-gal='m_PageScroll2id' data-ps2id-offset="70" >E-Book</a>
											</li>
											<li class="nav-item dropdown">
												<a class="nav-link "  href="{{ url('kontak') }}">Kontak Kami</a>												
											</li>
											
										</ul>
										
										<div class="pb-3 pb-xl-0"></div>
									</div>

								</nav>
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
		
		@yield('container')
		<style>
			.swal-overlay {
				z-index: 1321050 !important;
			}
			.swal-modal {
				z-index: 1070 !important;
			}
			.swal2-container  {
				z-index: 1321050 !important; /* Sesuaikan dengan nilai z-index yang Anda butuhkan */
			}
			.swal2-popup {
				font-size: 0.9rem !important;
			}
		</style>
		{{-- Modal Daftar Abstract --}}
        <div class="modal fade modal-large" id="modal-3" tabindex="-1" role="dialog" aria-labelledby="modal-3" aria-hidden="true" >
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body z-bigger">
                        <div class="container-fluid">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="uil uil-multiply"></i>
                            </button>
							<div class="alert alert-primary" role="alert">
								<h4 class="alert-heading text-white">Registrasi has Closed!</h4>
								<p class="text-white">Sorry, registration for the abstract competition has closed.</p>
								<hr>
							</div>
							  <!--<form action="#" id="form-abstract">-->
							  <!--	<div class="row justify-content-center">-->
							  <!--		<div class="col-12 text-center">-->
							  <!--			<h5 class="mb-2">Registrasi Call for Abstract</h5>-->
							  <!--			-->
							  <!--		</div>-->
  
							  <!--		<div class="col-lg-12 mt-3 "> -->
							  <!--			<div class="form-group">-->
							  <!--				<label for="" class="text-dark font-weight-bold">Nomor Registrasi Peserta </label>-->
							  <!--				<input type="hidden" name="_token" value="CXFtTPgOqodeiqkvwfmuSKrOfiyGeVRAgLt7c4ZE" autocomplete="off">-->
							  <!--				<input type="text" class="form-style text-uppercase" id="noregpeserta" name="noregpeserta" placeholder="PMKI-0023..." autocomplete="off" required>-->
							  <!--				<small class="text-danger">*Nomor registrasi didapatkan setelah verifikasi pembayaran peserta dikonfirmasi melalui email</small>-->
							  <!--			</div>   				-->
							  <!--		</div>	-->
  
							  <!--		<div class="col-lg-12 mt-3 "> -->
							  <!--			<div class="form-group">-->
							  <!--				<label for="" class="text-dark font-weight-bold">Nama Lengkap </label>-->
							  <!--				<input type="text" class="form-style bg-gray" id="namalengkap_abs" readonly  autocomplete="off">-->
							  <!--			</div>   				-->
							  <!--		</div>	-->
  
							  <!--		<div class="col-md-6 mt-3"> -->
							  <!--			<div class="form-group">-->
							  <!--				<label for="" class="text-dark font-weight-bold">Email </label>-->
							  <!--				<input type="text" class="form-style bg-gray" id="email_abs" readonly>-->
							  <!--			</div>             -->
							  <!--		</div>-->
							  <!--		<div class="col-md-6 mt-3 "> -->
							  <!--			<div class="form-group">-->
							  <!--				<label for="" class="text-dark font-weight-bold">No. HP WA  </label>-->
							  <!--				<input type="text" class="form-style bg-gray" id="nohp_abs" readonly>-->
							  <!--			</div>             -->
							  <!--		</div>-->
  
							  <!--		<div class="col-lg-12 mt-3 "> -->
							  <!--			<div class="form-group choose-file colored-bg">-->
							  <!--				<input type="file" class="form-file" name="file-abs" id="regfile-abs" accept="application/msword,.doc,.docx" required>-->
							  <!--				<label>Pilih File Abstract atau Tarik File anda disini</label>-->
							  <!--				<small class="text-danger">*Format file .DOCX</small> <br>-->
							  <!--				<small class="text-danger">*Besar File Maksimal 5Mb</small>-->
							  <!--			</div>             -->
							  <!--		</div>-->
  
									  
  
									  
																			  
							  <!--		<div class="col-12 text-center mt-3 pt-3">-->
							  <!--			<button data-dismiss="modal" type="button" class="btn btn-dark mx-1">Tutup</button> -->
							  <!--			<button type="submit" class="btn bg-pamki1 text-white mx-1">Daftar Abstract</button> -->
							  <!--		</div>-->
							  <!--	</div>-->
							  <!--</form>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

		{{-- Modal Reservasi --}}
        <div class="modal fade modal-large" id="modal-reservasi" tabindex="-1" role="dialog" aria-labelledby="modal-reservasi" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body z-bigger">
                        <div class="container-fluid">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <i class="uil uil-multiply"></i>
                            </button>
							<form action="#" id="form-reservasi-hotel">
								<div class="row justify-content-start">
									<div class="col-12 text-center">
										<h5 class="mb-2">Reservasi Hotel</h5>
										{{-- <p class="mb-3 pb-3">Silahkan <span class="border-bottom-primary">lengkapi</span> data diri anda.</p> --}}
									</div>

									<div class="col-lg-12 mt-3">
										<p>											
											<ol>
												<li><h6>Lengkapi Formulir Reservasi</h6>
													<ul class="text-dark">
														<li>Isilah formulir reservasi dengan data peserta yang valid.</li>
														<li>Harap menggunakan nomor handphone WA aktif untuk menerima notifikasi pembayaran dan informasi terkait reservasi.</li>
													</ul>
												</li>
												<li><h6>Tata Cara Pembayaran</h6>
													<ul class="text-dark">
														<li>Pembayaran dilakukan melalui : 
															<table width="100%" class="ml-3">
																<tr>
																	<th width="20%" >Nama BANK</th>
																	<th width="5%">:</th>
																	<td>MANDIRI</td>
																</tr>
																<tr>
																	<th>Nama Rekening</th>
																	<th >:</th>
																	<td>PAMKI CABANG RIAU</td>
																</tr>
																<tr>
																	<th>Nomor Rekening</th>
																	<th >:</th>
																	<td>1220093714817</td>
																</tr>
																<tr>
																	<th>PJ Hotel Zizi</th>
																	<th >:</th>
																	<td><a href="https://wa.me/6281365729628" target="_blank" class="text-dark">081365729628</a></td>
																</tr>
															</table>															
														</li>
														<li>Pada bagian berita acara transfer, pastikan Anda mengisi berita acara dengan kode <b>RSH#NAMALENGKAP</b>.</li>
														<li>Proses konfirmasi pembayaran akan memakan waktu hingga 1x24 jam.</li>
														<li>Pembayaran yang telah dilakukan <b class="text-danger">TIDAK DAPAT DIBATALKAN</b> atau <b class="text-danger">DIREFUND</b>.</li>
													</ul>
												</li>												
											</ol>         
										</p>
										<hr>
									</div>
									@csrf
									<div class="col-lg-12 mt-3 "> 
										<div class="form-group">
											<label for="nama-hotel" class="text-dark font-weight-bold">Nama Hotel </label>
											<input type="hidden" id="kodeuniqres" name="kodeuniqres">
											<input type="text" id="nama-hotel" name="namahotel" class="form-style bg-gray" readonly  autocomplete="off">
										</div>   				
									</div>

									<div class="col-lg-12 mt-3 "> 
										<div class="form-group">
											<label for="nama-hotel" class="text-dark font-weight-bold">Tanggal CheckIn - CheckOut </label>                                        
											<input type="text" class="form-style text-center" name="tanggalcico" id="tanggalcico"  />
										</div>   				
									</div>
									
									
																	
									<div class="col-lg-12 mt-3 bg-dangers">
										<div class="form-group">
											<label for="jenis-kamar" class="text-dark font-weight-bold">Jenis Kamar</label>
											<div class="section">
												<select class="wide select-style text-dark " id="jeniskamar" name="jeniskamar" required>
													{{-- <option value="" hidden selected disabled>-- Jenis Kamar --</option> --}}
												</select>
											</div> 	
										</div>                                                              
									</div>

									<div class="col-lg-12 mt-3 "> 
										<div class="form-group">
											<label for="" class="text-dark font-weight-bold">Nama Lengkap </label>
											<input type="text" class="form-style " placeholder="" name="nama_lengkap" autocomplete="off" required>
											<small class="text-danger">*Tanpa gelar</small>
										</div>   				
									</div>

									<div class="col-md-6 mt-3 "> 
										<div class="form-group">
											<label for="" class="text-dark font-weight-bold">No. HP WA  </label>
											<input type="number" class="form-style" name="nohp" autocomplete="off" required placeholder="0812345xxxx"> 
										</div>             
									</div>

									<div class="col-md-6 mt-3 "> 
										<div class="form-group">
											<label for="" class="text-dark font-weight-bold">Biaya  </label>
											<input type="text" class="form-style bg-gray text-right" id="jumlahhari-reservasi" autocomplete="off" readonly>
										</div>             
									</div>

									<div class="col-md-12 mt-3 "> 
										<div class="form-group">
											<label for="" class="text-dark font-weight-bold">Total Biaya  </label>
											<input type="text" class="form-style bg-gray text-right" id="total-biaya" autocomplete="off" readonly>
											<small class="text-danger">*Silahkan melakukan pembayaran senilai dengan total biaya, dan 3 angka dibelakang kode uniq yang tertera</small>
										</div>             
									</div>

									<div class="col-lg-12 mt-3 "> 
										<div class="form-group choose-file colored-bg">
											<input type="file" class="form-file" name="filestruct" id="filestruct" accept="image/*" required>
											<label>Pilih File Struck atau Tarik File anda disini</label>
											<small class="text-danger">*Format file .JPG, .JPEG, .PNG</small>
										</div>             
									</div>
									
									<div class="col-12 text-center mt-3 pt-3">
										<button data-dismiss="modal" type="button" class="btn btn-dark mx-1">Tutup</button> 
										<button type="submit" class="btn bg-pamki1 text-white mx-1">Daftar Reservasi</button> 
									</div>
								</div>
							</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

		{{-- Modal Syarat & Ketentuan Abstract --}}
		<div class="modal fade modal-large" id="modal-callabstract" tabindex="-1" role="dialog" aria-labelledby="modal-callabstract" aria-hidden="true">
			<div class="modal-dialog" role="document" >
				<div class="modal-content">
					<div class="modal-body z-bigger">
						<div class="container-fluid">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<i class="uil uil-multiply"></i>
							</button>
							<div class="row justify-content-center">
								<div class="col-12 text-center">
									<h5 class="mb-2">Syarat & Ketentuan</h5>
									{{-- <p class="mb-3 pb-3">Use <span class="border-bottom-primary">modal</span> to add dialogs to your site <span class="border-bottom-primary">for lightboxes</span>, <span class="border-bottom-primary">user notifications</span>, or <span class="border-bottom-primary">completely custom content</span>.</p> --}}
								</div>
								<p class="text-left mt-3 font-weight-bold">
									<a target="_blank" href="{{ url('download/documents/Abstract PIT Guidelines .docx') }}"><u>Download File Abstract</u></a>
								</p>
								<iframe id="pdfViewer" src="{{ url('documents/Call for Abstract.pdf') }}" width="100%" height="600px"></iframe>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		{{-- Modal Pengumuman --}}
		<div class="modal fade modal-large" id="modal-pengumuman" tabindex="-1" role="dialog" aria-labelledby="modal-pengumuman" aria-hidden="true">

			<div class="modal-dialog" role="document" >
				<div class="modal-content">
					<div class="modal-body z-bigger">
						<div class="container-fluid">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<i class="uil uil-multiply"></i>
							</button>
							<div class="row justify-content-center">								
								<iframe id="pdfViewer" src="{{ url('documents/PAMKI 2024 Final Announcement (V1).pdf') }}" width="100%" height="900px"></iframe>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>

		{{-- Modal Narapati --}}
		<div class="modal fade modal-large" id="modal-narapati" tabindex="-1" role="dialog" aria-labelledby="modal-narapati" aria-hidden="true">
			<div class="modal-dialog" role="document" >
				<div class="modal-content" style="background-color:#fff0;box-shadow: 0 0 40px 0 rgb(0 0 0 / 0%);">
					<div class="modal-body z-bigger">
						<div class="container-fluid">
							
							<div class="row justify-content-center">
								
								
									{{-- <img class="border-4" src="img/img-1.jpg" alt=""> 													 --}}
									<img class="border-4" src="{{ asset('img/narapati.png') }}" alt="" style="width: 100%;">
								{{-- <iframe id="pdfViewer" src="{{ url('documents/Call for Abstract.pdf') }}" width="100%" height="600px"></iframe> --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		


		<!-- Footer
		================================================== -->

		<div class="section pb-4 px-lg-2" id="footer">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<div class="section over-hide border-4 padding-top-80 pb-4 section-shadow-blue bg-white section-background-3 px-3 px-xl-5">
							
							<div class="row text-center text-md-left">								
								<div class="col-md text-center">
									<p class="mb-0 size-14 color-dark mt-1 font-weight-500">© 2024 PIT PAMKI by <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-narapati" >NARAPATI PROJECT</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


	    <div class="progress-wrap">
			<svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
				<path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98"/>
			</svg>
		</div>
		
	</div>



	
	<!-- JAVASCRIPT
    ================================================== -->
	<script src="js/jquery.min.js"></script>
	<!-- Sweet Alerts js -->
	<script src="{{ asset('sistems/libs/sweetalert2/sweetalert2.min.js') }}"></script>

	<!-- Sweet alert init js-->
	<script src="{{ asset('sistems/js/pages/sweet-alerts.init.js') }}"></script>
	
	<script src="js/popper.min.js"></script> 
	<script src="js/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script> 
	<script src="js/custom.js"></script>  

	{{-- <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script> --}}
	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

	{{-- <script src="js/public-custom.js"></script>   --}}
	<script src="{{ asset('js/public-custom.js?v='.filemtime(public_path('js/public-custom.js'))) }}"></script>  

	
    <script>
		$(function() {
			$('input[name="tanggalcico"]').daterangepicker({
				opens: 'left'
			}, function(start, end, label) {
				
			});
		});


		
	</script> 
<!-- End Document
================================================== -->
</body>
</html>