<?php

namespace Database\Seeders;

use App\Models\DataUsers;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class DataUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Tambahkan data ke dalam tabel data_users
        DataUsers::create([
            'pmkid'        => Uuid::uuid4(),
            'email'        => 'admin@example.com',
            'password'     => bcrypt('pw123123'),
            'nama_lengkap' => 'Admin',
            'no_hp'        => '08123456789',
            'level_akun'   => 'admin',
            'status_akun'  => 'aktif',
        ]);
        
    }
}
