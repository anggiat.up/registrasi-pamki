<?php

namespace Database\Seeders;

use App\Models\DataPaket;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class DataPaketMid extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $dataPaket = [
                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Clinical Microbiology Resident',
                            'nama_paket'    => 'Symposium',
                            'harga_paket'   => 850000,
                            'status'        => 'Inactive',
                        ],
                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Clinical Microbiology Resident',
                            'nama_paket'    => 'Workshop I',
                            'harga_paket'   => 1000000,
                            'status'        => 'Inactive',
                        ],
                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Clinical Microbiology Resident',
                            'nama_paket'    => 'Workshop I + Symposium',
                            'harga_paket'   => 1665000,
                            'status'        => 'Inactive',
                        ],
                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Clinical Microbiology Resident',
                            'nama_paket'    => 'Workshop II',
                            'harga_paket'   => 1000000,
                            'status'        => 'Inactive',
                        ],
                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Clinical Microbiology Resident',
                            'nama_paket'    => 'Workshop II + Symposium',
                            'harga_paket'   => 1665000,
                            'status'        => 'Inactive',
                        ],



                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Other',
                            'nama_paket'    => 'Symposium',
                            'harga_paket'   => 1000000,
                            'status'        => 'Inactive',
                        ],
                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Other',
                            'nama_paket'    => 'Workshop I',
                            'harga_paket'   => 1000000,
                            'status'        => 'Inactive',
                        ],
                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Other',
                            'nama_paket'    => 'Workshop I + Symposium',
                            'harga_paket'   => 1800000,
                            'status'        => 'Inactive',
                        ],
                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Other',
                            'nama_paket'    => 'Workshop II',
                            'harga_paket'   => 1000000,
                            'status'        => 'Inactive',
                        ],
                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Other',
                            'nama_paket'    => 'Workshop II + Symposium',
                            'harga_paket'   => 1800000,
                            'status'        => 'Inactive',
                        ],



                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Specialist Non-PAMKI Member',
                            'nama_paket'    => 'Symposium',
                            'harga_paket'   => 1800000,
                            'status'        => 'Inactive',
                        ],
                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Specialist Non-PAMKI Member',
                            'nama_paket'    => 'Workshop I',
                            'harga_paket'   => 1000000,
                            'status'        => 'Inactive',
                        ],
                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Specialist Non-PAMKI Member',
                            'nama_paket'    => 'Workshop I + Symposium',
                            'harga_paket'   => 2520000,
                            'status'        => 'Inactive',
                        ],
                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Specialist Non-PAMKI Member',
                            'nama_paket'    => 'Workshop II',
                            'harga_paket'   => 1000000,
                            'status'        => 'Inactive',
                        ],
                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Specialist Non-PAMKI Member',
                            'nama_paket'    => 'Workshop II + Symposium',
                            'harga_paket'   => 2520000,
                            'status'        => 'Inactive',
                        ],



                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Specialist PAMKI Member',
                            'nama_paket'    => 'Symposium',
                            'harga_paket'   => 1200000,
                            'status'        => 'Inactive',
                        ],
                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Specialist PAMKI Member',
                            'nama_paket'    => 'Workshop I',
                            'harga_paket'   => 1000000,
                            'status'        => 'Inactive',
                        ],
                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Specialist PAMKI Member',
                            'nama_paket'    => 'Workshop I + Symposium',
                            'harga_paket'   => 1980000,
                            'status'        => 'Inactive',
                        ],
                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Specialist PAMKI Member',
                            'nama_paket'    => 'Workshop II',
                            'harga_paket'   => 1000000,
                            'status'        => 'Inactive',
                        ],
                        [
                            'asal_peserta'  => 'national',
                            'jenis_peserta' => 'Specialist PAMKI Member',
                            'nama_paket'    => 'Workshop II + Symposium',
                            'harga_paket'   => 1980000,
                            'status'        => 'Inactive',
                        ],




                        [
                            'asal_peserta'  => 'international',
                            'jenis_peserta' => 'Consultant',
                            'nama_paket'    => 'Symposium',
                            'harga_paket'   => 3245000,
                            'status'        => 'Inactive',
                        ],
                        [
                            'asal_peserta'  => 'international',
                            'jenis_peserta' => 'Trainee',
                            'nama_paket'    => 'Symposium',
                            'harga_paket'   => 1379000,
                            'status'        => 'Inactive',
                        ]
            ];

        foreach ($dataPaket as $paket) {
            DataPaket::create([
                'pketid'        => Uuid::uuid4(),
                'asal_peserta'  => $paket['asal_peserta'],
                'jenis_peserta' => $paket['jenis_peserta'],
                'nama_paket'    => $paket['nama_paket'],
                'harga_paket'   => $paket['harga_paket'],
                'waktu'         => 'Mid Registration',
                'status'        => $paket['status'],
                'created_at'    => now(),
                'updated_at'    => now(),
            ]);
        }        
    }
}
