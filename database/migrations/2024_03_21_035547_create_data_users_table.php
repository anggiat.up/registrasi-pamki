<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    
    public function up(): void
    {
        Schema::create('data_users', function (Blueprint $table) {
            $table->uuid('pmkid', 36)->primary();
            $table->string('email')->unique();
            $table->string('password', 100);
            $table->string('nama_lengkap', 150);
            $table->char('no_hp', 20);
            $table->enum('level_akun', ['admin','adm-pendaftaran','adm-abstract','adm-hotel'])->default('adm-pendaftaran');
            $table->enum('status_akun', ['aktif', 'tidak-aktif'])->default('aktif');
            $table->integer('terakhir_login')->nullable();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('data_users');
    }
};
