<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data_paket_hotel', function (Blueprint $table) {
            $table->uuid('pakthtlid', 36)->primary();
            $table->char('hotel_id', 36);
            $table->string('nama_paket');
            $table->unsignedInteger('harga_paket');
            $table->enum('status_paket', ['aktif', 'tidak-aktif'])->default('aktif');
            $table->timestamps();

            $table->foreign('hotel_id')->references('hotelid')->on('data_hotel')->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_paket_hotel');
    }
};
