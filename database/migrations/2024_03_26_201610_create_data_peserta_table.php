<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {

        Schema::create('data_peserta', function (Blueprint $table) {            
            $table->char('pst_id', 12)->primary();
            $table->string('kode_invoice');
            $table->string('jenis_peserta');
            $table->string('paket_id');
            $table->string('nama_paket');
            $table->integer('harga_paket');
            $table->string('gelar_depan')->nullable();
            $table->string('gelar_belakang')->nullable();
            $table->string('nama_lengkap');
            $table->string('email');
            $table->string('nohp');
            $table->string('instansi_asal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_peserta');
    }
};
