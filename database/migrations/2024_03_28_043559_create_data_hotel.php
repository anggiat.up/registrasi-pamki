<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data_hotel', function (Blueprint $table) 
        {
            $table->uuid('hotelid', 36)->primary();
            $table->string('nama_hotel', 50);
            $table->string('gambar_hotel', 30)->nullable();
            $table->text('alamat');
            $table->integer('rate_bintang')->nullable();
            $table->enum('status', ['aktif', 'tidak-aktif'])->default('aktif');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_hotel');
    }
};
