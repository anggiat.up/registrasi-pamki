<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data_abstract', function (Blueprint $table) {
            $table->uuid('absid')->primary();
            $table->char('pst_id', 12)->unique();
            $table->string('file_abs')->nullable();
            $table->enum('status_pembayaran', ['menunggu', 'diterima', 'ditolak'])->default('menunggu');
            $table->timestamps();

            $table->foreign('pst_id')->references('pst_id')->on('data_peserta')->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_abstract');
    }
};
