<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data_reservasi', function (Blueprint $table) {
            $table->uuid('resid', 36)->primary();
            $table->char('paket_hotelid', 36);
            $table->date('tanggal_checkin');
            $table->date('tanggal_checkout');
            $table->string('nama_lengkap');
            $table->string('no_hp');
            $table->string('struct_pembayaran');
            $table->integer('total_biaya');
            $table->enum('status_res', ['menunggu', 'selesai', 'batal'])->default('menunggu');
            $table->char('admin_verifikasi', 36)->nullable();
            $table->timestamps();
            
            
            $table->foreign('admin_verifikasi')->references('pmkid')->on('data_users')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('paket_hotelid')->references('pakthtlid')->on('data_paket_hotel')->onUpdate('cascade')->onDelete('restrict');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_reservasi');
    }
};
