<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use App\Models\DataPeserta;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotificationQRPresensi;

class NotifQRCode implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        try {
            // Logika untuk job        
            $dataPeserta = DataPeserta::whereNull('notifikasi_qrpresensi')->get()->toArray();

            $jumlah_peserta = count($dataPeserta);
            $berhasil       = 0;
            $gagal          = 0;
            $limit          = 5; // Batas jumlah loop yang diinginkan
            $counter        = 0; // Variabel penghitung

    
            foreach ($dataPeserta as $peserta) 
            {
                
            
                $data_email  = [
                    'nama_lengkap' => $peserta['gelar_depan'] . ' ' . $peserta['nama_lengkap'] . ' ' . $peserta['gelar_belakang'],
                    'email'        => $peserta['email'],
                    'pst_id'       => $peserta['pst_id'],
                ];
                try {
                    Mail::to($peserta['email'])->send(new NotificationQRPresensi($data_email));
                    DataPeserta::where(['pst_id' => $peserta['pst_id']])->update(['notifikasi_qrpresensi' => 'berhasil', 'updated_at' => now()]);
                    $berhasil += 1;

                } catch (\Throwable $th) {
                    DataPeserta::where(['pst_id' => $peserta['pst_id']])->update(['notifikasi_qrpresensi' => 'gagal', 'updated_at' => now()]);                    
                    $gagal += 1;
                }

            }

            Log::info("Jumlah Notifikasi yang berhasil dikirimkan: $berhasil");
            Log::info("Jumlah Notifikasi yang gagal dikirimkan: $gagal");

        } catch (\Exception $e) {
            // Menangani error
            Log::error('Job failed: ' . $e->getMessage());
            // Optionally, you can retry the job or take other actions
        }
    }
}
