<?php

namespace App\Jobs;

use App\Models\DataPeserta;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Facades\Log;

class GenerateQR implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        try {
            // Logika untuk job        
            // Ambil data peserta
            $data           = DataPeserta::all()->toArray();
            $jumlah_peserta = count($data);
            $berhasil       = 0;
            $gagal          = 0;

            // Loop untuk memproses data peserta
            for ($i = 0; $i < $jumlah_peserta; $i++) 
            { 
                $valueqrCode = strtoupper($data[$i]['pst_id']);
                $filename    = $valueqrCode . '.png'; // Nama file QR Code
                $path        = 'qrcode-peserta/' . $filename; // Path untuk menyimpan file

                // Cek jika file QR Code belum ada
                if (!file_exists(public_path($path))) 
                {
                    try {
                        // Generate QR Code
                        QrCode::size(600)
                            ->format('png')
                            ->errorCorrection('H')
                            ->generate($valueqrCode, public_path($path));

                        // Cek jika file berhasil dibuat
                        if (file_exists(public_path($path))) {
                            $berhasil += 1;
                        } else {
                            $gagal += 1;
                        }
                    } catch (\Exception $e) {
                        // Tangani kesalahan jika terjadi
                        $gagal += 1;
                    }
                }
            }

            // Tampilkan hasil ke log
            Log::info("Jumlah QR Code yang berhasil digenerate: $berhasil");
            Log::info("Jumlah QR Code yang gagal digenerate: $gagal");
        } catch (\Exception $e) {
            // Menangani error
            Log::error('Job failed: ' . $e->getMessage());
            // Optionally, you can retry the job or take other actions
        }
    }    
}
