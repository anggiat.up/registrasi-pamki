<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataHotel extends Model
{
    use HasFactory;

    protected $table      = 'data_hotel'; // Nama tabel yang digunakan oleh model
    protected $primaryKey = 'hotel_id'; // Nama kolom primary key
    protected $keyType    = 'string'; // Tipe data primary key
    public $incrementing  = false; // Set false jika menggunakan UUID

    protected $fillable = [
        'hotel_id',
        'nama_hotel',
        'gambar_hotel',
        'alamat',
        'rate_bintang',
        'status',
    ];

    protected $dates = ['created_at', 'updated_at'];
}
