<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataPeserta extends Model
{
    use HasFactory;

    protected $table      = 'data_peserta';
    protected $primaryKey = 'pst_id';
    public $incrementing  = false;
    protected $keyType    = 'string';

    protected $fillable = [
        'pst_id',
        'kode_invoice',
        'jenis_peserta',
        'paket_id',
        'nama_paket',
        'harga_paket',
        'gelar_depan',
        'gelar_belakang',
        'nama_lengkap',
        'email',
        'nohp',
        'instansi_asal',
        'notifikasi_qrpresensi',
        'status_presensi',
        'waktu_presensi',
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function pendaftaran()
    {
        return $this->belongsTo(DataPendaftaran::class, 'kode_invoice', 'kode_invoice');
    }

    public static function searchPeserta($cari)
    {
        $results = DataPeserta::where('nama_lengkap', 'like', '%' . $cari . '%')
                                ->orWhere('pst_id', 'like', '%' . $cari . '%')
                                ->orWhere('email', 'like', '%' . $cari . '%')
                                ->get()
                                ->toArray();

        return $results;
    }
}
