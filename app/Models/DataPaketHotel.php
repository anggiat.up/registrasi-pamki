<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataPaketHotel extends Model
{
    use HasFactory;

    protected $table      = 'data_paket_hotel'; // Nama tabel yang digunakan oleh model
    protected $primaryKey = 'pakthtl_id'; // Nama kolom primary key
    protected $keyType    = 'string'; // Tipe data primary key
    public $incrementing  = false; // Set false jika menggunakan UUID

    protected $fillable = [
        'pakthtl_id',
        'hotel_id',
        'nama_paket',
        'harga_paket',
        'status_paket',
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function hoteldata()
    {
        return $this->belongsTo(DataHotel::class, 'hotel_id', 'hotelid');
    }
}
