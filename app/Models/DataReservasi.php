<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataReservasi extends Model
{
    use HasFactory;

    protected $table      = 'data_reservasi'; // Nama tabel yang digunakan oleh model
    protected $primaryKey = 'resid'; // Nama kolom primary key
    protected $keyType    = 'string'; // Tipe data primary key
    public $incrementing  = false; // Set false jika menggunakan UUID

    protected $fillable = [
        'resid',
        'tanggal_checkin',
        'tanggal_checkout',
        'paket_hotelid',
        'nama_lengkap',
        'no_hp',
        'total_biaya',
        'struct_pembayaran',
        'admin_verifikasi',
    ];

    protected $dates = ['tanggal_checkin', 'tanggal_checkout', 'created_at', 'updated_at'];

    public function paketdata()
    {
        return $this->belongsTo(DataPaketHotel::class, 'paket_hotelid', 'pakthtlid');
    }
}
