<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataAbstractPeserta extends Model
{
    use HasFactory;

    protected $table      = 'data_abstract'; // Nama tabel yang digunakan oleh model
    protected $primaryKey = 'absid'; // Nama kolom primary key
    protected $keyType    = 'string'; // Tipe data primary key
    public $incrementing  = false; // Set false jika menggunakan UUID

    protected $fillable = [
        'absid',
        'pst_id',
        'file_abs',        
        'status_peserta',
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function peserta()
    {
        return $this->belongsTo(DataPeserta::class, 'pst_id', 'pst_id');
    }
}
