<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AbsensiKehadiran extends Model
{
    use HasFactory;

    protected $table      = 'data_absensi'; // Nama tabel yang digunakan oleh model
    protected $primaryKey = 'absensiid'; // Nama kolom primary key
    protected $keyType    = 'string'; // Tipe data primary key
    public $incrementing  = false; // Set false jika menggunakan UUID

    protected $fillable = [
        'absensiid',
        'peserta_id',
    ];

    protected $dates = ['created_at', 'updated_at'];

    public function peserta()
    {
        return $this->belongsTo(DataPeserta::class, 'peserta_id', 'pst_id');
    }
}
