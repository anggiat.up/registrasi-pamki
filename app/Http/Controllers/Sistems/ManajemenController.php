<?php

namespace App\Http\Controllers\Sistems;

use App\Http\Controllers\Controller;
use App\Jobs\GenerateQR;
use App\Jobs\NotifQRCode;
use App\Mail\NotificationEmail;
use App\Mail\NotificationPendaftaranPeserta;
use App\Mail\NotificationQRPresensi;
use App\Models\DataAbstractPeserta;
use App\Models\DataPaket;
use App\Models\DataPaketHotel;
use App\Models\DataPendaftaran;
use App\Models\DataPeserta;
use App\Models\DataUsers;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\Facades\DataTables;
use Mpdf\Mpdf;

use ZipArchive;
use Intervention\Image\ImageManager;
use Intervention\Image\Drivers\Imagick\Driver;
use Intervention\Image\Typography\FontFactory;

class ManajemenController extends Controller
{
    public function pgeDashboard()
    {
        $jumlah1      = DataPendaftaran::count();
        $jumlah2      = DataPendaftaran::where('status_pembayaran', 'menunggu')->count();
        $jumlah3      = DataPeserta::count();
        $jumlah4      = DataAbstractPeserta::count(); //;
        $jumlah5      = DataPendaftaran::where('status_pembayaran', 'ditolak')->count();

        $sppamki      = DataPendaftaran::whereJsonContains('data_pendaftaran', [['jenis_peserta' => 'Specialist PAMKI Member']])->count();
        $sppamkiverif = DataPendaftaran::where('status_pembayaran', 'terverifikasi')
                        ->whereJsonContains('data_pendaftaran', [['jenis_peserta' => 'Specialist PAMKI Member']])
                        ->count();

        $spnonpamki        = DataPendaftaran::whereJsonContains('data_pendaftaran', [['jenis_peserta' => 'Specialist Non-PAMKI Member']])->count();
        $spnonpamkiverif   = DataPendaftaran::where('status_pembayaran', 'terverifikasi')
                    ->whereJsonContains('data_pendaftaran', [['jenis_peserta' => 'Specialist Non-PAMKI Member']])
                    ->count();

        $other      = DataPendaftaran::whereJsonContains('data_pendaftaran', [['jenis_peserta' => 'Other']])->count();
        $otherverif = DataPendaftaran::where('status_pembayaran', 'terverifikasi')
                        ->whereJsonContains('data_pendaftaran', [['jenis_peserta' => 'Other']])
                        ->count();

        $resident      = DataPendaftaran::whereJsonContains('data_pendaftaran', [['jenis_peserta' => 'Clinical Microbiology Resident']])->count();
        $residentverif = DataPendaftaran::where('status_pembayaran', 'terverifikasi')
                        ->whereJsonContains('data_pendaftaran', [['jenis_peserta' => 'Clinical Microbiology Resident']])
                        ->count();
        
        $consultant      = DataPendaftaran::whereJsonContains('data_pendaftaran', [['jenis_peserta' => 'Consultant']])->count();
        $consultantverif = DataPendaftaran::where('status_pembayaran', 'terverifikasi')
                        ->whereJsonContains('data_pendaftaran', [['jenis_peserta' => 'Consultant']])
                        ->count();
        
        $trainee      = DataPendaftaran::whereJsonContains('data_pendaftaran', [['jenis_peserta' => 'Trainee']])->count();
        $traineeverif = DataPendaftaran::where('status_pembayaran', 'terverifikasi')
                        ->whereJsonContains('data_pendaftaran', [['jenis_peserta' => 'Trainee']])
                        ->count();

        $symposium = DataPeserta::where('nama_paket', 'Symposium')->count();
        $ws1       = DataPeserta::where('nama_paket', 'Workshop I + Symposium ')->count();
        $ws2       = DataPeserta::where('nama_paket', 'Workshop II + Symposium ')->count();
                                
        
        $page = [
            'nama_halaman'    => 'Manajemen',
            'appname'         => config('app.name'),
            'jumlah1'         => $jumlah1,
            'jumlah2'         => $jumlah2,
            'jumlah3'         => $jumlah3,
            'jumlah4'         => $jumlah4,
            'jumlah5'         => $jumlah5,

            'sppamki'         => $sppamki,
            'sppamkiverif'    => $sppamkiverif,
            'spnonpamki'      => $spnonpamki,
            'spnonpamkiverif' => $spnonpamkiverif,
            'other'           => $other,
            'otherverif'      => $otherverif,
            'resident'        => $resident,
            'residentverif'   => $residentverif,
            'consultant'      => $consultant,
            'consultantverif' => $consultantverif,
            'trainee'         => $trainee,
            'traineeverif'    => $traineeverif,

            'symposium'       => $symposium,
            'ws1'             => $ws1,
            'ws2'             => $ws2,

            // 'kode_invoice' => 'IVC123',
            // 'total_biaya'  => 21750000,
            // 'kode_unik'    => 678,
            // 'nama_lengkap' => 'Andre',
            // 'nama_paket'   => 'Workshop + Symposium',

            // 'pst_id' => 'PMKI-0012'
        ];

        // echo auth()->user()->nama_lengkap;
        return view('admin-page/stm-dashboard', $page);
        // return view('layout/mail-notifikasipembayaran', $page);
        // return view('layout/mail-notifikasipendaftaran', $page);
    }

    public function pgePaketKegiatan()
    {
        $page = [
            'nama_halaman' => 'Paket Kegiatan',
            'appname'      => config('app.name')

        ];

        // echo auth()->user()->nama_lengkap;
        return view('admin-page/stm-paketkegiatan', $page);
    }

    public function pgePendaftaranPeserta()
    {
        $page = [
            'nama_halaman' => 'Pendaftaran Peserta',
            'appname'      => config('app.name')

        ];
        // return view('layout/mail-notifikasipembayaran', $page);
        // die;
        
        // echo auth()->user()->nama_lengkap;
        return view('admin-page/stm-datapendaftaran', $page);

    }

    public function pgePesertaTerverifikasi()
    {
        $page = [
            'nama_halaman' => 'Data Peserta',
            'appname'      => config('app.name')

        ];
        // return view('layout/mail-notifikasipembayaran', $page);
        // die;
        
        // echo auth()->user()->nama_lengkap;
        return view('admin-page/stm-datapeserta', $page);

    }
    
    public function pgeDataPresensi()
    {
        $page = [
            'nama_halaman' => 'Data Presensi Peserta',
            'appname'      => config('app.name')

        ];

        return view('admin-page/stm-datapresensi', $page);

    }

    public function previewEmail()
    {
        $page = [
            'nama_halaman' => 'Data Presensi Peserta',
            'appname'      => config('app.name'),
            'nama_lengkap' => 'Lorem Ipsum',
            'pst_id'       => 'pmki-0001',

        ];

        return view('layout/mail-notifikasiqrcode', $page);

    }

    public function testSendQrcode($cari)
    {
        $data            = DataPeserta::where(['email' => $cari])->first();
        $result          = NULL;
        
        if ($data) 
        {    
            $data_email  = [
                            'nama_lengkap' => $data['gelar_depan'] . ' ' . $data['nama_lengkap'] . ' ' . $data['gelar_belakang'],
                            'email'        => $data['email'],
                            'pst_id'       => $data['pst_id'],
                        ];
            try {
                Mail::to($cari)->send(new NotificationQRPresensi($data_email));
                // Mail::to($email_peserta)->send(new SendEmail($data));
                // Mail::to($cari)->send(new NotificationEmail($data_email));
                DataPeserta::where(['pst_id' => $data['pst_id']])->update(['notifikasi_qrpresensi' => 'berhasil', 'updated_at' => now()]);
                return response()->json(['statuslog' => 'success', 'message' => 'Notifikasi QR Code berhasil.', 'title' => 'BERHASIL']);

            } catch (\Throwable $th) {
                return response()->json(['statuslog' => 'error', 'message' =>  'Notifikasi QR Code Gagal jaringan tidak mendukung sehingga pengiriman email gagal silahkan notifikasi ulang kembali atau ganti jaringan anda' , 'title' => 'GAGAL']);
                // return response()->json(['statuslog' => 'success', 'message' =>  $th->getMessage() , 'title' => 'BERHASIL']);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
        }
    }

    public function pgeDataUser()
    {
        $page = [
            'nama_halaman' => 'Data User',
            'appname'      => config('app.name')

        ];
        // return view('layout/mail-notifikasipembayaran', $page);
        // die;
        
        // echo auth()->user()->nama_lengkap;
        return view('admin-page/stm-datauser', $page);

    }

    public function pgePesertaAbstract()
    {
        $page = [
            'nama_halaman' => 'Peserta Abstract',
            'appname'      => config('app.name')

        ];
        // return view('layout/mail-notifikasipembayaran', $page);
        // die;
        
        // echo auth()->user()->nama_lengkap;
        return view('admin-page/stm-pesertaabs', $page);

    }

    public function pgeAbsensiPeserta()
    {
        $page = [
            'nama_halaman' => 'Absensi Peserta',
            'appname'      => config('app.name')

        ];
        return view('admin-page/stm-absensi', $page);

    }

    public function reqCetakLaporan(Request $request)
    {
        
        $jenis       = $request->jenis;
        $dataPeserta = NULL;

        if ($jenis == 'semua') {
            $dataPeserta = DataPeserta::orderBy('pst_id', 'asc')->get();
        } 
        elseif ($jenis == 'symposium') {
            $dataPeserta = DataPeserta::where('nama_paket', 'Symposium')->orderBy('pst_id', 'asc')->get();
        } 
        elseif ($jenis == 'workshop1') {
            $dataPeserta = DataPeserta::where('nama_paket', 'Workshop I + Symposium')->orderBy('pst_id', 'asc')->get();
        } 
        elseif ($jenis == 'workshop2') {
            $dataPeserta = DataPeserta::where('nama_paket', 'Workshop II + Symposium')->orderBy('pst_id', 'asc')->get();
        } 


        $page = [
            'nama_halaman' => 'Cetak Laporan',
            'appname'      => config('app.name'),
            'datapeserta'  => $dataPeserta            
        ];
            
        $mpdf = new Mpdf([
                        'mode'          => 'utf-8',
                        'format'        => 'A4-L', // A4 landscape
                        'margin_left'   => 5, // 2,54 cm = 5 mm
                        'margin_right'  => 5, // 2,54 cm = 5 mm
                        'margin_top'    => 5, // 2,54 cm = 5 mm
                        'margin_bottom' => 5, // 2,54 cm = 5 mm
                        'margin_header' => 0,
                        'margin_footer' =>  0,
        ]);
        
        
        $mpdf->WriteHTML(view('admin-page/stm-cetaklaporan', $page));
        $mpdf->Output();
        // echo auth()->user()->nama_lengkap;
        // return view('admin-pages/stm-cetaklaporan', $page);
    }

    public function reqCetakLaporanPresensi(Request $request)
    {
        
        $jenis         = $request->jenis;
        $dataPeserta   = NULL;
        $colompresensi = null;
        $titlepresensi = null;

        if ($jenis == 'workshop1') {
            $dataPeserta = DataPeserta::where('nama_paket', 'Workshop I + Symposium')->orderBy('pst_id', 'asc')->get();
            $colompresensi = 'status_presensid1';
            $titlepresensi = 'Workshop I';
        } 
        elseif ($jenis == 'workshop2') {
            $dataPeserta = DataPeserta::where('nama_paket', 'Workshop II + Symposium')->orderBy('pst_id', 'asc')->get();
            $colompresensi = 'status_presensid1';
            $titlepresensi = 'Workshop II';
        } 
        elseif ($jenis == 'symposiumd1') {
            $dataPeserta = DataPeserta::where('nama_paket', 'Symposium')->orderBy('pst_id', 'asc')->get();
            $colompresensi = 'status_presensid1';
            $titlepresensi = 'Symposium D-I';
        } 
        elseif ($jenis == 'symposiumd2') {
            $dataPeserta = DataPeserta::where('nama_paket', 'Symposium')->orderBy('pst_id', 'asc')->get();
            $colompresensi = 'status_presensid2';
            $titlepresensi = 'Symposium D-II';
        } 


        $page = [
            'nama_halaman' => 'Cetak Laporan Presensi',
            'appname'      => config('app.name'),
            'datapeserta'  => $dataPeserta,
            'colompresensi' => $colompresensi,
            'titlepresensi' => $titlepresensi
        ];
            
        $mpdf = new Mpdf([
                        'mode'          => 'utf-8',
                        'format'        => 'A4-L', // A4 landscape
                        'margin_left'   => 5, // 2,54 cm = 5 mm
                        'margin_right'  => 5, // 2,54 cm = 5 mm
                        'margin_top'    => 5, // 2,54 cm = 5 mm
                        'margin_bottom' => 5, // 2,54 cm = 5 mm
                        'margin_header' => 0,
                        'margin_footer' =>  0,
        ]);
        
        
        $mpdf->WriteHTML(view('admin-page/stm-cetaklaporanpresensi', $page));
        $mpdf->Output();
        // echo auth()->user()->nama_lengkap;
        // return view('admin-pages/stm-cetaklaporan', $page);
    }

    public function reqAjaxGenerateQRCode()
    {
        GenerateQR::dispatch();
        return response()->json(['statuslog' => 'success', 'message' => 'Job GenerateQR telah dijalankan.', 'title' => 'SELESAI']);
    }

    public function reqAjaxSendNotificationQRCode()
    {
        NotifQRCode::dispatch();
        return response()->json(['statuslog' => 'success', 'message' => 'Job Notification telah dijalankan.', 'title' => 'SELESAI']);
    }



    public function reqAjaxTableDataPaket(Request $request)
    {
        if (request()->ajax()) 
        {
            return DataTables()->of(DataPaket::orderBy('waktu', 'desc')->orderBy('asal_peserta', 'asc')->orderBy('jenis_peserta', 'asc')->orderBy('nama_paket', 'asc')->get())
                ->addIndexColumn()
                ->editColumn('asal_peserta', function($row){
                    return ucfirst($row->asal_peserta);
                })
                ->editColumn('harga', function($row){
                    return 'Rp ' . number_format($row->harga_paket) . '.-';
                })
                ->editColumn('aksi', function($row){
                    return '<a href="javascript:void(0);" class="" onclick="editDataPaket(\''. $row->pketid .'\')">
                                <i class="fas fa-pen btn btn-sm btn-warning p-2"></i>
                            </a>';
                })
                ->rawColumns(['aksi'])
                ->make(true);
        }
    }

    public function reqAjaxTableDataUser(Request $request)
    {
        if (request()->ajax()) 
        {
            return DataTables()->of(DataUsers::orderBy('nama_lengkap', 'asc')->get())
                ->addIndexColumn()
                ->editColumn('level_akun', function($row){
                    return ucfirst($row->level_akun);
                })
                ->editColumn('status_akun', function($row){
                    return $row->status_akun;
                })
                ->editColumn('aksi', function($row){
                    return '<a href="javascript:void(0);" class="" onclick="editDataUser(\''. $row->pmkid .'\')">
                                <i class="fas fa-pen btn btn-sm btn-warning p-2"></i>
                            </a>
                            <a href="javascript:void(0);" class="" onclick="hapusUser(\''. $row->pmkid .'\')">
                                <i class="fas fa-times btn btn-sm btn-danger p-2"></i>
                            </a>';
                })
                ->rawColumns(['aksi'])
                ->make(true);
        }
    }

    public function reqAjaxTableDataAbstract(Request $request)
    {
        if (request()->ajax()) 
        {

            return DataTables()->of(DataAbstractPeserta::with('peserta')->orderBy('pst_id', 'asc')->get())
                ->addIndexColumn()
                ->editColumn('pst_id', function($row){
                    return strtoupper($row->pst_id);
                })
                ->editColumn('nama_lengkap', function($row){
                    return $row->peserta->gelar_depan . ' ' . $row->peserta->nama_lengkap . ' ' . $row->peserta->gelar_belakang;
                })
                ->editColumn('status_akun', function($row){
                    return $row->status_akun;
                })
                ->editColumn('file_abs', function($row){
                    return '<a href="'. asset("file-abstract/$row->file_abs") .'" class="" >
                                <i class="fas fa-cloud-download-alt btn btn-sm btn-secondary p-2"></i>
                            </a>
                            <a href="javascript:void(0);" onclick="previewAbstract(\'https://docs.google.com/viewer?url='. asset("file-abstract/$row->file_abs") .'&embedded=true\')" class="" >
                                <i class="fas fa-eye btn btn-sm btn-warning p-2"></i>
                            </a>';
                })
                ->editColumn('created_at', function($row){
                    return date('H:i d-M-Y', strtotime($row->created_at));
                })
                ->rawColumns(['file_abs'])
                ->make(true);
        }
    }

    public function reqAjaxTablePendaftaranPeserta(Request $request)
    {
        if (request()->ajax()) 
        {
            return DataTables()->of(DataPendaftaran::whereIn('status_pembayaran', ['menunggu', 'ditolak'])->orderBy('created_at', 'desc')->get())
                ->addIndexColumn()
                ->editColumn('kode_invoice', function($row){
                    return strtoupper($row->kode_invoice);
                })
                ->editColumn('status_pembayaran', function($row){
                    return statusPendaftaran($row->status_pembayaran);
                })
                ->editColumn('total_biaya', function($row){
                    return 'Rp. ' . number_format($row->total_biaya) . '.-';
                })
                ->editColumn('created_at', function($row){
                    return date('H:i/d-M-Y', strtotime($row->created_at));
                })
                ->editColumn('notifikasi', function($row){
                    return statusNotifikasi($row->notifikasi_pembayaran) . ' | ' . statusNotifikasi($row->notifikasi_pendaftaran);
                })
                ->editColumn('file_bukti', function($row){
                    if ($row->bukti_pembayaran == null) {
                        return 'Belum Upload';
                    }
                    else{
                        return '<a href="javascript:void(0);" class="" onclick="filePembayaran(\''. $row->pdftrid .'\')">
                                    <i class="fas fa-file-invoice-dollar btn btn-sm btn-secondary p-2"></i>
                                </a>';
                    }
                })
                ->editColumn('aksi', function($row){

                    $aksi = '<a href="javascript:void(0);" class="" onclick="detailPendaftaran(\''. $row->pdftrid .'\')">
                                <i class="fas fa-eye btn btn-sm btn-primary p-2"></i>
                            </a>';

                    if ($row->status_pembayaran == 'menunggu') {
                        $aksi .=    '<a href="javascript:void(0);" class="" onclick="konfirmasiPendaftaran(\''. $row->pdftrid .'\')">
                                        <i class="fas fa-check btn btn-sm btn-success p-2"></i>
                                    </a>
                                    <a href="javascript:void(0);" class="" onclick="tolakPendaftaran(\''. $row->pdftrid .'\')">
                                        <i class="fas fa-times btn btn-sm btn-danger p-2"></i>
                                    </a>';
                        
                        if (auth()->user()->level_akun == 'admin') {
                            $aksi .= '<a href="javascript:void(0);" class="" onclick="hapusPendaftaran(\''. $row->pdftrid .'\')">
                                        <i class="fas fa-trash btn btn-sm btn-dark p-2"></i>
                                    </a>';
                        }
                        
                        return $aksi;
                    }
                    else{
                        if (auth()->user()->level_akun == 'admin') {
                            $aksi .= '<a href="javascript:void(0);" class="" onclick="hapusPendaftaran(\''. $row->pdftrid .'\')">
                                        <i class="fas fa-trash btn btn-sm btn-dark p-2"></i>
                                    </a>';
                        }
                        return $aksi;
                    }
                })
                ->rawColumns(['aksi', 'file_bukti', 'status_pembayaran', 'notifikasi'])
                ->make(true);
        }
    }

    public function reqAjaxTablePeserta(Request $request)
    {
        if (request()->ajax()) 
        {
            return DataTables()->of(DataPeserta::orderBy('pst_id', 'desc')->get())
                ->addIndexColumn()
                ->editColumn('pst_id', function($row){
                    return strtoupper($row->pst_id);
                })
                ->editColumn('kode_invoice', function($row){
                    return strtoupper($row->kode_invoice);
                })
                ->editColumn('nama_lengkap', function($row){
                    return $row->gelar_depan . ' ' . $row->nama_lengkap . $row->gelar_belakang;
                })
                
                ->editColumn('created_at', function($row){
                    return date('H:i/d-M-Y', strtotime($row->created_at));
                })
                ->editColumn('notif', function($row){
                    // return statusNotifikasi($row->notifikasi_pembayaran) . ' | ' . statusNotifikasi($row->notifikasi_pendaftaran);
                    return statusNotifikasi($row->pendaftaran->notifikasi_pembayaran) . ' | ' . statusNotifikasi($row->pendaftaran->notifikasi_pendaftaran);
                })
                ->editColumn('aksi', function($row){

                    $aksi = '<a href="javascript:void(0);" class="" onclick="filePembayaran(\''. $row->pendaftaran->pdftrid .'\')">
                                <i class="fas fa-file-invoice-dollar btn btn-sm btn-secondary p-2"></i>
                            </a>
                            <a href="javascript:void(0);" class="" onclick="detailPendaftaran(\''. $row->pendaftaran->pdftrid .'\')">
                                <i class="fas fa-eye btn btn-sm btn-primary p-2"></i>
                            </a>';
                  
                    return $aksi;                  
                })
                ->rawColumns(['aksi', 'notif'])
                ->make(true);
        }
    }

    public function reqAjaxTablePresensi(Request $request)
    {
        if (request()->ajax()) 
        {
            return DataTables()->of(DataPeserta::orderBy('pst_id', 'desc')->get())
                ->addIndexColumn()
                ->editColumn('pst_id', function($row){
                    return strtoupper($row->pst_id);
                })
                ->editColumn('kode_invoice', function($row){
                    return strtoupper($row->kode_invoice);
                })
                ->editColumn('nama_lengkap', function($row){
                    return $row->gelar_depan . ' ' . $row->nama_lengkap . $row->gelar_belakang;
                })                
                ->editColumn('notif', function($row){
                    return statusNotifikasiPresensi($row->notifikasi_qrpresensi);
                })
                ->editColumn('statusd1', function($row){
                    return statusPresensi($row->status_presensid1);
                })
                ->editColumn('statusd2', function($row){
                    return statusPresensi($row->status_presensid2);
                })
                ->editColumn('statusd3', function($row){
                    return statusPresensi($row->status_presensid3);
                })
                ->editColumn('waktu_presensi', function($row){
                    return date('H:i d/M/Y', strtotime($row->waktu_presensi));
                })
                ->editColumn('aksi', function($row){

                    $aksi = '<a href="'. url('sistem/single-send-email/' . strtolower($row->email)) .'" target="_blank" class="">
                                <i class="fas fa-paper-plane btn btn-sm btn-purple p-2"></i>
                            </a>';
                  
                    return $aksi;                  
                })
                ->rawColumns(['aksi', 'notif', 'statusd1', 'statusd2', 'statusd3'])
                ->make(true);
        }
    }


    
    public function reqAjaxFormTambahPaket(Request $request)
    {
        if (request()->ajax()) 
        {
            $pketid        = Uuid::uuid4();
            $asal_peserta  = $request->asal_peserta;
            $jenis_peserta = $request->jenis_peserta;
            $nama_paket    = $request->nama_paket;
            $harga_paket   = $request->harga_paket;
            $status        = $request->status;

            $data_paket = [
                                    'pketid'        => $pketid,
                                    'asal_peserta'  => $asal_peserta,
                                    'jenis_peserta' => $jenis_peserta,
                                    'nama_paket'    => $nama_paket,
                                    'harga_paket'   => $harga_paket,
                                    'status'        => $status,                                    
                                    'created_at'    => date('Y-m-d H:i:s'),
                                    'updated_at'    => date('Y-m-d H:i:s')
                                ];
            try {
                DB::beginTransaction();
                DataPaket::insert($data_paket);
                DB::commit();
                return response()->json(['statuslog' => 'success', 'message' => 'Data Paket baru berhasil disimpan', 'title' => 'BERHASIL']);
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal menambahkan Data Paket, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqAjaxFormTambahUser(Request $request)
    {
        if (request()->ajax()) 
        {
            $pmkid        = Uuid::uuid4();
            $nama_lengkap = $request->nama_lengkap;
            $email        = $request->email;
            $password     = $request->password;
            $no_hp        = $request->no_hp;
            $level_user   = $request->level_user;
            $status       = $request->status;

            $datausers = [
                            'pmkid'        => $pmkid,
                            'nama_lengkap' => $nama_lengkap,
                            'email'        => $email,
                            'password'     => bcrypt($password),
                            'no_hp'        => $no_hp,
                            'level_akun'   => $level_user,
                            'status_akun'  => $status,
                            'created_at'   => date('Y-m-d H:i:s'),
                            'updated_at'   => date('Y-m-d H:i:s')
                        ];
            try {
                DB::beginTransaction();
                DataUsers::insert($datausers);
                DB::commit();
                return response()->json(['statuslog' => 'success', 'message' => 'Data User baru berhasil disimpan', 'title' => 'BERHASIL']);
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal menambahkan Data User, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqAjaxFormUpdateUser(Request $request)
    {
        if (request()->ajax()) 
        {
            $pmkid        = $request->pmkid;
            $nama_lengkap = $request->nama_lengkap;
            $email        = $request->email;
            $password     = $request->password;
            $no_hp        = $request->no_hp;
            $level_user   = $request->level_user;
            $status       = $request->status;
            $datausers    = [];

            $datausers = [
                            'nama_lengkap' => $nama_lengkap,
                            'email'        => $email,                            
                            'no_hp'        => $no_hp,
                            'level_akun'   => $level_user,
                            'status_akun'  => $status,
                            'updated_at'   => date('Y-m-d H:i:s')
                        ];
            if (!empty($password)) {
                $datausers['password']     = bcrypt($password);
            }

            try {
                DB::beginTransaction();
                    DataUsers::where(['pmkid'=>$pmkid])->update($datausers);
                DB::commit();
                return response()->json(['statuslog' => 'success', 'message' => 'Data User berhasil diupdate', 'title' => 'BERHASIL']);
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal mengupdate Data User, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqAjaxDataPaket(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = ['pketid' => $request->paket];
            $data            = DataPaket::where($cari)->first();
            $result          = NULL;

            if ($data) 
            {
                $result['pketid']        = $data->pketid;
                $result['asal_peserta']  = $data->asal_peserta;
                $result['jenis_peserta'] = $data->jenis_peserta;
                $result['nama_paket']    = $data->nama_paket;
                $result['harga_paket']   = $data->harga_paket;
                $result['status']        = $data->status;
                
                return response()->json($result);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }

    public function reqAjaxDataUser(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = ['pmkid' => $request->user];
            $data            = DataUsers::where($cari)->first();
            $result          = NULL;

            if ($data) 
            {
                $result['pmkid']        = $data->pmkid;
                $result['email']        = $data->email;
                $result['nama_lengkap'] = $data->nama_lengkap;
                $result['no_hp']        = $data->no_hp;
                $result['level_akun']   = $data->level_akun;
                $result['status_akun']  = $data->status_akun;
                
                return response()->json($result);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }

    public function reqAjaxFormUpdatePaket(Request $request)
    {
        if (request()->ajax()) 
        {
            $pketid        = ['pketid' => $request->pketid];
            $asal_peserta  = $request->asal_peserta;
            $jenis_peserta = $request->jenis_peserta;
            $nama_paket    = $request->nama_paket;
            $harga_paket   = $request->harga_paket;
            $status        = $request->status;

            $data_paket = [
                                    'asal_peserta'  => $asal_peserta,
                                    'jenis_peserta' => $jenis_peserta,
                                    'nama_paket'    => $nama_paket,
                                    'harga_paket'   => $harga_paket,
                                    'status'        => $status,                                    
                                    'updated_at'    => date('Y-m-d H:i:s')
                                ];
            try {
                DB::beginTransaction();
                    DataPaket::where($pketid)->update($data_paket);
                DB::commit();
                return response()->json(['statuslog' => 'success', 'message' => 'Data Paket berhasil diupdate', 'title' => 'BERHASIL']);
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal mengupdate Data Paket, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqAjaxHargaPaket(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = ['jenis_peserta' => $request->peserta, 'status' => 'Active'];
            $data            = DataPaket::where($cari)->get()->toArray();
            $result          = NULL;

            if ($data) 
            {
                $result = '<option value="" hidden selected disabled>-- Jenis Kegiatan --</option>';
                foreach ($data as $key => $value) {
                    if ($value['nama_paket'] == 'Workshop + Symposium') {
                        $result .= '<option value="' . $value['pketid'] . '" data-harga="'.$value['harga_paket'].'">' . $value['nama_paket'] . ' | Rp. '. number_format($value['harga_paket']) .' Have get Disc 10%</option>';
                    }
                    else{
                        $result .= '<option value="' . $value['pketid'] . '" data-harga="'.$value['harga_paket'].'">' . $value['nama_paket'] . ' | Rp. '. number_format($value['harga_paket']) .' </option>';
                    }
                }                
                return response()->json($result);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }
    
    public function reqAjaxNotifikasiUlang(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = $request->email;
            $data            = DataPendaftaran::whereJsonContains('data_pendaftaran', ['email' => $cari])->first();
            $result          = NULL;
            
            if ($data) 
            {    
                $data_email  = [
                                'nama_lengkap' => $data['data_pendaftaran'][0]['gelar_depan'] . ' ' . $data['data_pendaftaran'][0]['nama_lengkap'] . ' ' . $data['data_pendaftaran'][0]['gelar_belakang'],
                                'email'        => $data['data_pendaftaran'][0]['email'],
                                'kode_invoice' => $data['kode_invoice'],
                                'nama_paket'   => $data['data_pendaftaran'][0]['nama_paket'],
                                'total_biaya'  => $data['total_biaya'],
                                'kode_unik'    => $data['kode_unik'],
                            ];
                try {
                    // Mail::to('anggiat.up@gmail.com', 'QRCode Presensi PatKlin')->send(new SendEmail($data));
                    // Mail::to($email_peserta)->send(new SendEmail($data));
                    Mail::to($cari)->send(new NotificationEmail($data_email));
                    DataPendaftaran::where(['pdftrid' => $data['pdftrid']])->update(['notifikasi_pembayaran' => 'berhasil', 'updated_at' => date('Y-m-d H:i:s')]);
                    return response()->json(['statuslog' => 'success', 'message' => 'Notifikasi ulang berhasil silahkan check email anda untuk melanjutkan proses pembayaran', 'title' => 'BERHASIL']);

                } catch (\Throwable $th) {
                    DataPendaftaran::where(['pdftrid' => $data['pdftrid']])->update(['notifikasi_pembayaran' => 'gagal', 'updated_at' => date('Y-m-d H:i:s')]);
                    return response()->json(['statuslog' => 'error', 'message' =>  'Notifikasi ulang Gagal jaringan tidak mendukung sehingga pengiriman email gagal silahkan notifikasi ulang kembali atau ganti jaringan anda' , 'title' => 'GAGAL']);
                    // return response()->json(['statuslog' => 'success', 'message' =>  $th->getMessage() , 'title' => 'BERHASIL']);
                }
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }
    
    public function reqAjaxInvoice(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = $request->invoice;
            $data            = DataPendaftaran::where('kode_invoice', $cari)->first();
            $result          = NULL;

            if ($data) 
            {   
                $data = $data->toArray(); 
                $result  = [
                                'nama_lengkap' => $data['data_pendaftaran'][0]['gelar_depan'] . ' ' . $data['data_pendaftaran'][0]['nama_lengkap'] . ' ' . $data['data_pendaftaran'][0]['gelar_belakang'],                                
                                'nama_paket'   => $data['data_pendaftaran'][0]['nama_paket'],
                                'total_biaya'  => 'Rp. ' . number_format($data['total_biaya']),
                            ];
                                
                return response()->json($result);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }

    public function reqAjaxNoreg(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = $request->peserta;
            $data            = DataPeserta::where('pst_id', $cari)->first();
            $result          = NULL;

            if ($data) 
            {   
                $data = $data->toArray(); 
                $result  = [
                                'nama_lengkap' => $data['gelar_depan'] . ' ' . $data['nama_lengkap'] . ' ' . $data['gelar_belakang'],                                
                                'email'        => $data['email'],
                                'nohp'         => $data['nohp'],
                            ];
                                
                return response()->json($result);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }

    public function reqAjaxBuktiPembayaran(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = $request->pendaftaran;
            $data            = DataPendaftaran::where('pdftrid', $cari)->first();
            $result          = NULL;

            if ($data) 
            {   
                $data = $data->toArray(); 
                $result  = [
                                'kode_invoice'     => strtoupper($data['kode_invoice']),
                                'total_biaya'      => 'Rp. ' . number_format($data['total_biaya']),
                                'kode_unik'        => $data['kode_unik'],
                                'bukti_pembayaran' => asset('bukti-pembayaran/' . $data['bukti_pembayaran']),
                                'btn_verify'       => '-'
                            ];
                
                if ($data['status_pembayaran'] == 'menunggu') {
                    $result['btn_verify'] =     '<a href="javascript:void(0);" class="btn btn-sm btn-success" onclick="konfirmasiPendaftaran(\''. $cari .'\')">
                                                    <i class="fas fa-check  p-2"></i> Konfirmasi Pendaftaran
                                                </a>
                                                <a href="javascript:void(0);" class="btn btn-sm btn-danger" onclick="tolakPendaftaran(\''. $cari .'\')">
                                                    <i class="fas fa-times  p-2"></i> Pembayaran Tidak Ditemukan
                                                </a>';
                }
                                
                return response()->json($result);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }
    
    public function reqAjaxDataPesertaPendaftaran(Request $request)
    {   
        if (request()->ajax()) 
        {
            $cari            = $request->pendaftaran;
            $data            = DataPendaftaran::where('pdftrid', $cari)->first();
            $result          = NULL;

            if ($data) 
            {   
                $data             = $data->toArray(); 
                $data_pendaftaran = $data['data_pendaftaran'];

                $result  = [
                                'kode_invoice'       => strtoupper($data['kode_invoice']),
                                'total_biaya'        => 'Rp. ' . number_format($data['total_biaya']),
                                'kode_unik'          => $data['kode_unik'],
                                'status_pembayaran'  => ucfirst($data['status_pembayaran']),
                                'admin_verifikasi'   => $data['admin_verifikasi'],
                                'waktu_pendaftaran'  => date('H:i d-M-Y', strtotime($data['created_at'])),
                            ];
                
                $table_peserta = '<table class="table-sm" width="100%">';

                foreach ($data_pendaftaran as $value) {
                    $table_peserta .= '
                                        <tr>
                                            <th width="30%">Paket Kegiatan</th>
                                            <td width="5%">:</td>
                                            <td>'. $value["nama_paket"] .'</td>
                                        </tr>
                                        <tr>
                                            <th>Nama Lengkap Peserta</th>
                                            <td>:</td>
                                            <td> ' . $value["gelar_depan"] . ' ' . $value["nama_lengkap"] . ' ' . $value["gelar_belakang"] .' </td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>:</td>
                                            <td>'. $value["email"] .'</td>
                                        </tr>
                                        <tr>
                                            <th>Nomor Hp</th>
                                            <td>:</td>
                                            <td>'. $value["nohp"] .'</td>
                                        </tr>
                                        <tr>
                                            <th>Instansi Asal</th>
                                            <td>:</td>
                                            <td>'. $value["instansi_asal"] .'</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><hr></td>
                                        </tr>
                                    ';
                }

                $table_peserta .= ' </table>';

                $result['data_pendaftaran'] = $table_peserta;
                return response()->json($result);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }

    public function reqAjaxDataPesertaSertifikat(Request $request)
    {   
        if (request()->ajax()) 
        {
            $cari            = strtolower($request->nomor_peserta);
            $data            = DataPeserta::where('pst_id', $cari)->first();
            $result          = NULL;

            if ($data) 
            {   
                $data             = $data->toArray(); 

                $result  = [
                                'nama_lengkap'   => $data['gelar_depan'] . ', ' . $data['nama_lengkap'] . ', ' . $data['gelar_belakang'],
                                'pst_id'         => $data['pst_id'],
                                'nama_paket'     => $data['nama_paket'],
                            ];
                ;

                return response()->json(['statuslog'=> 'success' ,'message' => 'Data ditemukan', 'title'=> 'BERHASIL', 'data' => $result], 200);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 200);
            }
        }
    }

    public function reqAjaxCetakSertifikat(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari              = strtolower($request->nomor_peserta);
            $namaBaruPeserta   = $request->namalengkappeserta;
            $data              = DataPeserta::where('pst_id', $cari)->first();
            $result            = NULL;

            if ($data) {   
                $dataNew         = $data->toArray(); 
                $arsipSertifikat = NULL; 

                $genSertifikat = convertTextToArray($dataNew['nama_paket']);

                foreach ($genSertifikat as $value) 
                {
                    if ($this->generateCertificate($namaBaruPeserta, $dataNew, $value . '.png', $value)) {
                        $arsipSertifikat[] = strtoupper($dataNew['pst_id']) . '-'. $value .'.jpg';
                    }
                }
                
                if (!empty($arsipSertifikat)) { 

                    $zipFilePath = public_path('generate-sertifikat/' . strtoupper($dataNew['pst_id']) . '-sertifikat.zip');
                    $zip = new ZipArchive();

                    if ($zip->open($zipFilePath, ZipArchive::CREATE) !== TRUE) {
                        return response()->json(['statuslog'=> 'error' ,'message' => 'Tidak bisa melakukan arsip sertifikat', 'title'=> 'GAGAL'], 404);
                    }

                    foreach ($arsipSertifikat as $file) 
                    {
                        if (file_exists(public_path('generate-sertifikat/' . $file))) {
                            $zip->addFile(public_path('generate-sertifikat/' . $file), basename($file));
                        }
                    }
                    
                    $zip->close();

                    if (file_exists($zipFilePath)) {
                        return response()->json([
                            'statuslog'     => 'success',
                            'message'       => 'Sertifikat berhasil digenerate. Jika proses download tidak berjalan silahkan klik link berikut. <a href="'. url('download/' . strtolower($dataNew['pst_id']) . '-sertifikat.zip').'" target="_blank">DOWNLOAD</a>',
                            'title'         => 'BERHASIL',
                            'downloadlink'  => url('download/' . strtoupper($dataNew['pst_id']) . '-sertifikat.zip')
                        ], 200);
                    } else {
                        return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal membuat file ZIP.', 'title'=> 'GAGAL'], 500);
                    }
                }
                else{
                    return response()->json(['statuslog'=> 'error' ,'message' => 'Proses generate Sertifikat gagal', 'title'=> 'GAGAL'], 404);
                }

            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }

    public function generateCertificate($namaBaruPeserta, $dataNew, $namaTmpt, $namasertifikat)
    {
        $templatePath = 'template-sertifikat/' . strtolower($namaTmpt);
        
        $image = ImageManager::imagick()->read(public_path($templatePath));

        $imageWidth = $image->width() / 2;
        
        // Menambahkan teks ke gambar
        
        if ($namasertifikat == 'Workshop I' || $namasertifikat == 'Workshop II') {
            
            $imageHeight = 810;
            $image->text($namaBaruPeserta, $imageWidth, $imageHeight, function (FontFactory $font) {
                $font->filename(public_path('template-sertifikat/Roboto-Medium.ttf'));
                $font->size(55);
                $font->color('000');
                $font->align('center');
                $font->valign('middle');
                $font->lineHeight(1.6);
                $font->angle(0);
            });
        }
        else {
            $imageHeight = 930;
            $image->text($namaBaruPeserta, $imageWidth, $imageHeight, function (FontFactory $font) {
                $font->filename(public_path('template-sertifikat/Roboto-Medium.ttf'));
                $font->size(55);
                $font->color('000');
                $font->align('center');
                $font->valign('middle');
                $font->lineHeight(1.6);
                $font->angle(0);
            });
        }

        $filename = strtoupper($dataNew['pst_id']) . '-'. $namasertifikat .'.jpg';
        $savePath = public_path('generate-sertifikat/' . $filename);

        try {
            $image->save($savePath);
            return true;
        } catch (\Exception $e) {
            return false;
        }

    }

    public function reqAjaxVerifikasi(Request $request)
    {
        if (request()->ajax()) 
        {
            $pdftrid           = $request->pendaftaran;
            $status_pembayaran = $request->status;
            $admin_verifikasi  = auth()->user()->pmkid;

            $pendaftaran = [
                                    'status_pembayaran' => $status_pembayaran,
                                    'admin_verifikasi'  => $admin_verifikasi,                  
                                    'updated_at'        => date('Y-m-d H:i:s')
                                ];
            try {
                DB::beginTransaction();
                    DataPendaftaran::where(['pdftrid' => $pdftrid])->update($pendaftaran);
                DB::commit();

                $data             = DataPendaftaran::where('pdftrid', $pdftrid)->first();
                $data             = $data->toArray(); 
                $data_pendaftaran = $data['data_pendaftaran'];
                $nomor            = 0;
                $data_peserta     = [];

                foreach ($data_pendaftaran as $value) 
                {
                    $nomor += 1;
                    $data_peserta[] = [
                        'pst_id'         => generateID($nomor),
                        'kode_invoice'   => $data['kode_invoice'],
                        'jenis_peserta'  => $value['jenis_peserta'],
                        'paket_id'       => $value['paket_id'],
                        'nama_paket'     => $value['nama_paket'],
                        'harga_paket'    => $value['harga_paket'],
                        'gelar_depan'    => $value['gelar_depan'],
                        'gelar_belakang' => $value['gelar_belakang'],
                        'nama_lengkap'   => $value['nama_lengkap'],
                        'email'          => $value['email'],
                        'nohp'           => $value['nohp'],
                        'instansi_asal'  => $value['instansi_asal'],
                        'created_at'     => date('Y-m-d H:i:s'),
                        'updated_at'     => date('Y-m-d H:i:s')
                    ];
                    
                }
                
                if ($status_pembayaran == 'terverifikasi') {
                    DB::beginTransaction();
                        DataPeserta::insert($data_peserta);
                    DB::commit();
                    
                    try {
                        Mail::to($data_peserta[0]['email'])->send(new NotificationPendaftaranPeserta($data_peserta[0]));
                        DataPendaftaran::where(['pdftrid' => $pdftrid])->update(['notifikasi_pendaftaran' => 'berhasil']);
                        return response()->json(['statuslog' => 'success', 'message' => 'Pendaftaran Peserta berhasil ' .  ucfirst($status_pembayaran) . ' dan notifikasi email berhasil dikirim', 'title' => 'BERHASIL']);
                    } 
                    catch (\Throwable $th) {
                        DataPendaftaran::where(['pdftrid' => $pdftrid])->update(['notifikasi_pendaftaran' => 'gagal']);
                        return response()->json(['statuslog' => 'success', 'message' => 'Pendaftaran Peserta berhasil ' .  ucfirst($status_pembayaran) . ' namun notifikasi email gagal dikirim.'  . $th->getMessage(), 'title' => 'BERHASIL']);
                    }
                }
                else{
                    return response()->json(['statuslog' => 'success', 'message' => 'Pendaftaran telah ' .  ucfirst($status_pembayaran) . ' Segera kontak langsung peserta untuk notifikasi lebih baik.' , 'title' => 'BERHASIL']);
                }
                
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal Memverifikasi Peserta, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqAjaxHapusPendaftaran(Request $request)
    {
        if (request()->ajax()) 
        {
            $pdftrid           = $request->pendaftaran;
            $status_pembayaran = $request->status;
            $admin_verifikasi  = auth()->user()->pmkid;

            try {
                DB::beginTransaction();
                    DataPendaftaran::where(['pdftrid' => $pdftrid])->delete();
                DB::commit();
                return response()->json(['statuslog' => 'success', 'message' => 'Pendaftaran Peserta berhasil dihapus', 'title' => 'BERHASIL']);
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal menghapus data Peserta, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqAjaxRegistrasiPeserta(Request $request)
    {
        if (request()->ajax()) 
        {
            $jenis_partisipasi    = $request->input('jenis-partisipasi');
            $jenis_kegiatan       = $request->input('jenis-kegiatan');
            $total_biaya          = $request->input('total-biaya');
            $gelar_depan          = $request->input('gelar-depan');
            $gelar_belakang       = $request->input('gelar-belakang');
            $nama_lengkap_peserta = $request->input('nama-lengkap-peserta');
            $email                = $request->input('email');
            $nohp                 = $request->input('nohp');
            $instansi_asal        = $request->input('instansi-asal');

            $data            = DataPaket::where(['pketid' => $jenis_kegiatan])->first()->toArray();
            $harga_paket     = $data['harga_paket'];
            $nama_paket      = $data['nama_paket'];
            $kode_uniq       = mt_rand(100, 999);
            $invoice         = generatePesertaID(1);
            $pdftrid         = Uuid::uuid4();

            $data_peserta[] = [
                                    'jenis_peserta'  => $jenis_partisipasi,
                                    'paket_id'       => $jenis_kegiatan,
                                    'nama_paket'     => $nama_paket,
                                    'harga_paket'    => $harga_paket,
                                    'gelar_depan'    => $gelar_depan,
                                    'gelar_belakang' => $gelar_belakang,
                                    'nama_lengkap'   => $nama_lengkap_peserta,
                                    'email'          => $email,
                                    'nohp'           => $nohp,
                                    'instansi_asal'  => $instansi_asal,
                                ];
            $data_registrasi = [
                                    'pdftrid'           => $pdftrid,
                                    'kode_invoice'      => $invoice,
                                    'data_pendaftaran'  => json_encode($data_peserta),
                                    'total_biaya'       => $harga_paket + $kode_uniq,
                                    'kode_unik'         => $kode_uniq,
                                    'status_pembayaran' => 'menunggu',
                                    'bukti_pembayaran'  => NULL,
                                    'admin_verifikasi'  => NULL,
                                    'created_at'        => date('Y-m-d H:i:s'),
                                    'updated_at'        => date('Y-m-d H:i:s')
                                ];
            
            $data_email      = [
                                    'nama_lengkap' => $gelar_depan . ' ' . $nama_lengkap_peserta . ' ' . $gelar_belakang,
                                    'email'        => $email,
                                    'kode_invoice' => $invoice,
                                    'nama_paket'   => $nama_paket,
                                    'total_biaya'  => $harga_paket,
                                    'kode_unik'    => $kode_uniq,
            ];
                                
            $datapesertapdf = DataPendaftaran::whereJsonContains('data_pendaftaran', ['email' => $email])->first();
            if (!$datapesertapdf) 
            {
                try {
                    DB::beginTransaction();
                        DataPendaftaran::insert($data_registrasi);
                    DB::commit();
                    try {
                        // Mail::to('anggiat.up@gmail.com', 'QRCode Presensi PatKlin')->send(new SendEmail($data));
                        // Mail::to($email_peserta)->send(new SendEmail($data));
                        Mail::to($email)->send(new NotificationEmail($data_email));
                        DataPendaftaran::where(['pdftrid' => $pdftrid])->update(['notifikasi_pembayaran' => 'berhasil']);
                        return response()->json(['statuslog' => 'success', 'message' => 'Registrasi berhasil silahkan check email anda untuk melanjutkan proses pembayaran', 'title' => 'BERHASIL']);
    
                    } catch (\Throwable $th) {
                        // return response()->json(['statuslog' => 'success', 'message' =>  $th->getMessage() , 'title' => 'BERHASIL']);
                        DataPendaftaran::where(['pdftrid' => $pdftrid])->update(['notifikasi_pembayaran' => 'gagal']);
                        return response()->json(['statuslog' => 'success', 'message' =>  'Registrasi berhasil namun jaringan tidak stabil sehingga pengiriman email gagal silahkan klik menu notifikasi ulang' , 'title' => 'BERHASIL']);
                    }
                } 
                catch (\Exception $e) 
                {
                    DB::rollback();
                    return response()->json(['statuslog' => 'error', 'message' => 'Registrasi gagal, mohon coba kembali', 'title' => 'GAGAL'], 404);
                }
            }
            else{
                return response()->json(['statuslog' => 'error', 'message' => 'Alamat Email yang anda gunakan telah terdaftar didalam sistem, jika anda belum menerima email silahkan lakukan notifikasi ulang email'], 404);
            }
            
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqAjaxFormUploadPembayaran(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = $request->kode_invoice;
            $data            = DataPendaftaran::where('kode_invoice', $cari)->first();
            $file            = $request->file('bukti-pembayaran');

            if ($file->getSize() > 1048576) {
                return response()->json(['statuslog' => 'error', 'message' => 'Ukuran file terlalu besar. Maksimum 1 MB', 'title' => 'GAGAL'], 404);
            }

            $allowedExtensions = ['jpg', 'jpeg', 'png', 'gif'];
            if (!in_array(strtolower($file->getClientOriginalExtension()), $allowedExtensions)) {
                return response()->json(['statuslog' => 'error', 'message' => 'Jenis file tidak didukung. Hanya diperbolehkan: JPG, JPEG, PNG, GIF', 'title' => 'GAGAL'], 404);
            }

            $nama_file       = uniqid() . '-' . time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('bukti-pembayaran'), $nama_file);

            
            $data_paket = [
                                    'bukti_pembayaran' => $nama_file,                         
                                    'updated_at'       => date('Y-m-d H:i:s')
                                ];
            try {
                DB::beginTransaction();
                    DataPendaftaran::where(['kode_invoice' => $cari])->update($data_paket);
                DB::commit();
                return response()->json(['statuslog' => 'success', 'message' => 'Bukti Pembayaran berhasil diupload', 'title' => 'BERHASIL']);
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal mengupload Bukti Pembayaran, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqAjaxFormAbstract(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = $request->noregpeserta;
            $data            = DataPeserta::where('pst_id', $cari)->first()->toArray();
            $file            = $request->file('file-abs');

            if ($file->getSize() > 5242880) {
                return response()->json(['statuslog' => 'error', 'message' => 'Ukuran file terlalu besar. Maksimum 5 MB', 'title' => 'GAGAL'], 404);
            }

            $allowedExtensions = ['docx', 'doc'];
            if (!in_array(strtolower($file->getClientOriginalExtension()), $allowedExtensions)) {
                return response()->json(['statuslog' => 'error', 'message' => 'Jenis file tidak didukung. Hanya diperbolehkan: .DOCX, .DOC', 'title' => 'GAGAL'], 404);
            }
            
            $nama_file       = 'ABS-' . strtoupper($cari) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('file-abstract'), $nama_file);


            $data_paket = [
                                    // 'file_abs'      => $data->peserta->gelar_depan . ' ' . $data->peserta->nama_lengkap . ' ' . $data->peserta->gelar_belakang,
                                    'absid'          => Uuid::uuid4(),                         
                                    'pst_id'         => $data['pst_id'],
                                    'file_abs'       => $nama_file,                         
                                    'status_peserta' => 'menunggu',
                                    'created_at'     => date('Y-m-d H:i:s'),
                                    'updated_at'     => date('Y-m-d H:i:s')
                                ];
            try {
                DB::beginTransaction();
                    DataAbstractPeserta::insert($data_paket);
                DB::commit();
                return response()->json(['statuslog' => 'success', 'message' => 'Pendaftaran abstract berhasil dilakukan', 'title' => 'BERHASIL']);
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal melakukan pendaftaran abstract, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqAjaxSearchPeserta(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = $request->peserta;
            $data            = DataPeserta::searchPeserta($cari);
            $result          = NULL;
            $detailpeserta   = NULL;

            if (!empty($data)) {
                
                if (count($data) > 1) {
                    foreach ($data as $peserta) {
                        $result .= '<li class="list-group-item d-flex justify-content-between align-items-start" >' .
                                        '<div class="ms-2 me-auto">' .
                                            '<div class="header-title text-primary" onclick="getIdPeserta(\''. strtoupper($peserta['pst_id']) .'\')" style="cursor:pointer;"><b>' . strtoupper($peserta['pst_id']) . '</b></div>
                                            <span class="text-dark">' .
                                            $peserta['nama_lengkap'] . ' <br> ' . 
                                            strtolower($peserta['email']) . '
                                            </span>
                                        </div>' .
                                    '</li>';
                    }
                    return response()->json(['statuslog'=> 'success' ,'message' => 'Berhasil', 'results' => $result ], 200);
                }
                else {
                    
                    $result .= '<li class="list-group-item d-flex justify-content-between align-items-start">' .
                                '<div class="ms-2 me-auto">' .
                                    '<div class="header-title"><b>' . strtoupper($data[0]['pst_id']) . '</b></div>' .
                                    $data[0]['nama_lengkap'] .
                                '</div>' .
                            '</li>';
            
                    
                    $waktuPresensi   = NULL;
                    $statusPresensi  = NULL;
                    $buttonPresensi  = NULL;
                    $tanggalAbsensi  = date('Y-m-d');
            
                    if ($tanggalAbsensi == '2024-09-06') 
                    {
                        $waktuPresensi  = $data[0]['waktu_presensid1'] ? date('H:i, d/m/Y', strtotime($data[0]['waktu_presensid1'])) : '-';
                        $statusPresensi = $data[0]['status_presensid1'] == 'hadir' ? 'STATUS KEHADIRAN D-1 : Telah Hadir' : 'STATUS KEHADIRAN D-1 : Belum Hadir';
                        $buttonPresensi = buttonPresensi($data[0]['pst_id'], $data[0]['status_presensid1']);
                    }
                    else if ($tanggalAbsensi == '2024-09-07') 
                    {
                        $waktuPresensi  = $data[0]['waktu_presensid2'] ? date('H:i, d/m/Y', strtotime($data[0]['waktu_presensid2'])) : '-';
                        $statusPresensi = $data[0]['status_presensid2'] == 'hadir' ? 'STATUS KEHADIRAN D-2 : Telah Hadir' : 'STATUS KEHADIRAN D-2 : Belum Hadir';
                        $buttonPresensi = buttonPresensi($data[0]['pst_id'], $data[0]['status_presensid2']);
                    }
                    else if ($tanggalAbsensi == '2024-09-08') 
                    {
                        $waktuPresensi  = $data[0]['waktu_presensid3'] ? date('H:i, d/m/Y', strtotime($data[0]['waktu_presensid3'])) : '-';
                        $statusPresensi = $data[0]['status_presensid3'] == 'hadir' ? 'STATUS KEHADIRAN D-3 : Telah Hadir' : 'STATUS KEHADIRAN D-3 : Belum Hadir';
                        $buttonPresensi = buttonPresensi($data[0]['pst_id'], $data[0]['status_presensid3']);
                    }
                    

                    $detailpeserta =   '<h4 class=" text-center mb-3">DETAIL DATA PESERTA</h4>
                                        <div class="table-responsive">
                                            <table class="table  table-striped mb-0">
                                                <tbody><tr>
                                                    <th class="align-middle" scope="row" width="20%" height="40px">Nama Lengkap</th>
                                                    <td class="align-middle" width="5px">:</td>
                                                    <td class="align-middle" id="nama_peserta">'. $data[0]['gelar_depan'] .' ' . $data[0]['nama_lengkap'] . ' ' . $data[0]['gelar_belakang'] . '</td>
                                                    <th class="align-middle" scope="row" width="20%">No. Registrasi</th>
                                                    <td class="align-middle" width="5px">:</td>
                                                    <td class="align-middle" id="peserta_id">'. strtoupper($data[0]['pst_id']) .'</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Institusi</th>
                                                    <td>:</td>
                                                    <td  id="institusi">'. $data[0]['instansi_asal'] .'</td>
                                                    <th scope="row">Kegiatan</th>
                                                    <td>:</td>
                                                    <td  id="">'. $data[0]['nama_paket'] .'</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Email</th>
                                                    <td>:</td>
                                                    <td id="email_peserta">'. strtolower($data[0]['email']) .'</td>
                                                    <th scope="row">No. Handphone</th>
                                                    <td>:</td>
                                                    <td id="no_hp">'. $data[0]['nohp'] .'</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row"><b>Waktu Presensi</b></th>
                                                    <td>:</td>
                                                    <td id="" colspan="4">'. $waktuPresensi .'</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="6" class="text-center">
                                                        <h4 class="card-title text-center">' . $statusPresensi . '</h4>
                                                        '. $buttonPresensi .'
                                                    </td>
                                                </tr>
                                                
                                            </tbody></table>
                                        </div>';


                    return response()->json(['statuslog'=> 'single-success' ,'message' => 'Berhasil', 'results' => $result, 'detaildata' => $detailpeserta ], 200);
                }

            }
            else {
                $result = '<li class="list-group-item text-dark ">
                                <div class="ms-2 me-auto">
                                <div class="header-title text-center"><i><b>Tidak ada data peserta yang ditemukan</b></i></div>
                                </div>
                            </li>';
                
                return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal', 'results' => $result ], 200);
            }

           
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqAjaxPresensiKehadiran(Request $request)
    {
        if (request()->ajax()) 
        {
            $pesertaid    = $request->peserta;
            $datapeserta  = [];
            
            $tanggalAbsensi = date('Y-m-d');
            if ($tanggalAbsensi == '2024-09-06') 
            {
                $datapeserta = [
                    'status_presensid1'  => 'hadir',
                    'waktu_presensid1'   => now(),
                ];
            }
            else if ($tanggalAbsensi == '2024-09-07') 
            {
                $datapeserta = [
                    'status_presensid2'  => 'hadir',
                    'waktu_presensid2'   => now(),
                ];
            }
            else if ($tanggalAbsensi == '2024-09-08') 
            {
                $datapeserta = [
                    'status_presensid3'  => 'hadir',
                    'waktu_presensid3'   => now(),
                ];
            }
            else {
                return response()->json(['statuslog'=> 'error' ,'message' => 'Tidak bisa melakukan presensi pada tanggal ' . date('d-m-Y', strtotime($tanggalAbsensi))], 404);
            }


            try {
                DB::beginTransaction();
                    DataPeserta::where(['pst_id'=>$pesertaid])->update($datapeserta);
                DB::commit();
                return response()->json(['statuslog' => 'success', 'message' => 'Kehadiran Peserta berhasil dikonfirmasi', 'title' => 'BERHASIL']);
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal mengkonfirmasi kehadiran, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqAjaxResetPresensi(Request $request)
    {
        if (request()->ajax()) 
        {
            $presensi       = $request->hari;
            $presensiDay    = NULL;
            $statusPresensi = NULL;

            // Set kolom presensi berdasarkan input hari
            if ($presensi == 'day1') 
            {
                $presensiDay    = 'status_presensid1';
                $statusPresensi = ['status_presensid1' => NULL];
            } elseif ($presensi == 'day2') 
            {
                $presensiDay    = 'status_presensid2';
                $statusPresensi = ['status_presensid2' => NULL];
            } elseif ($presensi == 'day3') 
            {
                $presensiDay    = 'status_presensid3';
                $statusPresensi = ['status_presensid3' => NULL];
            }

            if ($presensiDay) { // Check if day is set
                try {
                    DB::beginTransaction();
                        DataPeserta::whereNotNull($presensiDay)->update($statusPresensi);
                    DB::commit();
                    return response()->json(['statuslog' => 'success', 'message' => 'Reset Presensi Berhasil', 'title' => 'BERHASIL']);
                } 
                catch (\Exception $e) 
                {
                    DB::rollback();
                    return response()->json(['statuslog' => 'error', 'message' => 'Gagal melakukan reset presensi', 'title' => 'GAGAL'], 404);
                }
            } else {
                return response()->json(['statuslog' => 'error', 'message' => 'Hari presensi tidak valid', 'title' => 'GAGAL'], 400);
            }
        }

    }

    public function generateQRCodeForPeserta(Request $request)
    {
        // GenerateQR::dispatch();
        // return response()->json(['status' => 'success', 'message' => 'Job GenerateQR telah dijalankan.']);
    }
}
