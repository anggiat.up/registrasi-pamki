<?php

namespace App\Http\Controllers\Sistems;

use App\Http\Controllers\Controller;
use App\Models\DataAbstractPeserta;
use App\Models\DataHotel;
use App\Models\DataPaketHotel;
use App\Models\DataReservasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class HotelController extends Controller
{
    public function pgeHotel()
    {

        $page = [
            'nama_halaman' => 'Data Hotel',
            'appname'      => config('app.name'),
            
        ];

        return view('admin-page/stm-hotel', $page);
    }

    public function reqAjaxTableDataHotel(Request $request)
    {
        if (request()->ajax()) 
        {

            return DataTables()->of(DataHotel::orderBy('rate_bintang', 'desc')->get())
                ->addIndexColumn()
                
                ->editColumn('gambar_hotel', function($row){
                    return '<img src="'. asset("gambar-hotel/$row->gambar_hotel") .'" width="100px" alt="">';
                })
                ->editColumn('aksi', function($row){
                    return '<a href="javascript:void(0);" class="btn btn-sm btn-warning disabled"  onclick="editDataHotel(\''. $row->hotelid .'\')">
                                <i class="fas fa-pen  p-2"></i>
                            </a>
                            <a href="javascript:void(0);" class="btn btn-sm btn-danger disabled" onclick="hapusHotel(\''. $row->hotelid .'\')">
                                <i class="fas fa-times  p-2"></i>
                            </a>';
                })
                ->rawColumns(['aksi', 'gambar_hotel'])
                ->make(true);
        }
    }

    public function reqAjaxFormTambahHotel(Request $request)
    {
        if (request()->ajax()) 
        {
            $hotelid       = Uuid::uuid4();
            $nama_hotel   = $request->nama_hotel;
            $alamat       = $request->alamat;
            $rate_bintang = $request->rate_bintang;
            $status       = $request->status;

            $file            = $request->file('gambar_hotel');
            $nama_file       = 'hotel-' . uniqid() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('gambar-hotel'), $nama_file);
            
            $gambar_hotel  = $request->gambar_hotel;
            $datahotel = [
                                    'hotelid'      => $hotelid,
                                    'nama_hotel'   => $nama_hotel,
                                    'alamat'       => $alamat,
                                    'gambar_hotel' => $nama_file,
                                    'rate_bintang' => $rate_bintang,
                                    'status'       => $status,                                    
                                    'created_at'   => date('Y-m-d H:i:s'),
                                    'updated_at'   => date('Y-m-d H:i:s')
                                ];
            try {
                DB::beginTransaction();
                    DataHotel::insert($datahotel);
                DB::commit();
                return response()->json(['statuslog' => 'success', 'message' => 'Data Hotel baru berhasil disimpan', 'title' => 'BERHASIL']);
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal menambahkan Data Hotel, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }



    // Paket Hotel 
    public function pgePaketHotel()
    {
        $dataHotel = DataHotel::orderBy('nama_hotel', 'asc')->get()->toArray();
        $page = [
            'nama_halaman' => 'Data Paket Hotel',
            'appname'      => config('app.name'),
            'datahotel'    => $dataHotel
            
        ];
        // print_r(DataAbstractPeserta::with('peserta')->orderBy('pst_id', 'asc')->get()->toArray());
        // print_r(DataPaketHotel::with('hoteldata')->orderBy('hotel_id', 'asc')->get()->toArray());
        return view('admin-page/stm-pakethotel', $page);
    }

    public function reqAjaxTableDataPaketHotel(Request $request)
    {
        if (request()->ajax()) 
        {
            
            return DataTables()->of(DataPaketHotel::with(['hoteldata' => function ($query) {$query->orderBy('rate_bintang', 'desc');}])->orderBy('harga_paket', 'asc')->get())
                ->addIndexColumn()
                ->editColumn('harga_paket', function($row){
                    return 'Rp. ' . number_format($row->harga_paket);
                })
                ->editColumn('nama_hotel', function($row){
                    return $row->hoteldata->nama_hotel;
                })
                ->editColumn('status_paket', function($row){
                    return $row->status_paket;
                })
                ->editColumn('aksi', function($row){
                    return '<a href="javascript:void(0);" class="btn btn-sm btn-warning "  onclick="editDataPaketHotel(\''. $row->pakthtlid .'\')">
                                <i class="fas fa-pen  p-1"></i>
                            </a>
                            <a href="javascript:void(0);" class="btn btn-sm btn-danger " onclick="hapusHotel(\''. $row->pakthtlid .'\')">
                                <i class="fas fa-times  p-1"></i>
                            </a>';
                })
                ->rawColumns(['aksi', 'status_paket'])
                ->make(true);
        }
    }

    public function reqAjaxFormTambahPaketHotel(Request $request)
    {
        if (request()->ajax()) 
        {
            $paketid    = Uuid::uuid4();
            $nama_hotel = $request->nama_hotel;
            $nama_paket = $request->nama_paket;
            $harga      = $request->harga;
            $status     = $request->status;

            $datapakethotel = [
                                    'pakthtlid'     => $paketid,
                                    'hotel_id'      => $nama_hotel,
                                    'nama_paket'    => $nama_paket,
                                    'harga_paket'   => $harga,
                                    'status_paket'  => $status,
                                    'created_at'    => date('Y-m-d H:i:s'),
                                    'updated_at'    => date('Y-m-d H:i:s')
                                ];
            try {
                DB::beginTransaction();
                    DataPaketHotel::insert($datapakethotel);
                DB::commit();
                return response()->json(['statuslog' => 'success', 'message' => 'Data Paket Hotel baru berhasil disimpan', 'title' => 'BERHASIL']);
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal menambahkan Data Paket Hotel, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqAjaxDataPaketHotel(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = ['pakthtlid' => $request->paket];
            $data            = DataPaketHotel::where($cari)->orderby('harga_paket', 'asc')->first();
            $result          = NULL;

            if ($data) 
            {
                $result = $data;
                
                return response()->json($result);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }

    public function reqAjaxFormUpdatePaketHotel(Request $request)
    {
        if (request()->ajax()) 
        {
            $paketid    = $request->paketid;
            $nama_hotel = $request->nama_hotel;
            $nama_paket = $request->nama_paket;
            $harga      = $request->harga;
            $status     = $request->status;

            $datapakethotel = [
                                    'hotel_id'      => $nama_hotel,
                                    'nama_paket'    => $nama_paket,
                                    'harga_paket'   => $harga,
                                    'status_paket'  => $status,
                                    'updated_at'    => date('Y-m-d H:i:s')
                                ];
            try {
                DB::beginTransaction();
                    DataPaketHotel::where(['pakthtlid' => $paketid])->update($datapakethotel);
                DB::commit();
                return response()->json(['statuslog' => 'success', 'message' => 'Data Paket Hotel berhasil diudpate', 'title' => 'BERHASIL']);
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal mengupdate Data Paket Hotel, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }


    // Reservasi Hotel
    public function pgeReservasiHotel()
    {
        $page = [
            'nama_halaman' => 'Data Reservasi Hotel',
            'appname'      => config('app.name'),
        ];
        
        return view('admin-page/stm-reservasihotel', $page);
    }

    // Datatables Reservasi Hotel
    public function reqAjaxTableDataReservasiHotel(Request $request)
    {
        if (request()->ajax()) 
        {            
            return DataTables()->of(DataReservasi::with('paketdata', 'paketdata.hoteldata')->orderBy('created_at', 'desc')->get())
                ->addIndexColumn()
                ->editColumn('nama_lengkap', function($row){
                    return $row->nama_lengkap . '<br><code>' . $row->no_hp . '</code>';
                })
                ->editColumn('detail_paket', function($row){
                    return $row->paketdata->nama_paket . '<br>-' . $row->paketdata->hoteldata->nama_hotel;
                })
                ->editColumn('detail_cico', function($row){
                    return 'CheckIn : ' . date('d-M-Y', strtotime($row->tanggal_checkin)) . '<br>' . 'CheckOut : ' .date('d-M-Y', strtotime($row->tanggal_checkout));
                })
                ->editColumn('detail_harga', function($row){
                    return jumlahHari($row->tanggal_checkin, $row->tanggal_checkout) . ' Malam x Rp. ' . number_format($row->paketdata->harga_paket) . '<br>' . 'Rp. ' . number_format($row->total_biaya);
                })
                ->editColumn('waktu_reservasi', function($row){
                    return date('H:i d-M-Y', $row->waktu_reservasi);
                })
                ->editColumn('aksi', function($row){

                    if ($row->status_res == 'menunggu') {
                        return '<a href="javascript:void(0);" class="" onclick="strukReservasi(\''. asset('file-reservasihotel/' . $row->struct_pembayaran ) .'\')">
                                    <i class="fas fa-file-invoice-dollar btn btn-sm btn-secondary p-2"></i>
                                </a>
                                <a href="javascript:void(0);" class=" "  onclick="verifikasiReservasi(\''. $row->resid .'\', \'selesai\')">
                                    <i class="fas fa-check btn btn-sm btn-success p-2"></i>
                                </a>
                                <a href="javascript:void(0);" class=" " onclick="verifikasiReservasi(\''. $row->resid .'\', \'batal\')">
                                    <i class="fas fa-times btn btn-sm btn-danger p-2"></i>
                                </a>
                                <a href="javascript:void(0);" class="" onclick="detailReservasi(\''. $row->resid .'\')">
                                    <i class="fas fa-eye btn btn-sm btn-primary p-2"></i>
                                </a>';
                    }
                    else{
                        return '<a href="javascript:void(0);" class="" onclick="strukReservasi(\''. asset('file-reservasihotel/' . $row->struct_pembayaran ) .'\')">
                                    <i class="fas fa-file-invoice-dollar btn btn-sm btn-secondary p-2"></i>
                                </a>
                                <a href="javascript:void(0);" class="" onclick="detailReservasi(\''. $row->resid .'\')">
                                    <i class="fas fa-eye btn btn-sm btn-primary p-2"></i>
                                </a>';
                    }                    
                })
                ->rawColumns(['aksi', 'detail_harga', 'nama_lengkap', 'detail_cico', 'detail_paket'])
                ->make(true);
        }
    }


    // Detail Reservasi
    public function reqAjaxDetailPaketHotel(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = ['hotel_id' => $request->hotel];
            $data            = DataPaketHotel::with('hoteldata')->where($cari)->orderBy('harga_paket', 'asc')->get();
            $result          = NULL;
            $nama_hotel      = NULL;
            $output          = NULL;

            if ($data) 
            {
                $result .= '<option value="" hidden selected disabled>-- Jenis Kamar --</option>';
                foreach ($data as $key => $value) 
                {
                    $result .= '<option value="'. $value['pakthtlid'] .'">'. $value->nama_paket .' | Rp '. number_format($value['harga_paket']) .' </option>';
                }

                $nama_hotel             = $data[0]['hoteldata']['nama_hotel'] ?? NULL;
                $output['select']       = $result;    
                $output['nama_hotel']   = $nama_hotel;
                
                return response()->json($output);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }

    // Detail Total Harga
    public function reqAjaxDetailTotalReservasi(Request $request)
    {
        if (request()->ajax()) 
        {
            $cari            = ['pakthtlid' => $request->paket];
            $waktu_cico      = $request->waktu;

            $data            = DataPaketHotel::where($cari)->first();
            $konversiWaktu   = convertDate($waktu_cico);
            $output          = NULL;
            $kode_uniq       = mt_rand(1, 999);

            if ($data) 
            {
                $output = [
                                'totaltext'   => 'Rp. ' . number_format(($data['harga_paket'] * $konversiWaktu['jumlahhari'])  + $kode_uniq),
                                'biaya'       => $konversiWaktu['jumlahhari'] . ' Malam x Rp. ' . number_format($data['harga_paket']) ,
                                'total_biaya' => ($data['harga_paket'] * $konversiWaktu['jumlahhari']) + $kode_uniq,
                                'kodeuniq'    => $kode_uniq
                ];
                
                return response()->json($output);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }

    // Form Reservasi Hotel
    public function reqAjaxFormReservasiHotel(Request $request)
    {
        if (request()->ajax()) 
        {
            $resid        = Uuid::uuid4();
            $tanggalcico  = convertDate($request->tanggalcico);
            $jeniskamar   = $request->jeniskamar;
            $nama_lengkap = $request->nama_lengkap;
            $nohp         = $request->nohp;
            $kodeuniq     = $request->kodeuniqres;
            $paket        = DataPaketHotel::where(['pakthtlid' => $jeniskamar])->first();            
            $file         = $request->file('filestruct');

            // Verifikasi ukuran file (maksimum 1 Mb,verifikasi menggunakan ukuran byte 1048576 )
            if ($file->getSize() > 1048576) {
                return response()->json(['statuslog' => 'error', 'message' => 'Ukuran file terlalu besar. Maksimum 1 MB', 'title' => 'GAGAL'], 404);
            }

            // Verifikasi jenis file (hanya diperbolehkan format gambar)
            $allowedExtensions = ['jpg', 'jpeg', 'png', 'gif'];
            if (!in_array(strtolower($file->getClientOriginalExtension()), $allowedExtensions)) {
                return response()->json(['statuslog' => 'error', 'message' => 'Jenis file tidak didukung. Hanya diperbolehkan: JPG, JPEG, PNG, GIF', 'title' => 'GAGAL'], 404);
            }
            
            $nama_file       = 'RH-' . strtoupper(uniqid()) . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('file-reservasihotel'), $nama_file);
            
            $datapakethotel = [
                                    'resid'             => $resid,
                                    'paket_hotelid'     => $jeniskamar,
                                    'tanggal_checkin'   => $tanggalcico['checkin'],
                                    'tanggal_checkout'  => $tanggalcico['checkout'],
                                    'nama_lengkap'      => $nama_lengkap,
                                    'no_hp'             => $nohp,
                                    'total_biaya'       => ($tanggalcico['jumlahhari'] * $paket['harga_paket']) + $kodeuniq,
                                    'status_res'        => 'menunggu',
                                    'struct_pembayaran' => $nama_file,
                                    'created_at'        => date('Y-m-d H:i:s'),
                                    'updated_at'        => date('Y-m-d H:i:s')
                                ];
            
            try {
                DB::beginTransaction();
                    DataReservasi::insert($datapakethotel);
                DB::commit();
                return response()->json(['statuslog' => 'success', 'message' => 'Reservasi Hotel anda telah berhasil disimpan, kami akan menginformasikan status reservasi anda 1 x 24 jam kedepan', 'title' => 'BERHASIL']);
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal melakukan Reservasi Hotel, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    public function reqAjaxVerifikasiReservasi(Request $request)
    {
        if (request()->ajax()) 
        {
            $resid             = $request->reservasi;
            $status_pembayaran = $request->statusres;
            $admin_verifikasi  = auth()->user()->pmkid;

            $pendaftaran = [
                                    'status_res'        => $status_pembayaran,
                                    'admin_verifikasi'  => $admin_verifikasi,                  
                                    'updated_at'        => date('Y-m-d H:i:s')
                                ];
            try {
                DB::beginTransaction();
                    DataReservasi::where(['resid' => $resid])->update($pendaftaran);
                DB::commit();
                return response()->json(['statuslog' => 'success', 'message' => 'Reservasi telah di ' .  ucfirst($status_pembayaran) . 'kan Segera hubungi langsung peserta untuk menginformasikan hasil reservasi.' , 'title' => 'BERHASIL']);                
            } 
            catch (\Exception $e) 
            {
                DB::rollback();
                return response()->json(['statuslog' => 'error', 'message' => 'Gagal Memverifikasi Peserta, mohon coba kembali', 'title' => 'GAGAL'], 404);
            }
        }
        else{
            return response()->json(['statuslog'=> 'error' ,'message' => 'Gagal Mengakses Halaman'], 404);
        }
    }

    // Preview Data Reservasi Hotel
    public function reqAjaxDetailReservasi(Request $request)
    {   
        if (request()->ajax()) 
        {
            $cari            = $request->reservasi;
            $data            = DataReservasi::where('resid', $cari)->first();
            $result          = NULL;

            
            if ($data) 
            {   
                $result           = [
                                    'namalengkap' => $data->nama_lengkap,
                                    'nohp'        => $data->no_hp,
                                    'namahotel'   => $data->paketdata->hoteldata->nama_hotel,
                                    'pakethotel'  => $data->paketdata->nama_paket,
                                    'cico'        => date('d-M-Y', strtotime($data->tanggal_checkin)) . ' s.d ' . date('d-M-Y', strtotime($data->tanggal_checkout)),
                                    'totalbiaya'  => jumlahHari($data->tanggal_checkin, $data->tanggal_checkout) . ' Malam x Rp. ' . number_format($data->paketdata->harga_paket) . '<br>' . 'Rp. ' . number_format($data->total_biaya),
                                    'waktu'       => date('d-M-Y', strtotime($data->created_at))
                ];
                
                return response()->json($result);
            }
            else{
                return response()->json(['statuslog'=> 'error' ,'message' => 'Data tidak ditemukan', 'title'=> 'GAGAL'], 404);
            }
        }
    }
}
