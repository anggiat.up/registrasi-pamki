<?php

namespace App\Http\Controllers;

use App\Models\DataHotel;
use App\Models\DataPaket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PublicDisplayController extends Controller
{
    public function pageLandingDisplay()
    {
        $page = [
            'nama_halaman' => 'PAMKI 2024 RIAU',
            'appname'      => config('app.name')

        ];

        // $dataHotel = DataHotel::orderBy('rate_bintang', 'desc')->get()->toArray();
        $dataHotel = DataHotel::where('status', 'aktif')->orderBy('rate_bintang', 'desc')->get()->toArray();

        $page['hotel'] = $dataHotel;
        

       
        // return view('public-page/page-landing', $page);
        return view('page-public/page-home', $page);
    }

    

    public function pageMateriAcara()
    {
        $page = [
            'nama_halaman' => 'Materi Acara | PIT PAMKI 2024 RIAU',
            'appname'      => config('app.name')

        ];
        return view('page-public/page-susunan-acara', $page);
    }

    public function pageKataSambutan()
    {
        $page = [
            'nama_halaman' => 'Kata Sambutan | PIT PAMKI 2024 RIAU',
            'appname'      => config('app.name')

        ];
        return view('public-page/page-kata-sambutan', $page);
    }

    public function pagePanitia()
    {
        $page = [
            'nama_halaman' => 'Panitia | PIT PAMKI 2024 RIAU',
            'appname'      => config('app.name')

        ];
        return view('page-public/page-panitia', $page);
    }

    public function pagePendaftaran()
    {
        $datapaket  = DataPaket::distinct()->pluck('jenis_peserta')->sort()->values()->toArray();
        $page = [
            'nama_halaman' => 'Pendaftaran | PIT PAMKI 2024 RIAU',
            'appname'      => config('app.name'),
            'paket'        => $datapaket

        ];
        return view('page-public/page-sertifikat', $page);
    }
    
    public function pageSertifikat()
    {
        $datapaket  = DataPaket::distinct()->pluck('jenis_peserta')->sort()->values()->toArray();
        $page = [
            'nama_halaman' => 'Sertifikat | PIT PAMKI 2024 RIAU',
            'appname'      => config('app.name'),
            'paket'        => $datapaket

        ];
        return view('page-public/page-sertifikat', $page);
    }

    public function pageKontakKami()
    {
        $page = [
            'nama_halaman' => 'Kontak Kami | PIT PAMKI 2024 RIAU',
            'appname'      => config('app.name')

        ];
        return view('page-public/page-kontak', $page);
    }

    public function sistemDownload($folderName, $fileName)
    {
        $file_path = public_path($folderName . '/' . $fileName);
        if (!file_exists($file_path)) {
            abort(404);
        }

        return response()->download($file_path, $fileName);
    }
}


// $dataHotels = [
//     [
//         "nama" => "PANGERAN",
//         "bintang" => "****",
//         "type" => "DELUXE",
//         "rate" => "Rp925.000",
//         "validity" => "",
//         "remark" => ""
//     ],
//     [
//         "nama" => "PANGERAN",
//         "bintang" => "****",
//         "type" => "GRAND DELUXE",
//         "rate" => "Rp980.000",
//         "validity" => "31-Dec-24",
//         "remark" => "All Market"
//     ],
//     [
//         "nama" => "PANGERAN",
//         "bintang" => "****",
//         "type" => "EXECUTIVE",
//         "rate" => "Rp1.125.000",
//         "validity" => "",
//         "remark" => "All Market"
//     ],
//     [
//         "nama" => "PANGERAN",
//         "bintang" => "****",
//         "type" => "JUNIOR SUITE",
//         "rate" => "Rp3.045.000",
//         "validity" => "",
//         "remark" => ""
//     ],
//     [
//         "nama" => "PANGERAN",
//         "bintang" => "****",
//         "type" => "GRAND EXEC SUITE",
//         "rate" => "Rp3.475.000",
//         "validity" => "",
//         "remark" => ""
//     ],
//     [
//         "nama" => "PANGERAN",
//         "bintang" => "****",
//         "type" => "PANGERAN SUITE",
//         "rate" => "Rp4.875.000",
//         "validity" => "",
//         "remark" => ""
//     ],
//     [
//         "nama" => "GRAND CENTRAL",
//         "bintang" => "",
//         "type" => "SUPERIOR",
//         "rate" => "",
//         "validity" => "",
//         "remark" => ""
//     ],
//     [
//         "nama" => "GRAND CENTRAL",
//         "bintang" => "",
//         "type" => "DELUXE",
//         "rate" => "",
//         "validity" => "31-Dec-24",
//         "remark" => "All Market"
//     ],
//     [
//         "nama" => "THE PREMIERE",
//         "bintang" => "****",
//         "type" => "Superior",
//         "rate" => "Rp950.000",
//         "validity" => "",
//         "remark" => ""
//     ],
//     [
//         "nama" => "THE PREMIERE",
//         "bintang" => "****",
//         "type" => "EXECUTIVE",
//         "rate" => "Rp1.090.000",
//         "validity" => "",
//         "remark" => ""
//     ],
//     [
//         "nama" => "THE PREMIERE",
//         "bintang" => "****",
//         "type" => "EXECUTIVE DELUXE",
//         "rate" => "Rp1.470.000",
//         "validity" => "",
//         "remark" => ""
//     ],
//     [
//         "nama" => "THE PREMIERE",
//         "bintang" => "****",
//         "type" => "JUNIOR SUITE",
//         "rate" => "Rp2.470.000",
//         "validity" => "29-Dec-24",
//         "remark" => "All Market"
//     ],
//     [
//         "nama" => "THE PREMIERE",
//         "bintang" => "****",
//         "type" => "PRESIDENT SUITE",
//         "rate" => "Rp4.170.000",
//         "validity" => "",
//         "remark" => ""
//     ],
//     [
//         "nama" => "THE PREMIERE",
//         "bintang" => "****",
//         "type" => "PENTHOUSE",
//         "rate" => "Rp6.170.000",
//         "validity" => "",
//         "remark" => ""
//     ],
//     [
//         "nama" => "GRAND JATRA",
//         "bintang" => "****",
//         "type" => "SUPERIOR",
//         "rate" => "Rp820.000",
//         "validity" => "",
//         "remark" => ""
//     ],
//     [
//         "nama" => "GRAND JATRA",
//         "bintang" => "****",
//         "type" => "DELUXE",
//         "rate" => "Rp920.000",
//         "validity" => "",
//         "remark" => ""
//     ],
//     [
//         "nama" => "GRAND JATRA",
//         "bintang" => "****",
//         "type" => "JUNIOR SUITE",
//         "rate" => "Rp1.620.000",
//         "validity" => "30-Dec-24",
//         "remark" => "All Market"
//     ],
//     [
//         "nama" => "GRAND JATRA",
//         "bintang" => "****",
//         "type" => "EXECUTIVE SUITE",
//         "rate" => "Rp2.120.000",
//         "validity" => "",
//         "remark" => ""
//     ],
//     [
//         "nama" => "ARYADUTA",
//         "bintang" => "*****",
//         "type" => "SUPERIOR",
//         "rate" => "Rp795.000",
//         "validity" => "",
//         "remark" => ""
//     ],
//     [
//         "nama" => "ARYADUTA",
//         "bintang" => "*****",
//         "type" => "DELUXE",
//         "rate" => "Rp895.000",
//         "validity" => "31-Dec-24",
//         "remark" => "All market"
//     ],
//     [
//         "nama" => "ARYADUTA",
//         "bintang" => "*****",
//         "type" => "EXECUTIVE",
//         "rate" => "Rp1.095.000",
//         "validity" => "",
//         "remark" => "all market"
//     ],
//     [
//         "nama" => "ARYADUTA",
//         "bintang" => "*****",
//         "type" => "BUSINESS SUITE",
//         "rate" => "Rp1.670.000",
//         "validity" => "",
//         "remark" => ""
//     ],
//     [
//         "nama" => "NOVOTEL",
//         "bintang" => "*****",
//         "type" => "SUPERIOR",
//         "rate" => "Rp970.000",
//         "validity" => "",
//         "remark" => "30-Dec-24 All Market"
//     ],
//     [
//         "nama" => "THE ZURI",
//         "bintang" => "*****",
//         "type" => "SUPERIOR",
//         "rate" => "Rp. 800.000",
//         "validity" => "",
//         "remark" => ""
//     ],
//     [
//         "nama" => "THE ZURI",
//         "bintang" => "*****",
//         "type" => "DELUXE",
//         "rate" => "Rp. 990.000",
//         "validity" => "31-Dec-24",
//         "remark" => "All Market"
//     ],
//     [
//         "nama" => "THE ZURI",
//         "bintang" => "*****",
//         "type" => "JUNIOR SUITE",
//         "rate" => "Rp. 1.770.000",
//         "validity" => "",
//         "remark" => ""
//     ],
//     [
//         "nama" => "CEMPAKA HOUSE SYARIAH",
//         "bintang" => "",
//         "type" => "DELUXE",
//         "rate" => "Rp. 495.000",
//         "validity" => "",
//         "remark" => "31-Dec-24 All market"
//     ],
//     [
//         "nama" => "CEMPAKA HOUSE SYARIAH",
//         "bintang" => "",
//         "type" => "GRAND DELUXE",
//         "rate" => "Rp. 575.000",
//         "validity" => "",
//         "remark" => "All market"
//     ],
//     [
//         "nama" => "CEMPAKA HOUSE SYARIAH",
//         "bintang" => "",
//         "type" => "EXECUTIVE",
//         "rate" => "Rp. 800.000",
//         "validity" => "",
//         "remark" => ""
//     ]
// ];