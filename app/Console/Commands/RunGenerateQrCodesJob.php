<?php

namespace App\Console\Commands;

use App\Jobs\GenerateQR;
use Illuminate\Console\Command;

class RunGenerateQrCodesJob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'job:generateqr';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate QR codes for all participants';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        GenerateQR::dispatch();
        $this->info('QR codes have been generated.');
    }
}
