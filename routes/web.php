<?php

use App\Http\Controllers\AuthsController;
use App\Http\Controllers\PublicDisplayController;
use App\Http\Controllers\Sistems\HotelController;
use App\Http\Controllers\Sistems\ManajemenController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Response;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [PublicDisplayController::class, 'pageLandingDisplay'])->name('dashboard');
Route::get('/home', [PublicDisplayController::class, 'pageLandingDisplay'])->name('home');
Route::get('/home#page-services', [PublicDisplayController::class, 'pageLandingDisplay'])->name('home');
Route::get('/materi-acara', [PublicDisplayController::class, 'pageMateriAcara'])->name('materi-acara');
Route::get('/kata-sambutan', [PublicDisplayController::class, 'pageKataSambutan'])->name('kata-sambutan');
Route::get('/home#kata-sambutan', [PublicDisplayController::class, 'pageLandingDisplay'])->name('home');
Route::get('/kontak', [PublicDisplayController::class, 'pageKontakKami'])->name('kontak');
Route::get('/pendaftaran', [PublicDisplayController::class, 'pagePendaftaran'])->name('pendaftaran');
Route::get('/sertifikat', [PublicDisplayController::class, 'pagePendaftaran'])->name('sertifikat');
Route::get('/panitia', [PublicDisplayController::class, 'pagePanitia'])->name('panitia');
Route::get('/makalah-dan-poster', [PublicDisplayController::class, 'pageMakalahPoster'])->name('makalah-dan-poster');
Route::get('/home#kata-sambutan', [PublicDisplayController::class, 'pageLandingDisplay'])->name('home');

Route::get('/download/{folderName}/{fileName}', [PublicDisplayController::class, 'sistemDownload'])->name('download.file');



// Sistem Administration
// Route::get('/sistem', [PublicDisplayController::class, 'pageLandingDisplay'])->name('sistem');
Route::get('/sistem', [AuthsController::class, 'index'])->name('sistem');
Route::get('/sistem/login', [AuthsController::class, 'index'])->name('sistem.login');
Route::get('/sistem/login', [AuthsController::class, 'index'])->name('login');
Route::post('/sistem/login', [AuthsController::class, 'AjaxprosesLogin'])->name('sistem.login');
Route::get('/logout', [AuthsController::class, 'logout'])->name('logout');
Route::get('/sistem/harga-paket', [ManajemenController::class, 'reqAjaxHargaPaket'])->name('sistem.harga-paket');
Route::post('/registrasi-peserta', [ManajemenController::class, 'reqAjaxRegistrasiPeserta'])->name('registrasi-peserta');
Route::post('/notifikasi-ulang', [ManajemenController::class, 'reqAjaxNotifikasiUlang'])->name('notifikasi-ulang');
Route::get('/detail-invoice', [ManajemenController::class, 'reqAjaxInvoice'])->name('detail-invoice');
Route::get('/detail-sertifikat', [ManajemenController::class, 'reqAjaxDataPesertaSertifikat'])->name('detail-sertifikat');
Route::post('/cetak-sertifikat', [ManajemenController::class, 'reqAjaxCetakSertifikat'])->name('cetak-sertifikat');
Route::post('/upload-bukti-bayar', [ManajemenController::class, 'reqAjaxFormUploadPembayaran'])->name('upload-bukti-bayar');
Route::get('/sistem/noreg-peserta', [ManajemenController::class, 'reqAjaxNoreg'])->name('sistem/noreg-peserta');
Route::post('/registrasi-abstract', [ManajemenController::class, 'reqAjaxFormAbstract'])->name('registrasi-abstract');
Route::get('/detail-hotel', [HotelController::class, 'reqAjaxDetailPaketHotel'])->name('detail-hotel');
Route::get('/total-harga-reservasi', [HotelController::class, 'reqAjaxDetailTotalReservasi'])->name('total-harga-reservasi');
Route::post('/reservasi-hotel', [HotelController::class, 'reqAjaxFormReservasiHotel'])->name('reservasi-hotel');

Route::get('/download/{filename}', function ($filename) {
    $filePath = public_path('generate-sertifikat/' . $filename);

    if (file_exists($filePath)) {
        return Response::download($filePath, $filename);
    } else {
        abort(404, 'File not found.');
    }
})->name('downloadFile');

Route::middleware(['auth'])->group(function () {
    Route::get('/sistem/dashboard', [ManajemenController::class, 'pgeDashboard'])->name('sistem.dashboard');
    Route::get('/sistem/paket-kegiatan', [ManajemenController::class, 'pgePaketKegiatan'])->name('sistem.paket-kegiatan');
    Route::get('/sistem/pendaftaran-peserta', [ManajemenController::class, 'pgePendaftaranPeserta'])->name('sistem.pendaftaran-peserta');
    Route::get('/sistem/peserta-terverifikasi', [ManajemenController::class, 'pgePesertaTerverifikasi'])->name('sistem.peserta-terverifikasi');
    Route::get('/sistem/data-user', [ManajemenController::class, 'pgeDataUser'])->name('sistem.data-user');
    Route::get('/sistem/peserta-abstract', [ManajemenController::class, 'pgePesertaAbstract'])->name('sistem.peserta-abstract');
    Route::get('/sistem/data-hotel', [HotelController::class, 'pgeHotel'])->name('sistem.data-hotel');
    Route::get('/sistem/data-paket-hotel', [HotelController::class, 'pgePaketHotel'])->name('sistem.data-paket-hotel');
    Route::get('/sistem/reservasi-hotel', [HotelController::class, 'pgeReservasiHotel'])->name('sistem.reservasi-hotel');

    Route::post('/sistem/tambah-paket', [ManajemenController::class, 'reqAjaxFormTambahPaket'])->name('sistem.tambah-paket');
    Route::get('/sistem/data-paket', [ManajemenController::class, 'reqAjaxDataPaket'])->name('sistem.data-paket');
    Route::post('/sistem/update-paket', [ManajemenController::class, 'reqAjaxFormUpdatePaket'])->name('sistem.update-paket');
    Route::get('/sistem/data-peserta-pendaftaran', [ManajemenController::class, 'reqAjaxDataPesertaPendaftaran'])->name('sistem.data-peserta-pendaftaran');
    Route::get('/sistem/bukti-pembayaran', [ManajemenController::class, 'reqAjaxBuktiPembayaran'])->name('sistem.bukti-pembayaran');
    Route::get('/sistem/verifikasi-pendaftaran', [ManajemenController::class, 'reqAjaxVerifikasi'])->name('sistem.verifikasi-pendaftaran');
    Route::get('/sistem/hapus-pendaftaran', [ManajemenController::class, 'reqAjaxHapusPendaftaran'])->name('sistem.hapus-pendaftaran');

    Route::get('/sistem/data-presensi', [ManajemenController::class, 'pgeDataPresensi'])->name('sistem.data-presensi');
    Route::get('/sistem/absensi-peserta', [ManajemenController::class, 'pgeAbsensiPeserta'])->name('sistem.absensi-peserta');
    Route::get('/sistem/view-email', [ManajemenController::class, 'previewEmail'])->name('sistem.view-email');
    Route::get('/sistem/single-send-email/{email}', [ManajemenController::class, 'testSendQrcode'])->name('sistem.single-send-email');
    Route::get('/sistem/search-peserta', [ManajemenController::class, 'reqAjaxSearchPeserta'])->name('sistem.search-peserta');
    Route::get('/sistem/presensi-kehadiran', [ManajemenController::class, 'reqAjaxPresensiKehadiran'])->name('sistem.presensi-kehadiran');
    Route::get('/sistem/generate-qrcode', [ManajemenController::class, 'reqAjaxGenerateQRCode'])->name('sistem.generate-qrcode');
    Route::get('/sistem/send-notif-qrcode', [ManajemenController::class, 'reqAjaxSendNotificationQRCode'])->name('sistem.send-notif-qrcode');
    Route::get('/sistem/reset-presensi', [ManajemenController::class, 'reqAjaxResetPresensi'])->name('sistem.reset-presensi');

    
    Route::post('/sistem/tambah-user', [ManajemenController::class, 'reqAjaxFormTambahUser'])->name('sistem.tambah-user');
    Route::get('/sistem/detail-user', [ManajemenController::class, 'reqAjaxDataUser'])->name('sistem.detail-user');
    Route::post('/sistem/update-user', [ManajemenController::class, 'reqAjaxFormUpdateUser'])->name('sistem.update-user');

    Route::post('/sistem/tambah-hotel', [HotelController::class, 'reqAjaxFormTambahHotel'])->name('sistem.tambah-hotel');
    Route::post('/sistem/tambah-paket-hotel', [HotelController::class, 'reqAjaxFormTambahPaketHotel'])->name('sistem.tambah-paket-hotel');
    Route::get('/sistem/detail-paket-hotel', [HotelController::class, 'reqAjaxDataPaketHotel'])->name('sistem.detail-paket-hotel');
    Route::post('/sistem/update-paket-hotel', [HotelController::class, 'reqAjaxFormUpdatePaketHotel'])->name('sistem.update-paket-hotel');
    Route::get('/sistem/verifikasi-reservasi', [HotelController::class, 'reqAjaxVerifikasiReservasi'])->name('sistem.verifikasi-reservasi');
    Route::get('/sistem/data-reservasi-detail', [HotelController::class, 'reqAjaxDetailReservasi'])->name('sistem.data-reservasi-detail');
    // Route::post('/sistem/tambah-paket-hotel', [HotelController::class, 'reqAjaxFormTambahPaketHotel'])->name('sistem.tambah-paket-hotel');

    
    Route::get('/sistem/table-data-paket', [ManajemenController::class, 'reqAjaxTableDataPaket'])->name('sistem.table-data-paket');
    Route::get('/sistem/table-data-pendaftaran', [ManajemenController::class, 'reqAjaxTablePendaftaranPeserta'])->name('sistem.table-data-pendaftaran');
    Route::get('/sistem/table-data-peserta', [ManajemenController::class, 'reqAjaxTablePeserta'])->name('sistem.table-data-peserta');
    Route::get('/sistem/table-data-presensi', [ManajemenController::class, 'reqAjaxTablePresensi'])->name('sistem.table-data-presensi');
    Route::get('/sistem/table-data-users', [ManajemenController::class, 'reqAjaxTableDataUser'])->name('sistem.table-data-users');
    Route::get('/sistem/table-data-abs', [ManajemenController::class, 'reqAjaxTableDataAbstract'])->name('sistem.table-data-abs');
    Route::get('/sistem/table-data-hotel', [HotelController::class, 'reqAjaxTableDataHotel'])->name('sistem.table-data-hotel');
    Route::get('/sistem/table-data-pakethotel', [HotelController::class, 'reqAjaxTableDataPaketHotel'])->name('sistem.table-data-pakethotel');
    Route::get('/sistem/table-data-reservasihotel', [HotelController::class, 'reqAjaxTableDataReservasiHotel'])->name('sistem.table-data-reservasihotel');

    Route::get('/sistem/cetak-laporan/{jenis}', [ManajemenController::class, 'reqCetakLaporan'])->name('sistem.cetak-laporan');
    Route::get('/sistem/cetak-laporan-presensi/{jenis}', [ManajemenController::class, 'reqCetakLaporanPresensi'])->name('sistem.cetak-laporan-presensi');
});
